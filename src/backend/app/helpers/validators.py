import email.utils as emailutil
import urllib.parse as urlparse
from flask_restx import abort


def _add_veb(veb, field, reason):
  if veb is None:
    abort(403, f"Validation error on '{field}': {reason}")
  else:
    veb.add(field, reason)

def validate_and_format_email(emailstr, veb=None):
  _,field_email = emailutil.parseaddr(emailstr)
  if "@" not in field_email:
    _add_veb(veb, "email", "Invalid email address.")
  return field_email


def validate_password(pwdstr, veb=None):
  pass

def validate_integer(field, value, default=None, minimum=1, maximum=None, veb=None):
  value = default_value(field, value, default, veb)
  if value is None:
    return None
  
  if type(value) is not int:
    try:
      value = int(value)
    except:
      _add_veb(veb, field, "Not an integer.")
      return None
  
  if minimum is not None and value < minimum:
    _add_veb(veb, field, f"Smaller than minimum required: {minimum}")
    return None
  elif maximum is not None and value > maximum:
    _add_veb(veb, field, f"Smaller than maximum required: {maximum}")
    return None

  return value

def validate_string(field, value, default=None, minlen=2, maxlen=100, veb=None):
  value = default_value(field, value, default, veb)
  if value is None:
    return None
  
  if type(value) is not str:
    _add_veb(veb, field, "Not a string.")
  elif minlen is not None and len(value) < minlen:
    _add_veb(veb, field, f"At least {minlen} characters.")
  elif maxlen is not None and len(value) > maxlen:
    _add_veb(veb, field, f"At most {maxlen} characters.")

  return value
  
def default_value(field, value, default, veb=None):
  if value is None:
    if default is not None:
      value = default
    else:
      _add_veb(veb, field, "This field is required.")
      return None
  return value
  

    
    