'''
Helper library for review specific

'''
import helpers.dbutils as dbutils
from helpers.request_handlers import single_unpack
from flask import abort

def delete_review(cur, revid):
  dbutils.simple_update(cur, "reviews", [("status", 'deleted')], wcb=dbutils.WhereClauseBuilder("revid=%s", revid))
  
def is_review_exist(cur, revid):
  dbutils.simple_select(cur, "reviews", ("revid",), wcb=dbutils.WhereClauseBuilder("revid=%s", revid))
  if single_unpack(cur.fetchone()) is None:
    abort(404, "Review ID not found")