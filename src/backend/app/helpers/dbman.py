import psycopg2
import config
from flask import g
from mainapp import app

def _connect_db():
  return psycopg2.connect(
    database=config.POSTGRES_DB,
    user=config.POSTGRES_USER,
    password=config.POSTGRES_PASSWORD,
    host=config.POSTGRES_HOST,
    port=config.POSTGRES_PORT
  )

def get_dbconn():
  if not hasattr(g, 'pgconn'):
    g.pgconn = _connect_db()
  return g.pgconn

@app.teardown_appcontext
def close_db(error):
  if hasattr(g, 'pgconn'):
    g.pgconn.close()