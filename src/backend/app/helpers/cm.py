from flask import request

class _Dummy():
  def __getattr__(self, name):
    """Return None automatically if attribute name is not found."""
    self.__setattr__(name, None)
    return None

def get_reqctx():
  if not hasattr(request, 'reqctx'):
    request.reqctx = _Dummy()
  return request.reqctx