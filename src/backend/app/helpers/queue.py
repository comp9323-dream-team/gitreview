import os.path as path
import pickle
from datetime import datetime

from werkzeug import exceptions
import config

def dump_queue(data):
  filename = datetime.utcnow().strftime('%Y%m%d%H%M%S%f')
  filename = path.join(config.QUEUE_DIR, f"qm_{filename}")
  try:
    with open(filename, "wb") as f:
      pickle.dump(data, f, fix_imports=False)
  except:
    return False

  return True
