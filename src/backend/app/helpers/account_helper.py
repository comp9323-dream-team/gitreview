from flask_restx import abort
import re

import helpers.dbman as db
from helpers.dbutils import WhereClauseBuilder, simple_select, single_unpack

def validate_register_username(username):
  # check if username is in correct format 
  if username is None:
    return None
      
  username = str(username).strip()
  if re.fullmatch("^[A-Za-z][A-Za-z0-9_]+$", username) is None:
    abort(400,  "Wrong username format. Only number, letter and underscore are allowed")
  
  # check if username is in the right length range
  length = len(username)
  if length < 2 or length > 20:
    abort(400, "Username must between 2 to 20 characters")
  
  with db.get_dbconn() as conn:
    cur = conn.cursor()
    wcb= WhereClauseBuilder("field=%s", "username")
    wcb.and_("data=%s", username.lower())
    simple_select(cur, "userprofiles", ("data",), wcb=wcb)
    if single_unpack(cur.fetchone()) is not None:
      abort(403,  "Username already taken, please choose another username")
  
  return username.lower()
  
def validate_username(username):
  if username is None:
    return None
  username = str(username).strip()
  
  # check if username is in correct format 
  if re.fullmatch("^[A-Za-z][A-Za-z0-9_]+$", username) is None:
    abort(400,  "Wrong username format. Only number, letter and underscore are allowed")
  
  # check if username is in the right length range
  length = len(username)
  if length < 2 or length > 20:
    abort(400, "Username must between 2 to 20 characters")
    
  return username.lower()
  
def get_userid(cur, username):
  # get uid from DB based on the username
  wcb= WhereClauseBuilder("field=%s", "username")
  wcb.and_("data=%s", username)
  simple_select(cur, "userprofiles", ("uid",), wcb=wcb)
  uid = single_unpack(cur.fetchone())
  
  #check if uid is exist
  if uid is None:
    abort(404, f"User with username = {username} not found.")
  
  return uid
  
def get_username(cur, uid):
  # get username from DB based on the uid
  wcb= WhereClauseBuilder("field=%s", "username")
  wcb.and_("uid=%s", uid)
  simple_select(cur, "userprofiles", ("data",), wcb=wcb)
  username = single_unpack(cur.fetchone())
  
  #check if username is exist
  if username is None:
    abort(404, f"Username not found.")
  
  return username
  
def validate_edit_username(username, uid):
  # check if username is in correct format 
  if username is None:
    return None
      
  username = str(username).strip()
  if re.fullmatch("^[A-Za-z][A-Za-z0-9_]+$", username) is None:
    abort(400,  "Wrong username format. Only number, letter and underscore are allowed")
  
  # check if username is in the right length range
  length = len(username)
  if length < 2 or length > 20:
    abort(400, "Username must between 2 to 20 characters")
  
  with db.get_dbconn() as conn:
    cur = conn.cursor()
    wcb= WhereClauseBuilder("field=%s", "username")
    wcb.and_("data=%s", username.lower())
    simple_select(cur, "userprofiles", ("uid",), wcb=wcb)
    owner = single_unpack(cur.fetchone()) 
    if owner is not None:
      if owner != uid:
        abort(403,  "Username already taken, please choose another username")
  
  return username.lower()