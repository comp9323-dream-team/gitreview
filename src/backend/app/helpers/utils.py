import secrets

class ValidationErrorListBuilder:
  def __init__(self, data=None):
    self._data = []
    self._ret = {"fields": self._data}
    if data is not None:
      self.extend(data)

  def get(self):
    return self._ret

  def add(self, field_name, reason):
    self._data.append({
      'name':field_name,
      'reason':reason
    })

  def extend(self, data):
    for item in data:
      self.add(item[0], item[1])

  def __len__(self):
    return len(self._data)

def gen_id():
  while True:
    ret = secrets.randbits(31)
    # we want to reserve low values "just in case"
    if ret < 1024:
      continue
    return ret

def default_str(v, default=""):
  return v if v is not None else default

def truthy_string(val):
  if type(val) is bool:
    return val
  val = str(val).lower().strip()
  if val == "true":
    return True
  # integer other than 0 == true. 0 is false
  try:
    intval = int(val)
    if intval != 0:
      return True
  except:
    pass
  # anything else (e.g. "correct", "alright", "false") = return false
  return False

def parse_git_ssh_url(val):
  """Returns: (username, hostname, path)"""
  try:
    sshhost, path = val.split(":", 1)
    username, hostname = sshhost.rsplit("@", 1)
    return username, hostname, path
  except:
    return None, None, None
  