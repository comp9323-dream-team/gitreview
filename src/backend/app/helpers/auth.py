import secrets
import hashlib
import pickle
from flask import request
import config
import os
import os.path as path
from helpers.request_handlers import *
import helpers.dbman as db

# some consts/settings
PBKDF2_ITER=1000

TOKEN_TYPE_SESSION="session"
TOKEN_TYPE_FORGOT_PASSWORD="iforgot"
TOKEN_TYPE_PASSWORD_RESET="pwreset"

def _get_token_filename(token):
  # TODO: sanitise/check token file name (must exact match regex)
  return path.join(config.SESSION_DIR, token)

def _gen_token():
  while True:
    token = secrets.token_hex(32)
    token_file = _get_token_filename(token)
    if not path.exists(token_file):
      return token, token_file

def get_pass_hash(pw, salt):
  pwraw = pw.encode('utf-8')
  return hashlib.pbkdf2_hmac('sha256', pwraw, salt, PBKDF2_ITER).hex()

def store_token_data(type, uid):
  (token, token_file) = _gen_token()
  with open(token_file, "wb") as f:
    data = {
      "uid": uid,
      "type": type
    }
    pickle.dump(data, f, fix_imports=False)
  return token

def read_token_data(token):
  """
  Returns token type and UID if token is valid.
  Caller is responsible for checking if the UID is actually valid.
  """
  # TODO: check token expiration (using atime)
  try:
    with open(_get_token_filename(token), "rb") as f:
      ret = pickle.load(f, fix_imports=False)
      return (ret["type"], ret["uid"])
  except FileNotFoundError:
    pass
  return (None, None)

def invalidate_token(token):
  try:
    os.unlink(_get_token_filename(token))
  except:
    pass

def check_user_valid(dbcur, uid):
  dbcur.execute("select blocked from users where uid=%s", (uid,))
  block_status = single_unpack(dbcur.fetchone())
  return not ((block_status is None) or block_status)

def get_authorisation_token(r):
  authvalue = r.headers.get('Authorization',None)
  if authvalue is None:
    abort(403, 'Authorisation value not found.')
  try:
    (authtype, token) = authvalue.split()
  except:
    abort(400, "Invalid authorisation value.")
  if authtype != "Token":
    abort(400, "Only 'Token' authorisation type is supported.")
  return token

def authenticate(r, token=None, mandatory=True):
  if token is None:
    token = get_authorisation_token(r)
  
  def failgen():
    if mandatory:
      abort(403, "Invalid authorisation token.")

  (token_type, uid) = read_token_data(token)
  if token_type != TOKEN_TYPE_SESSION:
    failgen()
  
  with db.get_dbconn() as conn:
    cur = conn.cursor()
    if not check_user_valid(cur, uid):
      abort(403, "Invalid authorisation token.")
  
  return uid
