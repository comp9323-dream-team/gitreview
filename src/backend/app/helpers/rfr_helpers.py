'''
Helper library for RFR specific

'''
import helpers.dbutils as dbutils
from helpers.request_handlers import single_unpack
from flask import abort

def is_rfr_exist(cur, rfrid):
  dbutils.simple_select(cur, "rfrs", ("rfrid",),
  wcb=dbutils.WhereClauseBuilder("rfrid=%s", rfrid))
  if single_unpack(cur.fetchone()) is None:
      abort(404, "RFR ID not found")
      
def is_rfr_owner(cur, uid, rfrid):
  dbutils.simple_select(cur, "rfrs", ("uid",), wcb=dbutils.WhereClauseBuilder("rfrid=%s", rfrid))
  owner = single_unpack(cur.fetchone()) 
  if owner is None:
    abort(404, "RFR owner not found")
  
  if uid != owner:
    abort(403, "You are not registered as the RFR owner.")
  
def get_rfr_repo(cur, rfrid):
  dbutils.simple_select(cur, "rfrs", ("repoid",), wcb=dbutils.WhereClauseBuilder("rfrid=%s", rfrid))
  repoid = single_unpack(cur.fetchone()) 
  if repoid is None:
    abort(404, "Repo of this RFR not found")
  
  return repoid

def get_rfr_type(cur, rfrid):
  dbutils.simple_select(cur, "rfrs", ("type",), wcb=dbutils.WhereClauseBuilder("rfrid=%s", rfrid))
  type = single_unpack(cur.fetchone()) 
  if type is None:
    abort(404, "Repo of this RFR not found")
  
  return type

def is_rfr_open(cur, rfrid):
  wcb=dbutils.WhereClauseBuilder("rfrid=%s", rfrid)
  wcb.and_("status=%s", "open")
  dbutils.simple_select(cur, "rfrs", ("rfrid",), wcb=wcb)
  if single_unpack(cur.fetchone()) is None:
      abort(405, "RFR not open")