"""
Additional helpers for simplifying the repo request_handlers.

Credit: Jake Antmann.
"""
import helpers.dbman as db
from helpers.request_handlers import single_unpack
from flask_restx import abort
from flask import make_response
import requests
import helpers.utils as utils
import helpers.dbutils as dbutils


def forward(resp):
    """Forward a response back to our sender."""
    return make_response(resp.content, resp.status_code, resp.headers.items())

def dictify(arr, old_dict):
    """Make a dict out of a subset of keys of another dict."""
    try:
        new_dict = {}
        for i in arr:
            new_dict[i] = old_dict[i]
    except KeyError:
        abort(400, "Input missing")
    return new_dict


def build_data(arr, old_dict):
    """Build data from an array and body."""
    try:
        data = dictify(arr, old_dict)
    except KeyError:
        abort(400, "Input missing")
    return data


def owns_repo(cur, uid, repoid):
    """Check the user owns the repo."""
    cur.execute(f"""select uid from repo_owners
                where uid = %s and repoid = %s""",
                (uid, repoid))
    uid = single_unpack(cur.fetchone())
    if uid is None:
        abort(403, ("You are not registered as an owner of this repo"))

def is_repo_exist(cur, repoid):
    wcb = dbutils.WhereClauseBuilder("repoid=%s", repoid)
    wcb.and_("removed=%s", 'false')
    dbutils.simple_select(cur, "repos", ("repoid",), wcb=wcb)
    if single_unpack(cur.fetchone()) is None:
        abort(404, "Repo ID not found")

class API_helper:
    """Class for API helpers."""

    def __init__(self, url):
        """Set the cursor."""
        self.url = url

    def add_cur(self, cur):
        """Add a cursor."""
        self.cur = cur

    def db_input(self, table_name, fields):
        """Input a dict of fields into a database table."""
        keys = ','.join(fields.keys())
        values = ', '.join(['%s']*len(fields))
        querystr = f"insert into {table_name} ({keys}) values ({values})"
        tup = tuple([fields[field] for field in fields])
        return self.cur.execute(querystr, tup)

    def make_id(self, id_name, table_name):
        """Generate an id."""
        while True:
            id_ = utils.gen_id()
            self.cur.execute(
                "select %s from %s where %s=%s" %
                (id_name, table_name, id_name, id_)
            )
            if single_unpack(self.cur.fetchone()) is None:
                break
        return int(id_)

    def check_exists(
        self, field, table_name, wcb=None,
        abort_text='%s not found', abort_response=404):
        # wcb = WhereClauseBuilder
        dbutils.simple_select(
            self.cur, table_name, (field, ), wcb=wcb)
        if single_unpack(self.cur.fetchone()) is None:
            abort(404, abort_text % field)

    def get(self, endpoint, headers={'Accept': 'application/json'}):
        """Get request for and endpoint."""
        target_url = f"{self.url}{endpoint}"
        response = requests.get(target_url, headers=headers)
        return response

    def post(self, endpoint, data=None, headers={
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }):
        """Post request to an endpoint."""
        target_url = f"{self.url}{endpoint}"
        if data is None:
            response = requests.post(target_url, headers=headers)
        else:
            response = requests.post(target_url, headers=headers, json=data)
        return response

    def put(self, endpoint, data=None, headers={
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }):
        """Post request to an endpoint."""
        target_url = f"{self.url}{endpoint}"
        if data is None:
            response = requests.put(target_url, headers=headers)
        else:
            response = requests.put(target_url, headers=headers, json=data)
        return response

    def owns_repo(self, uid, repoid):
        """Check the user owns the repo."""
        self.cur.execute(f"""select uid from repo_owners
                    where uid = %s and repoid = %s""",
                    (uid, repoid))
        uid = single_unpack(self.cur.fetchone())
        if uid is None:
            abort(403, ("You are not registered as an owner of this repo"))

    def not_removed(self, repoid):
        self.cur.execute(
            f"""SELECT repoid
            FROM repos
            WHERE removed = FALSE
            AND repoid = {repoid}""")
        repoid = single_unpack(self.cur.fetchone())
        if repoid is None:
            abort(410, ("The repo has been removed."))
