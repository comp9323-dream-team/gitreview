import itertools
import config

def simple_insert(cur, table_name, fields, **kwargs):
  def gen_row_param_str():
    return f"({','.join(['%s']*len(fields))})"

  if kwargs.get("value", None) is not None:
    # assuming a single row tuple
    values_str = gen_row_param_str()
    param_args = kwargs["value"]
    if type(param_args) is not tuple:
      param_args = tuple(param_args)
  elif kwargs.get("values", None) is not None:
    param_args = tuple(itertools.chain(*kwargs["values"]))
    if len(param_args) % len(fields) != 0:
      raise RuntimeError("Invalid field count.")
    values_str = ",".join([gen_row_param_str()] * (len(param_args) // len(fields)))
  else:
    raise RuntimeError("Supply either 'value' (for single row) or 'values' (for multiple rows).")

  if len(param_args) > 0:
    query = f"insert into {table_name} ({','.join(fields)}) values {values_str}"
    cur.execute(query, param_args)
    
def simple_select(cur, table_name, cols, wcb=None, wc=None, limit=None, orderby=None):
  if wc is None:
    wc = ""
    if wcb is not None:
      wc = f"where {wcb.phrase()}"
  elif wcb is not None:
    raise ValueError("Cannot have both wcb and wc set.")

  obc = "" if orderby is None or len(orderby) == 0 else f'order by {",".join(orderby)}'

  if type(table_name) is not str:
    # assume iterable
    table_name = ','.join(table_name)

  lc = f"limit {limit}" if limit is not None else ""
  q = f"select {','.join(cols)} from {table_name} {wc} {lc} {obc}"
  cur.execute(q, tuple() if wcb is None else tuple(wcb.values()))

def simple_update(cur, table_name, valuepairs, wcb=None):
  wc = ""
  if wcb is not None:
    wc = f"where {wcb.phrase()}"
  setphrase = ','.join(f'{field}=%s' for field,_ in valuepairs)
  q = f"update {table_name} set {setphrase} {wc}"
  params = [v for _,v in valuepairs]
  if wcb is not None:
    params.extend(wcb.values())
  cur.execute(q, params)

class WhereClauseBuilder:
  def __init__(self, expr=None, value=None, values=None):
    self._data = []
    self._values = []
    if expr is not None:
      self._add_clause(expr, value, values)

  def _add_clause(self, expr, value, values):
    self._data.append(f"({expr})")
    if value is not None:
      if values is not None:
        raise ValueError("specify either value or values!")
      self._values.append(value)
    elif values is not None:
      self._values.extend(values)

  def and_(self, expr, value=None, values=None):
    self._add_clause(expr, value, values)
    return self

  def phrase(self):
    return ' AND '.join(self._data)

  def values(self):
    return (i for i in self._values)

  def modify_values(self, *args):
    if len(args) != len(self._values):
      raise ValueError("Values count mismatch!")
    for i,v in enumerate(args):
      self._values[i] = v

  def __len__(self):
    """Number of clauses added (might be different from number of values)"""
    return len(self._data)

class BulkInserter:
  def __init__(self, cur, table_name, fields, capacity=None):
    self._capacity = capacity or config.SEARCH_RESULT_MAX
    self._table_name = table_name
    self._fields = fields
    self._store = []
    self._cur = cur

  def add_item(self, row):
    self._store.append(row)
    if len(self._store) >= self._capacity:
      self.commit()

  def commit(self):
    simple_insert(self._cur, self._table_name, self._fields, values=self._store)
    self._store.clear()

def single_unpack(v):
  """Extract the first element of a tuple/array (with None check)."""
  if v is None:
    return None
  (ret,) = v
  return ret