'''
Helper library for tags specific

'''
import helpers.dbutils as dbutils
from helpers.request_handlers import single_unpack
from flask import abort

def get_repo_tags(cur, repoid):
  dbutils.simple_select(cur, "repo_tags", ("tid",), wcb=dbutils.WhereClauseBuilder("repoid=%s", repoid))
  repo_tids = []
  for (tid,) in cur:
    repo_tids.append(tid)
    
  return repo_tids

def get_user_tags(cur, uid):
  dbutils.simple_select(cur, "user_tags", ("tid",), wcb=dbutils.WhereClauseBuilder("uid=%s", uid))
  user_tids = []
  for (tid,) in cur:
    user_tids.append(tid)