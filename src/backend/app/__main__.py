from dotenv import load_dotenv
load_dotenv(".env")

from flask import Flask,request,jsonify,make_response,abort
from flask_restx import Api
from flask_cors import CORS
from werkzeug.exceptions import HTTPException
import config

# import handlers (the one that has namespaces + route assignments) here
import handlers.auth
import handlers.accountmgmt

import os
from mainapp import app

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=config.BACKEND_PORT)
