from dotenv import load_dotenv
import os
import helpers.utils as utils

load_dotenv(".env")

# configs that doesn't depend on a particular system configuration
PASSPHRASE_MIN_LEN=6
PASSPHRASE_MAX_LEN=511
PASSWORD_SALT_BYTES=32
SESSION_EXPIRY_DAYS=3
SEARCH_RESULT_MAX=200
UPVOTE=5
DOWNVOTE=4
MAX_REVIEWER=5
IMAGE_MAX_WIDTH=1600
IMAGE_MAX_HEIGHT=1200

# need to load these configs from another source (env. variable)
# all of these have to be defined in the env, prefixed with "GITREVIEW_"
# either a name, or a tuple of (name, type)
_vars=[
  ("BACKEND_PORT",int),
  "SESSION_DIR",
  "QUEUE_DIR",
  "RESOURCES_DIR",
  "POSTGRES_USER",
  "POSTGRES_PASSWORD",
  "POSTGRES_DB",
  "POSTGRES_HOST",
  ("POSTGRES_PORT",(lambda v : int(v) if v.isdigit() else None)),
  "SMTP_SERVER",
  ("SMTP_PORT",int),
  "SMTP_USERNAME",
  "SMTP_SENDER",
  "SMTP_PASSWORD",
  "SVCURL",
  "WEBURL",
  "MAILERURL"
]

# array of optinal values. tuples: (name, type, default value)
_opt_vars=[
  ("RESET_PASSWORD_SKIP_EMAIL", utils.truthy_string, False)
]

for optional, vars in [(False, _vars), (True, _opt_vars)]:
  for var in vars:
    if type(var) is str:
      varname = var
      vartype = str
    elif type(var) is tuple:
      varname = var[0]
      vartype = var[1]
    else:
      raise RuntimeError("Unknown variable definition format.")
    try:
      varval = os.environ[varname]
    except KeyError as ex:
      if not optional:
        raise ex
      else:
        varval = var[2]
    varval = vartype(varval)
    globals()[varname] = varval
  
