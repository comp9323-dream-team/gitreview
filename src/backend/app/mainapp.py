from flask import Flask,request,jsonify,make_response,abort
from flask_restx import Api
from flask_cors import CORS
from werkzeug.exceptions import HTTPException
import config

app = Flask(__name__)
# app configs
app.config['ERROR_404_HELP'] = False
# allow different domain to access this web service
CORS(app)
api = Api(app)

@app.errorhandler(HTTPException)
def handle_exception(e):
    """Return JSON instead of HTML for HTTP errors."""
    return make_response(jsonify({
        "message": e.description,
    }), e.code)

# import handlers (the one that has namespaces + route assignments) here
import handlers.auth
import handlers.accountmgmt
import handlers.repo
import handlers.rfr
import handlers.review
import handlers.reviewer
import handlers.photomgr
import handlers.tag