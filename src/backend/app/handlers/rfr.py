"""Handles RFRs."""

from mainapp import api
import models
from flask import request
from flask_restx import Resource, abort
from datetime import datetime

import helpers.validators as validators
import helpers.utils as utils
import helpers.dbman as db
import helpers.dbutils as dbutils
import helpers.auth as auth
import config
from helpers.request_handlers import *
from helpers.repo_helpers import forward, owns_repo, is_repo_exist
from helpers.repo_helpers import API_helper
from helpers.rfr_helpers import get_rfr_repo, get_rfr_type, is_rfr_exist, is_rfr_open, is_rfr_owner
from helpers.account_helper import get_userid, get_username, validate_username


git_url = config.SVCURL
helper = API_helper(git_url)

META_RFR = ("branch", "desc", "data")

rfrmgr = api.namespace('rfr', description="RFR management")


def validate_rfr_meta(body):
    """Validate the rfr meta field is formatted correctly."""
    veb = utils.ValidationErrorListBuilder()
    validators.validate_string(
        "type", body["type"], minlen=2, maxlen=2, veb=veb)

    for meta in META_RFR:
        validators.validate_string(
            meta, body[meta], default="", minlen=0, maxlen=None, veb=veb)

    if len(veb):
        abort(400, veb_reformat(veb))


@rfrmgr.route('/byrepo/<repoid>')
class NewRFR(Resource):
    """
    Handles a single RFR.

    Uses the following gitsvc endpoints:
    - '/<repoid>/rfr'
    - '/<repoid>/rfr/<rfrid>'
    """

    @rfrmgr.doc(
        description=(
            "Endpoint to create RFR. On success returns HTTP 201 with string "
            "'success'"))
    @rfrmgr.expect(models.new_rfr_meta)
    @rfrmgr.response(201, 'New RFR created', models.rfr_post_output)
    @rfrmgr.response(
        400, 'Wrong input format or input does not pass validation',
        models.validation_error_list)
    @rfrmgr.response(409, 'RFR for current branch already exist')
    def post(self, repoid):
        """Create an RFR."""
        body = get_request_body()
        validate_rfr_meta(body)

        uid = auth.authenticate(request)

        with db.get_dbconn() as conn:
            cur = conn.cursor()

            # Check if repo exist
            is_repo_exist(cur, repoid)

            # generate rfr id
            while True:
                rfrid = utils.gen_id()
                cur.execute("select rfrid from rfrs where rfrid=%s", (rfrid,))
                if single_unpack(cur.fetchone()) is None:
                    break

            create_date = datetime.now()
            # unload data from request
            branch, desc, type = dict_unpack(
                get_request_body(), "branch", "desc", "type", required=True)

            # create RFR of base commit
            if type == 'bo':

                # check if user is owner of the repo
                owns_repo(cur, uid, repoid)

                # check if base commit already have an RFR
                wcb = dbutils.WhereClauseBuilder("repoid=%s", repoid)
                wcb.and_("type=%s", type)
                wcb.and_("base_commit=%s", branch)
                dbutils.simple_select(cur, "rfrs", ("rfrid",), wcb=wcb)
                if single_unpack(cur.fetchone()) is not None:
                    abort(409, "RFR of this branch already exist")

                # send create base commit rfr to gitreview
                msg = {
                    "id": rfrid,
                    "basecommit": branch
                }
                endpoint = f"/repo/{repoid}/rfr"
                response = helper.post(endpoint, msg)
                if response.status_code != 200:
                    return forward(response)

                dbutils.simple_insert(
                    cur, "rfrs",
                    ("rfrid", "description", "createdate", "uid", "repoid",
                        "type", "base_commit"),
                    value=(
                        rfrid, desc, create_date.strftime("%Y-%m-%d %H:%M:%S"),
                        uid, repoid, type, branch))

            # create RFR target commit
            elif type == 'tc':
                data = body["data"]

                # check if user is owner of the repo
                owns_repo(cur, uid, repoid)

                # check if base commit already have an RFR
                wcb = dbutils.WhereClauseBuilder("repoid=%s", repoid)
                wcb.and_("type=%s", type)
                wcb.and_("base_commit=%s", branch)
                wcb.and_("target_commit=%s", data)
                dbutils.simple_select(cur, "rfrs", ("rfrid",), wcb=wcb)
                if single_unpack(cur.fetchone()) is not None:
                    abort(409, "RFR of this branch already exist")

                # send create target commit rfr to gitsvc
                msg = {
                    "id": rfrid,
                    "basecommit": branch
                }
                endpoint = f"/repo/{repoid}/rfr"
                response = helper.post(endpoint, msg)
                if response.status_code != 200:
                    return forward(response)

                msg2 = {
                    "type": type,
                    "data": data
                }
                endpoint = f"/repo/{repoid}/rfr/{rfrid}"
                response = helper.post(endpoint, msg2)
                if response.status_code != 200:
                    return forward(response)

                dbutils.simple_insert(
                    cur, "rfrs",
                    ("rfrid", "description", "createdate", "uid", "repoid",
                        "type", "base_commit", "target_commit"),
                    value=(
                        rfrid, desc, create_date.strftime("%Y-%m-%d %H:%M:%S"),
                        uid, repoid, type, branch, data))

            # create RFR patchset
            elif type == 'ps':
                data = body["data"]

                # send create patchset rfr to gitsvc
                msg = {
                    "id": rfrid,
                    "basecommit": branch
                 }
                endpoint = f"/repo/{repoid}/rfr"
                response = helper.post(endpoint, msg)
                if response.status_code != 200:
                    return forward(response)

                msg2 = {
                    "type": type,
                    "data": data
                }
                endpoint = f"/repo/{repoid}/rfr/{rfrid}"
                response = helper.post(endpoint, msg2)
                if response.status_code != 200:
                    return forward(response)

                dbutils.simple_insert(
                    cur, "rfrs",
                    ("rfrid", "description", "createdate", "uid", "repoid",
                        "type"),
                    value=(
                        rfrid, desc, create_date.strftime("%Y-%m-%d %H:%M:%S"),
                        uid, repoid, type))

            else:
                abort(code=404, message="RFR type not found")

        return {'rfrid': rfrid}

    @rfrmgr.doc(description="List the RFRs in a repo.")
    @rfrmgr.response(200, "List of RFRs", models.rfr_list)
    @rfrmgr.response(204, 'The repo has no RFRs')
    @rfrmgr.response(404, "Repo ID not found.")
    def get(self, repoid):
        """List the RFRs in a repo."""
        # = auth.authenticate(request)

        rfrs = []
        with db.get_dbconn() as conn:
            cur = conn.cursor()

            # Check if the repo is exist
            is_repo_exist(cur, repoid)

            # collect all rfrs data
            dbutils.simple_select(
                cur, "rfrs",
                ("rfrid", "uid", "createdate", "status", "rate", "type",
                    "description"),
                wcb=dbutils.WhereClauseBuilder("repoid=%s", repoid),
                orderby=("createdate",))

            # Map rfrs data to be send to frontend
            cur2 = conn.cursor()
            for (rfrid, uid, create_date, status, rate, type, desc) in cur:
                rfr = {
                    "rfrid": rfrid,
                    "owner": get_username(cur2, uid),
                    "create_date": create_date.strftime("%Y-%m-%d"),
                    "status": status,
                    "rate": rate,
                    "type": type,
                    "desc": desc
                }

                rfrs.append(rfr)

        if len(rfrs) == 0:
            return '', 204
        return {'rfr_list': rfrs}


@rfrmgr.route('/<rfrid>')
class Rfr(Resource):
    """Handles a single RFR."""

    @rfrmgr.doc(
        description=(
            "Edit an RFR description. On success returns HTTP 200 with string"
            " 'success'"))
    @rfrmgr.expect(models.rfr_desc_model)
    @rfrmgr.response(200, 'RFR description succesfully updated')
    @rfrmgr.response(
        400, 'Wrong input format or input does not pass validation',
        models.validation_error_list)
    @rfrmgr.response(404, 'rfr ID not Found')
    @rfrmgr.response(405, 'Not allowed to update RFR')
    def put(self, rfrid):
        """Update an RFR."""
        uid = auth.authenticate(request)
        body = get_request_body()

        # validate the text update
        veb = utils.ValidationErrorListBuilder()
        new_desc = validators.validate_string(
            "desc", body["desc"], minlen=1, veb=veb)
        if len(veb):
            abort(400, "Description is not a text")

        with db.get_dbconn() as conn:
            cur = conn.cursor()

            # Check whether the RFR exists
            is_rfr_exist(cur, rfrid)

            # Check whether the user owns the RFR
            is_rfr_owner(cur, uid, rfrid)

            # Check the RFR is open
            is_rfr_open(cur, rfrid)

            # Check whether the repo exists
            cur.execute(f"""
            SELECT rfrs.description
            FROM rfrs
            INNER JOIN repos
            ON rfrs.repoid=repos.repoid
            WHERE repos.removed=FALSE
            AND rfrs.uid={uid}
            ORDER BY rfrs.createdate DESC
            """)

            # get old decsription as comparison
            old_desc = single_unpack(cur.fetchone())
            if old_desc is None:
                abort(404, "The repo does not exist.")

            # update description in DB if there are changes
            if old_desc != new_desc:
                dbutils.simple_update(
                    cur, "rfrs", [("description", new_desc)],
                    wcb=dbutils.WhereClauseBuilder("rfrid=%s", rfrid))

        return "Success", 200

    @rfrmgr.doc(description="Get an individual RFR.")
    @rfrmgr.response(200, "RFR information", models.rfr_list)
    @rfrmgr.response(404, "RFR ID not found.")
    def get(self, rfrid):
        with db.get_dbconn() as conn:
            cur = conn.cursor()

            dbutils.simple_select(
                cur, "rfrs",
                ("rfrid", "uid", "createdate", "status", "rate", "type",
                    "description", "repoid"),
                wcb=dbutils.WhereClauseBuilder("rfrid=%s", rfrid))

            row = cur.fetchone()
            if row is None:
                abort(404, "RFR not found")

            (rfrid, uid, create_date, status, rate, type, desc, repoid) = row
            return {
                "rfrid": rfrid,
                "owner": get_username(cur, uid),
                "create_date": create_date.strftime("%Y-%m-%d"),
                "status": status,
                "rate": rate,
                "type": type,
                "desc": desc,
                "repoid": repoid
            }

@rfrmgr.route('/<rfrid>/details')
class RfrDetails(Resource):
    @rfrmgr.doc(description="Get the technical info of the RFR.")
    @rfrmgr.response(200, "Technical RFR information.")
    @rfrmgr.response(404, "RFR ID not found.")
    def get(self, rfrid):
        with db.get_dbconn() as conn:
            cur = conn.cursor()

            dbutils.simple_select(
                cur, "rfrs",
                ("repoid",),
                wcb=dbutils.WhereClauseBuilder("rfrid=%s", rfrid))

            repoid = single_unpack(cur.fetchone())
            if repoid is None:
                abort(404, "RFR not found")

            endpoint = f"/repo/{repoid}/rfr/{rfrid}"
            response = helper.get(endpoint)
            return forward(response)


@rfrmgr.route('/<rfrid>/status')
class RfrStatus(Resource):
    """
    Handles RFR status (approval, rejection etc.).

    Uses gitsvc endpoint: ​'/repo​/{repoid}​/rfr​/{rfrid}​/close'
    """

    @rfrmgr.doc(
        description=(
            "Update an RFR status (close), On success returns HTTP 204 with "
            "string 'success'"))
    @rfrmgr.expect(models.rfr_status)
    @rfrmgr.response(200, 'RFR description succesfully updated')
    @rfrmgr.response(
        400, 'Wrong input format or input does not pass validation',
        models.validation_error_list)
    @rfrmgr.response(404, 'rfr ID not Found')
    @rfrmgr.response(404, 'Not allowed to update RFR')
    def put(self, rfrid):
        """Update the status of an RFR."""
        uid = auth.authenticate(request)
        body = get_request_body()

        with db.get_dbconn() as conn:
            cur = conn.cursor()

            # Check whether the RFR exists
            is_rfr_exist(cur, rfrid)

            # Check whether the user owns the RFR
            is_rfr_owner(cur, uid, rfrid)

            # Check the RFR is open
            is_rfr_open(cur, rfrid)

            # Check whether the repo exists
            cur.execute(f"""
            SELECT rfrs.repoid, rfrs.status
            FROM rfrs
            INNER JOIN repos
            ON rfrs.repoid=repos.repoid
            WHERE repos.removed=FALSE
            AND rfrs.uid={uid}
            ORDER BY rfrs.createdate DESC
            """)
            output = [x for x in cur]
            if len(output) == 0:
                abort(404, "The repo does not exist.")
            repoid, old_status = output[0]

            # Check the user owns the repo
            owns_repo(cur, uid, repoid)

            # check if the RFR status is still open
            if old_status != 'open':
                abort(405, "Failed to update RFR")

            # validate the status update
            veb = utils.ValidationErrorListBuilder()
            new_status = validators.validate_string(
                "desc", body["status"], minlen=8, maxlen=8, veb=veb)
            branch = validators.validate_string(
                "desc", body["branch"], minlen=0, veb=veb)
            if len(veb):
                abort(400, "Status is not a text")

            endpoint = f"/repo/{repoid}/rfr/{rfrid}/close"
            if new_status == 'accepted':
                msg2 = {
                    "branch": branch,
                    "merge": 'true'
                }
                response = helper.post(endpoint, msg2)
                if response.status_code != 200:
                    return forward(response)
            else:
                msg2 = {
                    "branch": branch,
                    "merge": 'false'
                }
                response = helper.post(endpoint, msg2)
                if response.status_code != 200:
                    return forward(response)

            dbutils.simple_update(
                cur, "rfrs", [("status", new_status)],
                wcb=dbutils.WhereClauseBuilder("rfrid=%s", rfrid))

        return "Success", 200


@rfrmgr.route('/other/<username>')
class listCurrentUserRFR(Resource):
    """Handles an arbitrary user's RFRs."""

    @rfrmgr.doc(
        description="Endpoint to list all user's RFRs based on username.")
    @rfrmgr.response(200, 'Successfully got all RFRs', models.rfr_list)
    @rfrmgr.response(204, 'The user has no RFRs')
    @rfrmgr.response(
        400, 'Wrong input format or input does not pass validation',
        models.validation_error_list)
    def get(self, username):
        """Get a user's RFRs."""
        username = validate_username(username)

        with db.get_dbconn() as conn:
            cur = conn.cursor()

            uid = get_userid(cur, username)

            rfrs = []

            # collect all rfrs data
            cur.execute(f"""
                SELECT rfrs.rfrid, rfrs.createdate, rfrs.status,
                rfrs.rate, rfrs.type, rfrs.description
                FROM rfrs
                INNER JOIN repos
                ON rfrs.repoid=repos.repoid
                WHERE repos.removed=FALSE
                AND uid={uid}
                ORDER BY createdate DESC
            """)

            # Map rfrs data to be send to frontend
            for (rfrid, create_date, status, rate, type, desc) in cur:
                rfr = {
                    "rfrid": rfrid,
                    "create_date": create_date.strftime("%Y-%m-%d"),
                    "status": status,
                    "rate": rate,
                    "type": type,
                    "desc": desc
                }

                rfrs.append(rfr)

            if len(rfrs) == 0:
                return '', 204
            return {'rfr_list': rfrs}, 200


@rfrmgr.route('/my')
class listUserRFR(Resource):
    """Handles the logged in user's RFRs."""

    @rfrmgr.doc(description="Endpoint to list all user owned RFRs.")
    @rfrmgr.response(200, "Success get all the user's RFRs", models.rfr_list)
    @rfrmgr.response(
        400, 'Wrong input format or input does not pass validation',
        models.validation_error_list)
    def get(self):
        """Get all your RFRs."""
        uid = auth.authenticate(request)

        with db.get_dbconn() as conn:
            cur = conn.cursor()

            rfrs = []

            # collect all rfrs data
            cur.execute(f"""
                SELECT rfrs.rfrid, rfrs.createdate, rfrs.status,
                rfrs.rate, rfrs.type, rfrs.description
                FROM rfrs
                INNER JOIN repos
                ON rfrs.repoid=repos.repoid
                WHERE repos.removed=FALSE
                AND uid={uid}
                ORDER BY createdate DESC
            """)

            # Map rfrs data to be send to frontend
            for (rfrid, create_date, status, rate, type, desc) in cur:
                rfr = {
                    "rfrid": rfrid,
                    "create_date": create_date.strftime("%Y-%m-%d"),
                    "status": status,
                    "rate": rate,
                    "type": type,
                    "desc": desc
                }

                rfrs.append(rfr)

            if len(rfrs) == 0:
                return '', 204
            return {'rfr_list': rfrs}


@rfrmgr.route('/<rfrid>/browse')
class listUserRFR(Resource):
    @rfrmgr.doc(description="Browse RFR.")
    @rfrmgr.response(200, "Browse through RFR.")
    @rfrmgr.response(404, "RFR ID not found.")
    def get(self, rfrid):
        with db.get_dbconn() as conn:
            cur = conn.cursor()
            is_rfr_exist(cur, rfrid)
            is_rfr_open(cur, rfrid)
            
            repoid = get_rfr_repo(cur, rfrid)
            
            type = get_rfr_type(cur, rfrid)
            
            if type == 'bo':
                return forward(helper.get(f"/repo/{repoid}/rfr/{rfrid}/0/browse"))
            else:
                return forward(helper.get(f"/repo/{repoid}/rfr/{rfrid}/1/browse"))
                
@rfrmgr.route('/<rfrid>/download')
class listUserRFR(Resource):
    @rfrmgr.doc(description="Download RFR.")
    @rfrmgr.response(200, "Downloaded RFR information.")
    @rfrmgr.response(404, "RFR ID not found.")
    def get(self, rfrid):
        with db.get_dbconn() as conn:
            cur = conn.cursor()
            is_rfr_exist(cur, rfrid)
            is_rfr_open(cur, rfrid)
            
            repoid = get_rfr_repo(cur, rfrid)
            
            type = get_rfr_type(cur, rfrid)
            
            if type == 'bo':
                return forward(helper.get(f"/repo/{repoid}/rfr/{rfrid}/0/download"))
            else:
                return forward(helper.get(f"/repo/{repoid}/rfr/{rfrid}/1/download"))

@rfrmgr.route('/<rfrid>/item/<path:path>')
class listUserRFR(Resource):
    @rfrmgr.doc(description="Get specific data on RFR.")
    @rfrmgr.response(200, "Specific data on RFR.")
    @rfrmgr.response(404, "RFR ID not found.")
    def get(self, rfrid, path):
        with db.get_dbconn() as conn:
            cur = conn.cursor()
            is_rfr_exist(cur, rfrid)
            is_rfr_open(cur, rfrid)
            
            repoid = get_rfr_repo(cur, rfrid)
            
            type = get_rfr_type(cur, rfrid)
            
            if type == 'bo':
                return forward(helper.get(f"/repo/{repoid}/rfr/{rfrid}/0/item/{path}"))
            else:
                return forward(helper.get(f"/repo/{repoid}/rfr/{rfrid}/1/item/{path}"))
    
