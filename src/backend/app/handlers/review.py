"""Handles reviews and review comments."""

import psycopg2
import psycopg2.errors
from mainapp import api
import models
from flask import request
from flask_restx import Resource, abort
from datetime import datetime

import helpers.validators as validators
import helpers.utils as utils
import helpers.dbman as db
import helpers.dbutils as dbutils
import helpers.auth as auth
import config
from helpers.request_handlers import *
from helpers.repo_helpers import forward
from helpers.repo_helpers import API_helper
from helpers.rfr_helpers import is_rfr_exist
from helpers.review_helpers import *
from helpers.tags_helper import get_repo_tags, get_user_tags
from helpers.account_helper import get_userid, get_username, validate_username

reviewmgr = api.namespace('review', description="Review management")
git_url = config.SVCURL
helper = API_helper(git_url)


@reviewmgr.route('/byrfr/<rfrid>')
class RfrReviews(Resource):
    """Handles reviews for a single RFR."""

    @reviewmgr.doc(description="Create new review, and returns review id")
    @reviewmgr.expect(models.new_review_meta)
    @reviewmgr.response(201, 'New Review created', models.revid_response)
    @reviewmgr.response(
        400, 'Wrong input format or input does not pass validation',
        models.validation_error_list)
    @reviewmgr.response(403, 'Forbidden access')
    @reviewmgr.response(409, 'You already reviwed this RFR')
    def post(self, rfrid):
        """Create a new review."""
        uid = auth.authenticate(request)
        body = get_request_body()

        veb = utils.ValidationErrorListBuilder()
        overall = validators.validate_string(
            "overall_review", body["overall_review"], minlen=1, veb=veb)
        if len(veb):
            abort(400, veb_reformat(veb))

        with db.get_dbconn() as conn:
            cur = conn.cursor()

            # check whether the RFR exist or not
            is_rfr_exist(cur, rfrid)

            # check whether the reviewer is the owner of RFR
            wcb = dbutils.WhereClauseBuilder("rfrid=%s", rfrid)
            dbutils.simple_select(
                cur, "rfrs", ("uid", "repoid", "type"), wcb=wcb)
            owner, repoid, type = cur.fetchone()
            if uid == owner:
                abort(403, "You cannot review your own RFR")

            # generate review id
            while True:
                revid = utils.gen_id()
                cur.execute(
                    "select revid from reviews where revid=%s", (revid,))
                if single_unpack(cur.fetchone()) is None:
                    break

            # setting up revision number
            if type == 'bo':
                rfrrev = 0
            else:
                rfrrev = 1

        # preparing meta data
        create_date = datetime.now()
        inline = body["inline_reviews"]

        # send inline review to gitsvc
        msg = {"inline_reviews": inline}
        endpoint = f"/repo/{repoid}/rfr/{rfrid}/{rfrrev}/review/{revid}"
        response = helper.post(endpoint, msg)
        if not response.ok:
            return forward(response)

        with db.get_dbconn() as conn:
            cur = conn.cursor()
            # get tags id of the repo
            repo_tids = get_repo_tags(cur, repoid)
            repo_tids = set(repo_tids)

            # get user tags
            user_tids = get_user_tags(cur, uid)
            user_tids = set([] if user_tids is None else user_tids)

            # Get tags that user don't have yet
            diff = repo_tids.difference(user_tids)

            # Insert new tags to user tags db
            for tid in diff:
                # check if this user-tag pair exists
                wcb = dbutils.WhereClauseBuilder('uid=%s', uid)
                wcb.and_('tid=%s', tid)
                dbutils.simple_select(cur, 'user_tags', ('count(*)',),
                    wcb=wcb)
                if single_unpack(cur.fetchone()) == 0:
                    # apparently it is not. let's create one!
                    dbutils.simple_insert(
                        cur, "user_tags", ("uid", "tid", "karma"),
                        value=(uid, tid, 0))

            # Insert data to the DB
            dbutils.simple_insert(
                cur, "reviews",
                ("revid", "review", "uid", "rfrid", "createdate"),
                value=(
                    revid, overall, uid, rfrid,
                    create_date.strftime("%Y-%m-%d %H:%M:%S")
                ))

        return {'revid': revid}

    @reviewmgr.doc(description="list of reviews of an RFR.")
    @reviewmgr.response(
        200,
        """Returns all the reviews in a list of dicts.
        Values: revid, review, create_date, status and published.""")
    @reviewmgr.response(204, "No reviews to return")
    @reviewmgr.response(404, "RFR ID not found.")
    def get(self, rfrid):
        """List the reviews of an RFR."""
        reviews = []
        with db.get_dbconn() as conn:
            cur = conn.cursor()

            # check if the RFR is exist
            is_rfr_exist(cur, rfrid)

            # collection reviews data from the DB
            dbutils.simple_select(
                cur, "reviews",
                ("revid", "uid", "review", "createdate", "status", "is_published"),
                wcb=dbutils.WhereClauseBuilder("rfrid=%s", rfrid),
                orderby=("createdate",))

            # preparing data to be sent to the frontend
            for (revid, reviewer, review, create_date, status, published) in cur:
                if status != 'deleted':
                    review = {
                        "revid": revid,
                        "reviewer" : reviewer,
                        "review": review,
                        "create_date": create_date.strftime("%Y-%m-%d"),
                        "status": status,
                        "published": published
                    }

                    reviews.append(review)
            
            for review in reviews:
                review["reviewer"] = get_username(cur, review["reviewer"])

        if len(reviews) == 0:
            return '', 204
        return reviews, 200


@reviewmgr.route('/publish/<revid>')
class Publish(Resource):
    """Handles publishing reviews."""

    @reviewmgr.doc(
        description=(
            "Publish a Review, On success returns HTTP 200 with string "
            "'success'"))
    @reviewmgr.response(200, 'Review succesfully published')
    @reviewmgr.response(
        400, 'Wrong input format or input does not pass validation',
        models.validation_error_list)
    @reviewmgr.response(401, 'Unauthorized acess')
    @reviewmgr.response(404, 'Review ID not Found')
    def put(self, revid):
        """Publish review."""
        uid = auth.authenticate(request)

        with db.get_dbconn() as conn:
            cur = conn.cursor()

            # check whether the one try to publish in the owner of the review
            dbutils.simple_select(
                cur, "Reviews", ("uid",),
                wcb=dbutils.WhereClauseBuilder("revid=%s", revid))
            if single_unpack(cur.fetchone()) != uid:
                abort(401, "You are not authorized to change this review")

            # check whether the review exist or not
            is_review_exist(cur, revid)

            # update the publish status on DB
            dbutils.simple_update(
                cur, "reviews", [("is_published", 'true')],
                wcb=dbutils.WhereClauseBuilder("revid=%s", revid))

        return "Success", 200


@reviewmgr.route('/<revid>')
class Reviews(Resource):
    """Handles a single review."""

    @reviewmgr.doc(description=(
        "Add inline review into existing review, On success returns HTTP 204 "
        "with string 'success'"))
    @reviewmgr.response(200, 'Review succesfully updated')
    @reviewmgr.response(
        400, 'Wrong input format or input does not pass validation')
    @reviewmgr.response(401, 'Unauthorized acess')
    @reviewmgr.response(404, 'Review ID not Found')
    @reviewmgr.response(405, 'Not allowed to update the review')
    @reviewmgr.expect(models.new_review_meta)
    def put(self, revid):
        """Update a review."""
        uid = auth.authenticate(request)
        body = get_request_body()

        with db.get_dbconn() as conn:
            cur = conn.cursor()

            # check whether the review exist or not
            is_review_exist(cur, revid)

            # Check whether the one try to update is the owner and is the
            # status is still open to update
            dbutils.simple_select(
                cur, "Reviews", ("uid", "status"),
                wcb=dbutils.WhereClauseBuilder("revid=%s", revid))
            owner, status = cur.fetchone()
            if owner != uid:
                abort(401, "Not authorized to update this review")

            if status == 'deleted':
                abort(404, "Review not found")
            elif status == 'accepted' or status == 'rejected':
                abort(405, "Review cannot be changed")

            # chehck whether the review is published or not
            dbutils.simple_select(
                cur, "Reviews", ("is_published", "reviews", "rfrid"),
                wcb=dbutils.WhereClauseBuilder("revid=%s", revid))
            published, review, rfrid = cur.fetchone()
            if not published:
                veb = utils.ValidationErrorListBuilder()
                overall = validators.validate_string(
                    "overall_review", body["overall_review"], minlen=1,
                    veb=veb)
                if len(veb):
                    abort(400, "Review is not a text")

                # check if there are any differences between existing ovwrall
                # review to the new review, if there is, save the new review in
                # the DB
                if overall != review:
                    dbutils.simple_update(
                        cur, "reviews", [("review", overall)],
                        wcb=dbutils.WhereClauseBuilder("revid=%s", revid))

            dbutils.simple_select(
                cur, "rfrs", ("repoid", "type"),
                wcb=dbutils.WhereClauseBuilder("rfrid=%s", rfrid))
            repoid, type = cur.fetchone()

            # setting up revision number
            if type == 'bo':
                rfrrev = 0
            else:
                rfrrev = 1

            # send inline review to gitsvc
            inline = body["inline_reviews"]
            msg = {"inline_reviews": inline}
            endpoint = f"/repo/{repoid}/rfr/{rfrid}/{rfrrev}/review/{revid}"
            response = helper.put(endpoint, msg)
            if not response.ok:
                return forward(response)
        return "Success", 200

    @reviewmgr.doc(description="Get review data")
    @reviewmgr.response(200, 'Success retrieving review data')
    @reviewmgr.response(
        400, 'Wrong input format or input does not pass validation')
    @reviewmgr.response(404, 'Review ID not Found')
    def get(self, revid):
        """Get the data for a single review."""
        uid = auth.authenticate(request)

        with db.get_dbconn() as conn:
            cur = conn.cursor()

            # check whether the review exist or not
            is_review_exist(cur, revid)

            # get owner and publication status of a review
            dbutils.simple_select(
                cur, "Reviews",
                ("uid", "rfrid", "review", "is_published", "status"),
                wcb=dbutils.WhereClauseBuilder("revid=%s", revid))
            owner, rfrid, review, published, status = cur.fetchone()

            if status == 'deleted':
                abort(404, "Review no longer exist")

            # taking data from DB
            dbutils.simple_select(
                cur, "rfrs", ("repoid", "type"),
                wcb=dbutils.WhereClauseBuilder("rfrid=%s", rfrid))
            repoid, type = cur.fetchone()

            # setting up revision number
            if type == 'bo':
                rfrrev = 0
            else:
                rfrrev = 1

            # get owner username
            wcb = dbutils.WhereClauseBuilder("uid=%s", owner)
            wcb.and_("field=%s", "username")
            dbutils.simple_select(cur, "userprofiles", ("data",), wcb=wcb)
            owner_name = single_unpack(cur.fetchone())

            ret = {
                "owner": owner_name,
                "published": published,
                "status": status,
                "overall_review": review,
                "rfrid": rfrid,
            }

            # get inline reviews from gitsvc
            endpoint = f"/repo/{repoid}/rfr/{rfrid}/{rfrrev}/review/{revid}"
            response = helper.get(endpoint)
            if not response.ok:
                return forward(response)

            ret["inline_reviews"] = response.json()

            if owner == uid:
                return ret
            else:
                if published:
                    return ret
                else:
                    abort(404, "Review is not published")


    @reviewmgr.doc(description=(
        "Delete a review. On success returns HTTP 204 with string "
        "'success'"))
    @reviewmgr.response(202, 'Success delete review')
    @reviewmgr.response(
        400, 'Wrong input format or input does not pass validation')
    @reviewmgr.response(404, 'Review ID not Found')
    def delete(self, revid):
        """Remove a review."""
        uid = auth.authenticate(request)

        with db.get_dbconn() as conn:
            cur = conn.cursor()

            # check whether the review exist or not
            is_review_exist(cur, revid)

            dbutils.simple_select(
                cur, "reviews", ("uid", "status"),
                wcb=dbutils.WhereClauseBuilder("revid=%s", revid))
            owner, status = cur.fetchone()
            # check whether user is the owner
            if owner != uid:
                abort(401, "Not authorized to delete this review")

            # check if teh review already deleted
            if status == 'deleted':
                abort(404, "Review not found")

            # delete all comments from this review first
            # query = "DELETE FROM comments WHERE revid=%s"
            # cur.execute(query, (revid,))

            delete_review(cur, revid)

        return "Success", 204


@reviewmgr.route('/<revid>/status')
class ReviewStatus(Resource):
    """Handles the status of a review."""

    @reviewmgr.doc(description=(
        "Update the status of a review (accepted, rejected). "
        "On success, just return HTTP 204 with "" string 'success'"))
    @reviewmgr.response(204, 'Review succesfully updated')
    @reviewmgr.response(
        400, 'Wrong input format or input does not pass validation')
    @reviewmgr.response(401, 'Unauthorized acess')
    @reviewmgr.response(404, 'Review ID not Found')
    @reviewmgr.response(405, 'Not allowed to update the review')
    @reviewmgr.expect(models.review_status)
    def put(self, revid):
        """Approve or reject a review."""
        uid = auth.authenticate(request)
        body = get_request_body()

        with db.get_dbconn() as conn:
            cur = conn.cursor()

            # check whether the review exist or not
            is_review_exist(cur, revid)

            # check if teh review already deleted
            dbutils.simple_select(
                cur, "Reviews", ("rfrid", "status"),
                wcb=dbutils.WhereClauseBuilder("revid=%s", revid))
            rfrid, status = cur.fetchone()

            if status == 'deleted':
                abort(404, "Review not found")
            elif status == 'accepted' or status == 'rejected':
                abort(405, "Failed to change review status")

            # check whether user is the rfr owner
            dbutils.simple_select(
                cur, "rfrs", ("uid", ),
                wcb=dbutils.WhereClauseBuilder("rfrid=%s", rfrid))
            rfr_owner = single_unpack(cur.fetchone())

            if rfr_owner != uid:
                abort(401, "Not authorized to update this review")

            # validate status update
            veb = utils.ValidationErrorListBuilder()
            new_status = validators.validate_string(
                "status", body["status"], minlen=8, maxlen=8, veb=veb)
            if len(veb):
                abort(400, veb_reformat(veb))

            dbutils.simple_update(
                cur, "reviews", [("status", new_status)],
                wcb=dbutils.WhereClauseBuilder("revid=%s", revid))

        return "Success", 204


@reviewmgr.route('/<revid>/vote')
class ReviewVote(Resource):
    """Handles review votes."""

    @reviewmgr.doc(
        description="""Upvote or downvote a review.
        On success return the new vote position""")
    @reviewmgr.response(200, 'vote succesfully updated', models.review_vote)
    @reviewmgr.response(
        400, 'Wrong input format or input does not pass validation')
    @reviewmgr.response(404, 'Review ID not Found')
    @reviewmgr.expect(models.review_vote)
    def put(self, revid):
        """Upvote or downvote a review."""
        uid = auth.authenticate(request)
        body = get_request_body()

        veb = utils.ValidationErrorListBuilder()
        new_vote = validators.validate_string(
            "vote", body["vote"], minlen=6, maxlen=8, veb=veb)
        if len(veb):
            abort(400, veb_reformat(veb))

        with db.get_dbconn() as conn:
            cur = conn.cursor()

            # check whether the review exist or not
            is_review_exist(cur, revid)

            dbutils.simple_select(
                cur, "reviews", ("uid",),
                wcb=dbutils.WhereClauseBuilder("revid=%s", revid))
            owner = single_unpack(cur.fetchone())

            # get repoid to get repo tags
            query = f"""SELECT repoid
            FROM rfrs
            INNER JOIN reviews
            ON rfrs.rfrid = reviews.rfrid
            WHERE revid = {revid}"""

            cur.execute(query)
            repoid = single_unpack(cur.fetchone())

            # get tags of a repo
            repo_tids = get_repo_tags(cur, repoid)
            tids = ','.join(tid for tid in repo_tids)

            wcb = dbutils.WhereClauseBuilder("uid=%s", uid)
            wcb.and_("revid=%s", revid)
            dbutils.simple_select(cur, "votes", ("vote",), wcb=wcb)
            old_vote = single_unpack(cur.fetchone())
            if old_vote is None:
                dbutils.simple_insert(
                    cur, "votes", ("uid", "revid", "vote"),
                    values=(uid, revid, new_vote))
                if new_vote == "upvote":
                    query = f"""UPDATE user_tags
                    SET karma = karma + {config.UPVOTE}
                    WHERE uid = {owner}
                    AND tid in ({tids})"""

                    cur.execute(query)
                elif new_vote == "downvote":
                    query = f"""UPDATE user_tags
                    SET karma = karma - {config.DOWNVOTE}
                    WHERE uid = {owner}
                    AND tid in ({tids})"""

                    cur.execute(query)
                elif new_vote == "no vote":
                    pass
                else:
                    abort(400, "Wrong vote request")
            else:
                if old_vote == 'upvote' and new_vote == 'no vote':
                    query = f"""UPDATE user_tags
                    SET karma = karma - {config.UPVOTE}
                    WHERE uid = {owner}
                    AND tid in ({tids})"""
                    cur.execute(query)
                elif old_vote == 'upvote' and new_vote == 'downvote':
                    query = f"""UPDATE user_tags
                    SET karma = karma - {config.UPVOTE + config.DOWNVOTE}
                    WHERE uid = {owner}
                    AND tid in ({tids})"""
                    cur.execute(query)
                elif old_vote == 'no vote' and new_vote == 'upvote':
                    query = f"""UPDATE user_tags
                    SET karma = karma + {config.UPVOTE}
                    WHERE uid = {owner}
                    AND tid in ({tids})"""
                    cur.execute(query)
                elif old_vote == 'no vote' and new_vote == 'downvote':
                    query = f"""UPDATE user_tags
                    SET karma = karma - {config.DOWNVOTE}
                    WHERE uid = {owner}
                    AND tid in ({tids})"""
                    cur.execute(query)
                elif old_vote == 'downvote' and new_vote == 'upvote':
                    query = f"""UPDATE user_tags
                    SET karma = karma + {config.UPVOTE + config.DOWNVOTE}
                    WHERE uid = {owner}
                    AND tid in ({tids})"""
                    cur.execute(query)
                elif old_vote == 'downvote' and new_vote == 'no vote':
                    query = f"""UPDATE user_tags
                    SET karma = karma + {config.DOWNVOTE}
                    WHERE uid = {owner}
                    AND tid in ({tids})"""
                    cur.execute(query)

        return {"vote": new_vote}


@reviewmgr.route('/<username>')
class listUserReviews(Resource):
    """Handles a user's reviews."""

    @reviewmgr.doc(
        description="Endpoint to list all user's reviews based on username.")
    @reviewmgr.response(
        200,
        """Returns all the reviews in a list of dicts.
        Values: revid, review, create_date, status and published.""")
    @reviewmgr.response(204, "No reviews to return")
    @reviewmgr.response(
        400, 'Wrong input format or input does not pass validation',
        models.validation_error_list)
    def get(self, username):
        """Get a user's reviews."""
        username = validate_username(username)

        with db.get_dbconn() as conn:
            cur = conn.cursor()

            uid = get_userid(cur, username)

            reviews = []

            # collect all rfrs data
            dbutils.simple_select(
                cur, "reviews",
                ("revid", "review", "status", "is_published", "createdate"),
                wcb=dbutils.WhereClauseBuilder("uid=%s", uid),
                orderby=("createdate DESC",))

            # Map rfrs data to be send to frontend
            for (revid, review, status, published, create_date) in cur:
                if published:
                    review = {
                        "revid": revid,
                        "review": review,
                        "status": status,
                        "create_date": create_date.strftime("%Y-%m-%d"),
                        "status": status
                    }

                    reviews.append(review)

            if len(reviews) == 0:
                return '', 204
            return reviews, 200


@reviewmgr.route('/my')
class currentUserRFRReviews(Resource):
    """Handles reviews of the logged in user's RFRs."""

    @reviewmgr.doc(
        description="Endpoint to list all reviews of the user's RFRs.")
    @reviewmgr.response(
        200,
        """Returns all the reviews in a list of dicts.
        Values: revid, review, create_date, status and published.""")
    @reviewmgr.response(204, "No reviews to return")
    @reviewmgr.response(
        400, 'Wrong input format or input does not pass validation',
        models.validation_error_list)
    def get(self, username):
        """Get the reviews of the logged in user's RFRs."""
        uid = auth.authenticate(request)

        with db.get_dbconn() as conn:
            cur = conn.cursor()

            reviews = []

            # collect all rfrs data
            dbutils.simple_select(
                cur, "reviews",
                ("revid", "review", "status", "is_published", "createdate"),
                wcb=dbutils.WhereClauseBuilder("uid=%s", uid),
                orderby=("createdate DESC",))

            # Map rfrs data to be send to frontend
            for (revid, review, status, published, create_date) in cur:
                review = {
                    "revid": revid,
                    "is_published": published,
                    "review": review,
                    "status": status,
                    "create_date": create_date.strftime("%Y-%m-%d"),
                    "status": status
                }

                reviews.append(review)

            if len(reviews) == 0:
                return '', 204
            return reviews


@reviewmgr.route('/<revid>/comment')
class ReviewComment(Resource):
    """Handles comments for a given review."""

    @reviewmgr.doc(description="Comment on a review")
    @reviewmgr.response(
        201, 'Comment successfully posted',
        models.post_review_comment_response)
    @reviewmgr.response(
        400, 'Wrong input format or input does not pass validation')
    @reviewmgr.response(401, 'Unauthorized acess')
    @reviewmgr.response(404, 'Review ID not found')
    # @reviewmgr.expect(models.post_comment)
    def post(self, revid):
        """Post review comment."""
        uid = auth.authenticate(request)
        body = get_request_body()

        with db.get_dbconn() as conn:
            cur = conn.cursor()
            helper.add_cur(cur)

            # Check if the review id exists
            helper.check_exists('revid', 'reviews')

            # Generate commend id
            commentid = helper.make_id('cid', 'comments')

            dt = str(datetime.now())
            # Add the comment to the database
            fields = {
                'cid': commentid,
                'comment': body['comment'],
                'datetime': dt,
                'uid': uid
            }
            helper.db_input('comments', fields)

            # Associate the comment with the review
            fields = {
                'cid': commentid,
                'revid': revid
            }
            helper.db_input('review_comments', fields)

        return {
            'commentid': commentid,
            'comment': body['comment'],
            'revid': revid,
            'datetime': dt
        }, 201


@reviewmgr.doc(description="Get all comments for a review")
@reviewmgr.response(
    200, "All review comments returned", models.get_review_comment_list)
@reviewmgr.response(204, "No comments to return")
@reviewmgr.response(
    400, 'Wrong input format or input does not pass validation')
@reviewmgr.response(401, 'Unauthorized acess')
@reviewmgr.response(404, 'Review ID not found')
def get(self, revid):
    """Get all comments for a review."""
    with db.get_dbconn() as conn:
        cur = conn.cursor()
        helper.add_cur(cur)

        # Check if the review id exists
        helper.check_exists('revid', 'reviews')

        # Get all comments for that review
        cur.execute(
            f"""SELECT comments.cid, comments.comment, comments.datetime,
            comments.uid, comments.edited, comments.removed
            FROM comments
            INNER JOIN review_comments
            ON comments.cid = review_comments.cid
            WHERE review_comments.revid={revid}""")

        fields = [
            'commentid',
            'comment',
            'datetime',
            'userid',
            'edited',
            'removed'
        ]

        body = [dict(zip(fields, item)) for item in cur]
        if len(body) == 0:
            return '', 204

    return {'comments': body}, 200


@reviewmgr.route('/<revid>/comment/<commentid>')
class ReviewCommentUpdate(Resource):
    """Handles updating and deleting comments."""

    @reviewmgr.doc(description="Edit a review comment")
    @reviewmgr.response(
        200, 'Review comment edited.', models.put_review_comment_response)
    @reviewmgr.response(
        400, 'Wrong input format or input does not pass validation')
    @reviewmgr.response(401, 'Unauthorized acess')
    @reviewmgr.response(404, 'Review ID or comment ID not found')
    @reviewmgr.response(409, 'Comment has been removed')
    @reviewmgr.expect(models.post_comment)
    def put(self, revid, commentid):
        """Edit a review comment."""
        auth.authenticate(request)
        body = get_request_body()

        with db.get_dbconn() as conn:
            cur = conn.cursor()
            helper.add_cur(cur)

            # Check if the review id exists
            helper.check_exists('revid', 'reviews')

            # Check it the comment exists
            helper.check_exists('cid', 'comments'),

            # Check comment hasn't been removed
            helper.check_exists(
                'cid', 'comments',
                wcb=dbutils.WhereClauseBuilder("removed=%s", 'false'),
                abort_text="%s removed", abort_response=409)

            dt = str(datetime.now())

            # Update the comment
            cur.execute(f"""
                UPDATE comments
                SET comment='{body['comment']}', datetime='{dt}', edited=TRUE
                WHERE cid={commentid}
            """)

        return {
            'commentid': commentid,
            'comment': body['comment'],
            'datetime': dt,
            'edited': True
        }, 200


@reviewmgr.doc(description="Remove a review comment")
@reviewmgr.response(
    200, 'Review comment removed', models.delete_review_comment_response)
@reviewmgr.response(
    400, 'Wrong input format or input does not pass validation')
@reviewmgr.response(401, 'Unauthorized acess')
@reviewmgr.response(404, 'Review ID or comment ID not found')
@reviewmgr.response(409, 'Comment has already been removed')
def delete(self, revid, commentid):
    """Remove a review comment."""
    auth.authenticate(request)

    with db.get_dbconn() as conn:
        cur = conn.cursor()
        helper.add_cur(cur)

        # Check if the review id exists
        helper.check_exists('revid', 'reviews')

        # Check it the comment exists
        helper.check_exists('cid', 'comments'),

        # Check comment hasn't already been removed
        helper.check_exists(
            'cid', 'comments',
            wcb=dbutils.WhereClauseBuilder("removed=%s", 'false'),
            abort_text="%s removed", abort_response=409)

        dt = str(datetime.now())

        # Remove the comment
        cur.execute(f"""
            UPDATE comments
            SET datetime='{dt}', removed=TRUE
            WHERE cid={commentid}
        """)

    return {
        'commentid': commentid,
        'datetime': dt,
        'removed': True
    }, 200
