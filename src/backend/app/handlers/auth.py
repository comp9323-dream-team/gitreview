"""Handles authentication."""

import helpers.auth as auth
from mainapp import api
from flask_restx import Resource, abort
from flask import request

import models
from helpers.request_handlers import dict_unpack, single_unpack, row_extract
from helpers.request_handlers import get_request_body
import helpers.dbman as db

import config
import os.path as path

authapi = api.namespace('auth', description='Authentication module')


@authapi.route('/login')
class Login(Resource):
    """Handles login requests."""

    @authapi.response(200, 'Login success', models.auth_token)
    @authapi.response(400, 'Missing email/password', models.validation_error)
    @authapi.response(403, 'Invalid email/password', models.validation_error)
    @authapi.response(
        404, 'Password authentication data not found',
        models.validation_error)
    @authapi.expect(models.login_details)
    @authapi.doc(
        description="""
        Use this endpoint to authenticate email/password pair.
        Returns a token which should be put in the Authorisation header.
        """)
    def post(self):
        """Log in."""
        (un, pw) = dict_unpack(
            get_request_body(),
            "email", "password",
            required=True, strlenmax=255)

        # ensure that transactions are closed by scoping the *connection*
        # (not cursor) in "with"
        with db.get_dbconn() as conn:
            cur = conn.cursor()
            # Identify which user the supplied email belongs to
            # (right now we only do email)
            cur.execute(
                "select uid from userident where field=%s and data=%s",
                ('email', un))
            uid = single_unpack(cur.fetchone())
            if uid is None:
                abort(403, "email not found.")

            # get the hashed password data
            cur.execute(("select authtype,data from authdata where uid=%s and"
                         " authtype in %s"), (uid, ("pw", "pwsalt")))
            (pwhash, pwsalt) = row_extract(cur, 1, "pw", "pwsalt")
            if None in (pwhash, pwsalt):
                abort(404, "Password authentication data not found.")
            # convert salt, which should be in hex string, to byte array
            try:
                pwsalt = bytearray.fromhex(pwsalt)
            except (TypeError, ValueError):
                abort(404, "Password salt error.")

            # check the supplied password hashes to the same hash stored in DB
            if pwhash != auth.get_pass_hash(pw, pwsalt):
                abort(403, "Invalid password.")

            # OK! let's generate token!
            new_token = auth.store_token_data(auth.TOKEN_TYPE_SESSION, uid)

            return {
                "token": new_token
            }


@authapi.route('/logout')
class Logout(Resource):
    """Handles logout requests."""

    @authapi.response(200, 'Logout success')
    def get(self):
        """Log out."""
        token = auth.get_authorisation_token(request)
        auth.authenticate(request, token)
        auth.invalidate_token(token)
        return "success"


@authapi.route('/whoami')
class WhoAmI(Resource):
    """Handles request for user id."""

    @authapi.response(200, 'Your user ID')
    def get(self):
        """Get user id."""
        return auth.authenticate(request)


def initialise():
    """Initialise authentication module."""
    if not path.exists(config.SESSION_DIR):
        raise RuntimeError(
            f"Session directory '{config.SESSION_DIR}' does not exist.")


initialise()
