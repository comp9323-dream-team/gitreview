"""
Gitreview's backend repo controller.

Communicates with the GitSVC service.
Credit: Jake Antmann, with template from Boni Krismahardhika
"""

from helpers.account_helper import get_userid
import config

from datetime import datetime
from flask import request, make_response
from flask_restx import Resource, abort

from mainapp import api
import models

from helpers.request_handlers import get_request_body
import helpers.dbman as db
from helpers.dbutils import simple_select, WhereClauseBuilder, single_unpack
import helpers.auth as auth
from helpers.repo_helpers import dictify, forward, owns_repo
from helpers.repo_helpers import API_helper, is_repo_exist
import helpers.utils as utils


git_url = config.SVCURL
helper = API_helper(git_url)

repomgr = api.namespace('repo', description='Repo management')


@repomgr.route('')
class Repo(Resource):
    """Handles repos."""

    @repomgr.doc(
        description="On success, just return the repoid of the repo"
    )
    @repomgr.expect(models.post_repo)
    @repomgr.response(201, 'Success', models.generic_success_id)
    @repomgr.response(
        400,
        'Wrong input format or input does not pass validation',
        models.validation_error)
    @repomgr.response(
        409,
        'Not allowed',
        models.validation_error)
    def post(self):
        """
        Post a repo.

        Create a new repo.
        Add the repo's tags to the tag table.
        Add the repo's tags to the repo_tags table
        Upload the user's SSH key and corresponding details to the database.
        Pair the SSH key with the repo.

        Upload the repo to gitsvc.
        Upload the repo details and SSH key to gitsvc.
        """
        resp = helper.get("/repo/2/heads")

        uid = auth.authenticate(request)
        body = get_request_body()

        # Get repo url
        try:
            url = body['url']
            username, _, _ = utils.parse_git_ssh_url(url)
            if username is None:
                raise ValueError("Invalid format.")
        except:
            abort(400, "Invalid Git SSH URL format.")

        response = None
        try:
            with db.get_dbconn() as conn:
                cur = conn.cursor()
                helper.add_cur(cur)

                # Make sure the url is not already uploaded
                cur = conn.cursor()
                simple_select(cur, "repos", ("url",),
                    WhereClauseBuilder("url=%s", url))
                repourl = single_unpack(cur.fetchone())
                if repourl is not None:
                    abort(409, ("This URL has already been uploaded"))

                # Make repo id
                repoid = helper.make_id('repoid', 'repos')

                # Generate time or check time of request
                createdate = datetime.now()

                # Generate status
                status = 1

                # Create row
                fields = {
                    'repoid': repoid,
                    'status': status,
                    'createdate': createdate,
                    'url': url,
                    'name': body.get('name', url),
                    'description': body.get('description', ''),
                    'removed': False
                }

                # Enter into db
                helper.db_input('repos', fields)

                # Add each tag
                for tag in body['tags']:
                    tid = helper.make_id('tid', 'tags')

                    # Post to tags
                    fields = {
                        'tag': tag,
                        'tid': tid
                    }
                    helper.db_input('tags', fields)

                    # Post to repo_tags
                    fields = {
                        'repoid': repoid,
                        'tid': tid
                    }
                    helper.db_input('repo_tags', fields)

                # Set repo owner
                fields = {
                    'repoid': repoid,
                    'uid': uid
                }

                helper.db_input('repo_owners', fields)

                # Make empty gitsvc repo
                endpoint = "/repo"
                data = {'id': repoid}
                response = helper.post(endpoint, data)
                if response.ok:
                    # return ID of the new repo
                    return data

                # to rollback the transaction
                raise RuntimeError("GitSvc returned error.")
        except Exception as ex:
            if response is None:
                raise ex
            return forward(response)

    @repomgr.doc(description="Get basic information for all repos")
    @repomgr.response(200, 'List of repos', models.repo_output_list)
    @repomgr.response(204, 'No repos to return')
    @repomgr.response(
        400,
        'Wrong input format or input does not pass validation',
        models.validation_error)
    def get(self):
        """Get basic information for all repos."""

        # Get repo ids
        with db.get_dbconn() as conn:
            cur = conn.cursor()
            helper.add_cur(cur)

            simple_select(
                cur, "repos",
                ("repoid", "name", "description", "url"),
                wcb=WhereClauseBuilder("removed=%s", "False"))

            body = [{
                'repoid': item[0],
                'name': item[1],
                'description': item[2],
                'url': item[3]
            } for item in cur]

            if len(body) == 0:
                return '', 204

            # Find the tags
            for i, obj in enumerate(body):
                cur.execute(
                    f"""SELECT tags.tag
                    FROM tags
                    INNER JOIN repo_tags
                    ON tags.tid = repo_tags.tid
                    WHERE repo_tags.repoid={obj['repoid']}""")
                body[i]['tags'] = [item[0] for item in cur]

        return {'repo_list': body}, 200


@repomgr.route('/<repoid>')
class Repo(Resource):
    """Handles access to specific repos."""

    @repomgr.doc(
        description="Get the details of a repo."
    )
    @repomgr.response(200, 'Successfully removed.', models.repo_output)
    @repomgr.response(
        400,
        'Wrong input format or input does not pass validation.',
        models.validation_error)
    @repomgr.response(404, "Not found.")
    @repomgr.response(
        409,
        'Not allowed.',
        models.validation_error)
    def get(self, repoid):
        """Get a single repo."""
        fields = ("repoid","name","description","url")
        with db.get_dbconn() as conn:
            cur = conn.cursor()

            # Check the repo exists
            is_repo_exist(cur, repoid)

            # Get relevant repo details
            fields = ("repoid", "name", "description", "url")
            wcb = WhereClauseBuilder("repoid=%s", repoid)
            wcb.and_("removed=%s", 'false')

            simple_select(cur, "repos", fields, wcb=wcb)
            repo_details = [x for x in cur]
            if len(repo_details) == 0:
                abort(404, "The repo does not exist")
            repo_details = repo_details[0]

            # Return repo details
            body=dict(zip(fields, repo_details))

            # Find the tags
            cur.execute(
                f"""SELECT tags.tid, tags.tag
                FROM tags
                INNER JOIN repo_tags
                ON tags.tid = repo_tags.tid
                WHERE repo_tags.repoid={repoid}""")
            fields = ["tagid", "tag"]
            body['tags'] = [dict(zip(fields, item)) for item in cur]

        return body, 200

    def delete(self, repoid):
        """Remove a repo."""
        uid = auth.authenticate(request)
        with db.get_dbconn() as conn:
            cur = conn.cursor()
            helper.add_cur(cur)

            # Check the repo exists
            is_repo_exist(cur, repoid)

            # Check the user owns the repo
            helper.owns_repo(uid, repoid)

            # Remove repo
            cur.execute(f"""
                UPDATE repos
                SET removed=TRUE
                WHERE repoid={repoid}
            """)
        return 'Successfully removed', 200


@repomgr.route('/<repoid>/owners')
class RepoOwners(Resource):
    """Handles repo owners."""

    @repomgr.doc(
        description="Get all of a repo's owners."
    )
    @repomgr.response(200, 'All repo owners returned.', models.repo_owners)
    @repomgr.response(
        400,
        'Wrong input format or input does not pass validation.',
        models.validation_error)
    @repomgr.response(404, "Not found.")
    @repomgr.response(
        409,
        'Not allowed.',
        models.validation_error)
    def get(self, repoid):
        """Get all of a repo's owners."""        
        with db.get_dbconn() as conn:
            cur = conn.cursor()
            helper.add_cur(cur)

            # Check the repo exists
            is_repo_exist(cur, repoid)

            # Get relevant repo owners
            cur.execute(f"""SELECT repo_owners.repoid, userprofiles.data
                FROM repo_owners
                INNER JOIN userprofiles
                ON repo_owners.uid = userprofiles.uid
                WHERE userprofiles.field = 'username'
                AND repo_owners.repoid = {repoid}""")

            fields = ['repoid', 'username']
            body=[dict(zip(fields, item)) for item in cur]
            if len(body) == 0:
                return '', 204

        return body, 200

@repomgr.route('/byuser/<username>')
class ReposOwned(Resource):
    @repomgr.doc(description="Get basic information for all a user's repos")
    @repomgr.response(200, 'List of repos', models.repo_output_list)
    @repomgr.response(204, 'No repos to return')
    @repomgr.response(
        400,
        'Wrong input format or input does not pass validation',
        models.validation_error)
    def get(self, username):
        # Get repo ids
        if username == "my":
            userid = auth.authenticate(request)
        with db.get_dbconn() as conn:
            cur = conn.cursor()
            helper.add_cur(cur)
            
            """Get basic information for all a user's repos."""
            if username != "my":
                # if it is already "my", then we already got it from auth
                userid = get_userid(cur, username)
            
            # Get repo ids  
            simple_select(
                cur, "repo_owners", ("repoid",),
                WhereClauseBuilder("uid=%s", userid))

            repoid_list = [item for item in cur]
            if len(repoid_list) == 0:
                return '', 204

            cur.execute(
                f"""SELECT repos.repoid, repos.name,
                repos.description, repos.url
                FROM repo_owners
                INNER JOIN repos
                ON repo_owners.repoid = repos.repoid
                WHERE repo_owners.uid={userid}
                AND repos.removed=False""")

            body = [{
                'repoid': item[0],
                'name': item[1],
                'description': item[2],
                'url': item[3]
            } for item in cur]
            if len(body) == 0:
                return '', 204

            # Find the tags
            for i, obj in enumerate(body):
                cur.execute(
                    f"""SELECT tags.tag
                    FROM tags
                    INNER JOIN repo_tags
                    ON tags.tid = repo_tags.tid
                    WHERE repo_tags.repoid={obj['repoid']}""")
                body[i]['tags'] = [item[0] for item in cur]

        return body, 200

@repomgr.route('/<repoid>/remote')
class RepoRemote(Resource):
    @repomgr.doc(description="Set SSH key to the repo's remote settings.")
    @repomgr.expect(models.repo_key_settings)
    def post(self, repoid):
        body = get_request_body()

        uid = auth.authenticate(request)
        repoid = int(repoid)

        with db.get_dbconn() as conn:
            cur = conn.cursor()

            # Check the repo exists
            is_repo_exist(cur, repoid)

        # get repo url, so we can find hostname and path
        with db.get_dbconn() as conn:
            cur = conn.cursor()
            # also check for ownership
            owns_repo(cur, uid, repoid)
            simple_select(cur, "repos", ("url",),
                WhereClauseBuilder("repoid=%s", repoid))
            repourl = single_unpack(cur.fetchone())

        # repourl assumed to be populated already
        username, hostname, path = utils.parse_git_ssh_url(repourl)
        sshhost = f"{username}@{hostname}"
        data = {
            "host": sshhost,
            "path": path,
            "key": body["key"],
            "key_type": body["key_type"]
        }

        endpoint = f"/repo/{repoid}/remote"
        return forward(helper.post(endpoint, data))

    @repomgr.doc(description="Get repo remote details from repo id")
    @repomgr.response(200, 'Remote settings')
    @repomgr.response(
        400,
        'Wrong input format or input does not pass validation',
        models.validation_error)
    @repomgr.response(
        403,
        'Unauthorized to get this remote',
        models.validation_error)
    def get(self, repoid):
        """Get repo details from gitsvc."""
        uid = auth.authenticate(request)

        with db.get_dbconn() as conn:
            cur = conn.cursor()

            # Check the repo exists
            is_repo_exist(cur, repoid)

            # Check the user is a repo owner
            owns_repo(cur, uid, repoid)

        endpoint = f"/repo/{repoid}/remote"
        return forward(helper.get(endpoint))


@repomgr.route('/<repoid>/fetch')
class RepoIdFetch(Resource):
    """Handles fetch jobs."""

    @repomgr.doc(description="Submit a fetch job")
    @repomgr.response(200, 'Fetch task successfully submitted.')
    @repomgr.response(
        400,
        'Wrong input format or input does not pass validation',
        models.validation_error)
    @repomgr.response(404, 'Repository or remote settings not found.')
    @repomgr.response(
        429, 'Fetch is busy or rate limit exceeded. Try again later.')
    def post(self, repoid):
        """Post a fetch job to gitsvc."""
        # No authentication needed.

        # Check the repo exists
        with db.get_dbconn() as conn:
            cur = conn.cursor()
            is_repo_exist(cur, repoid)

        return forward(helper.post(f"/repo/{repoid}/fetch"))

    @repomgr.doc(description="Get the remote repository fetching status.")
    @repomgr.response(200, 'Repository fetching status.')
    @repomgr.response(400, 'Invalid request')
    @repomgr.response(404, 'Repository not found')
    def get(self, repoid):
        """Get status of or results of fetch job."""
        # No authentication needed.

        # Check the repo exists
        with db.get_dbconn() as conn:
            cur = conn.cursor()
            is_repo_exist(cur, repoid)

        return forward(helper.get(f"/repo/{repoid}/fetch"))


@repomgr.route('/<repoid>/heads')
class RepoIdHeads(Resource):
    """Handles repo heads."""

    @repomgr.doc(description="Get head(s) of a repository")
    @repomgr.response(
        200, 'List of references or heads and their hash values'
    )
    @repomgr.response(400, 'Invalid request')
    @repomgr.response(404, 'Repository not found')
    @repomgr.response(428, 'Fetch the repository first!')
    def get(self, repoid):
        """Get the heads of a repo."""
        # Check the repo exists
        # Check the repo exists
        with db.get_dbconn() as conn:
            cur = conn.cursor()
            is_repo_exist(cur, repoid)

        return forward(helper.get(f"/repo/{repoid}/heads"))


@repomgr.route('/<repoid>/parents/<hash>')
class RepoIdParentsHash(Resource):
    """Handles parents."""

    @repomgr.doc(description="Get list of parents for the given commit hash.")
    @repomgr.response(200, 'List of parents, ordered by closest parent.')
    @repomgr.response(400, 'Invalid request')
    @repomgr.response(404, 'Repository or commit hash not found.')
    def get(self, repoid, hash):
        """Get a repo's parents."""
        # Check the repo exists
        with db.get_dbconn() as conn:
            cur = conn.cursor()
            is_repo_exist(cur, repoid)

        return forward(helper.get(f"/repo/{repoid}/parents/{hash}"))


@repomgr.route('/<repoid>/browse/<hash>')
class RepoIdBrowseHash(Resource):
    """Handles browsing a commit."""

    # @repomgr.doc(
    #     description=""
    # )
    @repomgr.response(200, 'Success')
    @repomgr.response(400, 'Invalid request')
    @repomgr.response(404, 'Repository, hash or path not found.')
    @repomgr.response(428, 'Fetch the repository first!')
    def get(self, repoid, hash):
        """Get tree (directory) structure of the given tree or commit hash."""
        # Check the repo exists
        with db.get_dbconn() as conn:
            cur = conn.cursor()
            is_repo_exist(cur, repoid)

        return forward(helper.get(f"/repo/{repoid}/browse/{hash}"))


@repomgr.route('/<repoid>/download/<hash>/<path:path>')
class RepoIdDownloadHashPath(Resource):
    """Handles raw files from a given commit or tree hash and path."""

    # @repomgr.doc(
    #     description=""
    # )
    @repomgr.response(200, 'Success')
    @repomgr.response(400, 'Invalid request')
    @repomgr.response(404, 'Repository, hash or path not found.')
    @repomgr.response(428, 'Fetch the repository first!')
    def get(self, repoid, hash, path):
        """Download raw file from the given commit or tree hash and path."""
        # Check the repo exists
        with db.get_dbconn() as conn:
            cur = conn.cursor()
            is_repo_exist(cur, repoid)

        return forward(helper.get(f"/repo/{repoid}/download/{hash}/{path}"))

@repomgr.route('/<repoid>/lines/<hash>/<path:path>')
class RepoIdLinesHashPath(Resource):
    """Handles raw files from a given commit or tree hash and path in line format."""

    # @repomgr.doc(
    #     description=""
    # )
    @repomgr.response(200, 'Success')
    @repomgr.response(400, 'Invalid request')
    @repomgr.response(404, 'Repository, hash or path not found.')
    @repomgr.response(428, 'Fetch the repository first!')
    def get(self, repoid, hash, path):
        """Get lines of the file from the given commit or tree hash and path."""
        # Check the repo exists
        with db.get_dbconn() as conn:
            cur = conn.cursor()
            is_repo_exist(cur, repoid)

        return forward(helper.get(f"/repo/{repoid}/lines/{hash}/{path}"))

@repomgr.route('/<repoid>/pushstatus')
class RepoIdPushStatus(Resource):
    """Handles Push Status checking of a Repo."""

    # @repomgr.doc(
    #     description=""
    # )
    @repomgr.response(200, 'Success')
    @repomgr.response(400, 'Invalid request')
    @repomgr.response(404, 'Repository not found.')
    def get(self, repoid):
        """Get push ststus of a Repo."""
        # Check the repo exists
        with db.get_dbconn() as conn:
            cur = conn.cursor()
            is_repo_exist(cur, repoid)

        return forward(helper.get(f"/repo/{repoid}/pushstatus"))