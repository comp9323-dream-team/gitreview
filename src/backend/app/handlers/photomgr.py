from mainapp import api
from flask_restx import Resource, abort
from flask import request, send_from_directory, safe_join
import os
import sys
import secrets
from PIL import Image, UnidentifiedImageError
import io

import config
import models
import helpers.dbman as db
from helpers.dbutils import *
from helpers.cm import get_reqctx
import helpers.auth as auth
from helpers.account_helper import get_userid, validate_username

# all images will be JPEG formatted per our policy. So we can safely pass that JPEG's mime.
IMG_MIME="image/jpeg"
DEFAULT_JPEG_QUALITY=85

photomgr = api.namespace('photos', description='Photos management and retreival.')

@photomgr.route('/profpic')
@photomgr.route('/profpic/<username>')
class ProfilePicture(Resource):
  @photomgr.response(200, "Image data.")
  @photomgr.response(404, "Image data not found.")
  @photomgr.produces([IMG_MIME])
  def get(self, username):
    if username == "my":
      uid = auth.authenticate(request)
    else:
      username = validate_username(username)
      uid = get_userid(username)
      
    with db.get_dbconn() as conn:
      cur = conn.cursor()
      imgid = self._get_photoid(cur, uid)
      if imgid is None:
        abort(404, "User profile picture data not found.")

    try:
      return send_from_directory(config.RESOURCES_DIR, filename=imgid, as_attachment=False, mimetype=IMG_MIME, cache_timeout=-1)
    except FileNotFoundError:
      abort(404, "User profile picture not found.")

  @photomgr.response(200, "Profile picture successfully set.")
  @photomgr.doc(description="Accepts common image format.")
  def post(self, username=None): 
    if username is not None:
      abort(403, "You can only change your own profile picture.")
    uid = auth.authenticate(request)
    
    im = _process_photo(request.data)

    # at this point, photo is loaded (and resized) OK.
    # check existing photo and delete it if exists
    with db.get_dbconn() as conn:
      cur = conn.cursor()
      self._delete_photo(cur, uid)
      newid = _save_photo(im)
      simple_insert(cur, "userprofiles", ("uid", "field", "data"),
        value=(uid, "photo", newid))
    
    return "success"

  @photomgr.response(200, "Profile picture successfully deleted.")
  def delete(self, username=None):
    if username is not None:
      abort(403, "You can only delete your own profile picture.")
    uid = auth.authenticate(request)
    with db.get_dbconn() as conn:
      cur = conn.cursor()
      self._delete_photo(cur, uid)
    return "success"

  def _delete_photo(self, cur, uid):
    oldid = self._get_photoid(cur, uid)
    if oldid is not None:
      cur.execute("delete from userprofiles where (uid=%s) and (field=%s)", (uid, "photo"))
      _delete_photo(oldid)

  def _get_photoid(self, cur, uid):
    simple_select(cur, "userprofiles", ("data",), 
      WhereClauseBuilder("uid=%s", uid).and_("field=%s", "photo"))
    imgid = single_unpack(cur.fetchone())
    return imgid

def _photo_filename(imgid):
  return safe_join(config.RESOURCES_DIR, imgid)

def _delete_photo(imgid):
  try:
    os.unlink(_photo_filename(imgid))
  except FileNotFoundError:
    # other errors should be passed to caller
    pass

def _gen_resource_id():
  """Returns a hex string."""
  LEN_BYTES=8
  while True:
    ret = secrets.randbits(LEN_BYTES * 8)
    if ret < 1024:
      continue
    ret = ret.to_bytes(LEN_BYTES, sys.byteorder, signed=False).hex()
    if not os.path.exists(safe_join(config.RESOURCES_DIR, ret)):
      return ret

def _determine_photo_size(w, h):
  """return tuple (resize, new_w, new_h)"""
  resize = False
  if w > config.IMAGE_MAX_WIDTH:
    resize = True
    h = h * config.IMAGE_MAX_WIDTH // w
    w = config.IMAGE_MAX_WIDTH
  if h > config.IMAGE_MAX_HEIGHT:
    resize = True
    w = w * config.IMAGE_MAX_HEIGHT // h
    h = config.IMAGE_MAX_HEIGHT

  return resize, w, h

def _process_photo(data):
  bo = io.BytesIO(data)
  try:
    im = Image.open(bo)
  except UnidentifiedImageError:
    abort(400, "Invalid or unrecognised image format.")
  # resize if needed
  resize, new_w, new_h = _determine_photo_size(im.size[0], im.size[1])
  if resize:
    im.resize((new_w, new_h), reducing_gap=2)
  # JPEG doesn't support transparency
  im = im.convert("RGB")
  return im

def _save_photo(im):
  newid = _gen_resource_id()
  with open(_photo_filename(newid), "wb") as f:
    im.save(f, format="JPEG", quality=DEFAULT_JPEG_QUALITY)
  return newid

def initialise():
  """Photo/resource manager initialisation"""
  if not os.path.exists(config.RESOURCES_DIR):
    raise RuntimeError(f"Resource directory '{config.RESOURCES_DIR}' does not exist.")

initialise()
