from re import sub
from mainapp import api
import models
from flask import request
from flask_restx import Resource, abort
from datetime import datetime

import helpers.validators as validators
import helpers.utils as utils
import helpers.dbman as db
import helpers.dbutils as dbutils
from helpers.request_handlers import veb_reformat

tagmgr = api.namespace('tag', description="tag management")

@tagmgr.route('/autocomplete/<substr>')
class AutocompleTag(Resource):
    @tagmgr.doc(description="Get tags for autocomplete, On success returns HTTP 200 with string 'success'")
    @tagmgr.response(200, 'Verify forget password success', models.auth_token)
    @tagmgr.response(404, 'Token not found, expired, or invalid')
    def get(self, substr):
      # validate string for query 
      veb = utils.ValidationErrorListBuilder()
      substr = validators.validate_string("substr", substr, veb=veb)
      if len(veb):
        abort(400, veb_reformat(veb))
    
      with db.get_dbconn() as conn:
        cur = conn.cursor()
        
        substr = substr+'%'
        # retrieve tags data from tag table
        dbutils.simple_select(cur, "tags", ("tag",), wcb=dbutils.WhereClauseBuilder("tag ILIKE %s", substr))
        tags = []  
        # convert tasg data to list
        for (tag,) in cur:
          tags.append(tag)
      
      return tags