import re
from mainapp import api
from flask import request, send_file
from flask_restx import Resource, abort, reqparse, fields
from datetime import datetime
import requests

import models
import helpers.validators as validators
import helpers.utils as utils
import helpers.dbman as db
import helpers.dbutils as dbutils
import helpers.auth as auth
import config
from helpers.repo_helpers import API_helper, forward
from helpers.request_handlers import *
from helpers.rfr_helpers import is_rfr_exist, is_rfr_owner, get_rfr_repo
from helpers.review_helpers import *
from helpers.tags_helper import get_repo_tags
from helpers.account_helper import get_userid, get_username, validate_username
from helpers.queue import dump_queue

reviewermgr = api.namespace('reviewer', description="Reviewer management")

mailer_url = config.MAILERURL
helper = API_helper(mailer_url)

@reviewermgr.route('/<rfrid>')
class ReviewerRFR(Resource):
  @reviewermgr.doc(description="list all possible reviwer of an RFR.")
  @reviewermgr.response(200, "List of reviews")
  @reviewermgr.response(404, "Not found.")
  def get(self, rfrid):
    uid = auth.authenticate(request)
    
    with db.get_dbconn() as conn:
      cur = conn.cursor()
      
      # check if the rfr is exist
      is_rfr_exist(cur, rfrid)
      
      # check if user is the rfr owner
      is_rfr_owner(cur, uid, rfrid)
      
      # get the repo of the RFR
      repoid = get_rfr_repo(cur, rfrid)
      
      # get relevant user to be reviewer
      repotags = get_repo_tags(cur, repoid)
      repotags =  tuple(repotags)
      dbutils.simple_select(cur, "reviewer_tags_v1", ("username", "tag", "karma"), wcb=dbutils.WhereClauseBuilder("tid IN %s", repotags), orderby=("karma DESC",))
      
      reviewers = {}
      for user, tag, karma in cur:
        if user not in reviewers:
          reviewers[user] = { "karma" : 0,
                             "tags" : []
                           }
        reviewers[user]["karma"] += karma
        reviewers[user]["tags"].append({ tag : karma})
        
    return reviewers
    
  @reviewermgr.doc(description="Send request to reviewer. On success returns HTTP 200 with string 'success'")
  @reviewermgr.expect(models.reviewer_request_list)
  @reviewermgr.response(200, 'Reviewer request sent')
  @reviewermgr.response(400, 'Wrong input format or input does not pass validation', models.validation_error_list)
  @reviewermgr.response(404, 'Not found')
  def post(self, rfrid):
    uid = auth.authenticate(request)
    body = get_request_body()
    
    # cut reviewers number if exceeded the length max number reviewers request
    reviewers = list(body["reviewers"])
    if len(reviewers) > config.MAX_REVIEWER:
      reviewers = reviewers[:config.MAX_REVIEWER]
    
    # validate the anme of each reviewer
    for i in range(len(reviewers)):
      reviewers[i] = validate_username(reviewers[i])
    
    mail_address = []
    with db.get_dbconn() as conn:
      cur = conn.cursor()
      
      # get sender username
      sender = get_username(cur, uid)
      
      # construct rfr link  --> not final change later
      rfr_link = f"{config.WEBURL}rfr/{rfrid}/review/"   
      
      for reviewer in reviewers:
        #get reviewer user id
        uid = get_userid(cur, reviewer)
        
        # extract reviewer email from DB
        wcb = dbutils.WhereClauseBuilder("field=%s", "email")
        wcb.and_("uid=%s", uid)
        dbutils.simple_select(cur, "userprofiles", ("data",), wcb=wcb)
        email = single_unpack(cur.fetchone())
        if email is None:
          abort(404, "Reviewer not found")
        mail_address.append({"receiver" : reviewer, "address" : email})
        
        queue_data = (reviewer, email, sender, rfr_link)
        
        success = dump_queue(queue_data)    
        if not success:
          abort(404, "Send email failed.")
    
    return "Success"
    
        
        
        
    
   
      
