"""Handles account management."""

from mainapp import api
import models
from flask import request
from flask_restx import Resource, abort
from datetime import datetime

from helpers.request_handlers import dict_unpack, veb_reformat
from helpers.request_handlers import get_request_body, single_unpack
import helpers.validators as validators
import helpers.utils as utils
import helpers.dbman as db
import helpers.dbutils as dbutils
import secrets
import helpers.auth as auth
import config
import helpers.emailer as emailer
import helpers.account_helper as account

accmgr = api.namespace('account', description='Account management')
FIELDS_BASIC = ("email", "username") 
FIELDS_WITH_DESC = ("username", "description")


def validate_account_details(body):
    """Validate account details."""
    veb = utils.ValidationErrorListBuilder()
    # get the universally mandatory fields
    (field_email, field_username) = dict_unpack(
        body,
        *FIELDS_BASIC,
        required=True, strlenmin=2, strlenmax=255, strtrim=True)
    # validate these fields
    body["email"] = validators.validate_and_format_email(field_email, veb)
    body["username"] = account.validate_register_username(field_username)
    if body["username"] is None:
        veb.add("username", "Required value not found.")
    if len(veb):
        abort(400, veb_reformat(veb))

def validate_change_account_details(body, uid):
    """Validate account details."""
    veb = utils.ValidationErrorListBuilder()
    # get the universally mandatory fields
    (field_username, field_description) = dict_unpack(
        body,
        "username", "description",
        required=False, strlenmin=2, strlenmax=255, strtrim=True)
    body["description"] = validators.validate_string("description", field_description, default="", minlen=0, veb=veb)
    body["username"] = account.validate_edit_username(field_username, uid)
    if body["username"] is None:
        veb.add("username", "Required value not found.")
    if len(veb):
        abort(400, veb_reformat(veb))


@accmgr.route('/register')
class Register(Resource):
    """Handles user registration."""

    @accmgr.doc(
        description="On success, return HTTP 200 with the string 'success'")
    @accmgr.expect(models.register_account_details)
    @accmgr.response(201, 'Registration success')
    @accmgr.response(
        400,
        'Wrong input format or input does not pass validation',
        models.validation_error)
    @accmgr.response(403, 'Identifier (email) exists', models.validation_error)
    def post(self):
        """Register a user."""
        body = get_request_body()
        # this function might make some modification to body
        validate_account_details(body)

        with db.get_dbconn() as conn:
            # check if email is not used by anyone else
            email = body["email"]
            cur = conn.cursor()
            cur.execute("select uid from userident where field=%s and data=%s",
                        ("email", email))
            if single_unpack(cur.fetchone()) is not None:
                abort(403, "This email address is already registered.")

            # OK. let's create a new user. Let's generate a random ID first.
            while True:
                uid = utils.gen_id()
                cur.execute("select uid from users where uid=%s", (uid,))
                if single_unpack(cur.fetchone()) is None:
                    break

            # account is not blocked, but they won't have means for
            # authentication
            dbutils.simple_insert(cur, "users", ("uid", "blocked"),
                                  value=(uid, False))

            # fill up the remaining profile data
            querystr = ("insert into userprofiles (uid,field,data) "
                        "values (%s,%s,%s)")
            # (generic)
            for field in FIELDS_BASIC:
                cur.execute(querystr, (uid, field, body[field]))

            # add registration date
            now = datetime.now()
            cur.execute(
                "insert into userprofiles (field,data,uid) values (%s,%s,%s)",
                ("created_on", now, uid))

            # map email to uid
            cur.execute(
                "insert into userident (field,data,uid) values (%s,%s,%s)",
                ("email", email, uid))

        # at this point, the above entries to DB is committed.
        # Now we send a confirmation email separately
        RequestForgetPassword.generate_and_send_pwreset(False, email, uid)
        return "success", 201


@accmgr.route('/iforgot/request')
class RequestForgetPassword(Resource):
    """Handles requests to forget password."""

    @accmgr.response(200, 'Forget password request successful')
    @accmgr.response(403, 'Account associated with the email is blocked')
    @accmgr.response(404, 'Email not found or invalid')
    @accmgr.expect(models.iforgot_request)
    def post(self):
        """Request your password be forgotten."""
        body = get_request_body()
        (email,) = dict_unpack(
            body,
            "email",
            required=True, strlenmin=3, strtrim=True)

        with db.get_dbconn() as conn:
            cur = conn.cursor()
            cur.execute(
                "select uid from userident where field=%s and data=%s",
                ("email", email))
            uid = single_unpack(cur.fetchone())
            if uid is None:
                abort(404, "Email address not found.")
            cur.execute("select blocked from users where uid=%s", (uid,))
            blocked = single_unpack(cur.fetchone())

        if blocked:
            abort(403, "The user is blocked.")

        RequestForgetPassword.generate_and_send_pwreset(True, email, uid)

        return "success"

    @staticmethod
    def generate_and_send_pwreset(iforgot, email, uid):
        """Generate and send password reset."""
        token = auth.store_token_data(auth.TOKEN_TYPE_FORGOT_PASSWORD, uid)
        emailer.send_template_email_reset_password(iforgot, email, token)


@accmgr.route('/iforgot/verify/<token>')
class VerifyForgetPassword(Resource):
    """Verify a password has been forgotten."""

    @accmgr.response(200, 'Verify forget password success', models.auth_token)
    @accmgr.response(404, 'Token not found, expired, or invalid')
    def get(self, token):
        """Get verification that your password has been forgotten."""
        def fail():
            abort(404, "Invalid token.")

        (tokentype, uid) = auth.read_token_data(token)
        if tokentype != auth.TOKEN_TYPE_FORGOT_PASSWORD:
            fail()

        # invalidate this token once it is used
        auth.invalidate_token(token)

        # now check if UID exists and active
        with db.get_dbconn() as conn:
            cur = conn.cursor()
            if not auth.check_user_valid(cur, uid):
                fail()

        # OK! and let's give caller a password reset token!
        pwrsttok = auth.store_token_data(auth.TOKEN_TYPE_PASSWORD_RESET, uid)
        return {
            "token": pwrsttok
        }


@accmgr.route('/iforgot/resetpw')
class ResetPassword(Resource):
    """Handles password reset."""

    @accmgr.expect(models.iforgot_reset)
    @accmgr.response(200, 'Reset password success')
    @accmgr.response(404, 'Token not found, expired, or invalid')
    def post(self):
        """Reset password."""
        def fail():
            abort(404, "Invalid token.")

        body = get_request_body()
        (token, newpassword) = dict_unpack(
            body,
            "token", "password",
            required=True,
            type=str,
            strlenmax=config.PASSPHRASE_MAX_LEN,
            strlenmin=config.PASSPHRASE_MIN_LEN)

        (tokentype, uid) = auth.read_token_data(token)
        if tokentype != auth.TOKEN_TYPE_PASSWORD_RESET:
            fail()

        # invalidate this token once it is used
        auth.invalidate_token(token)

        with db.get_dbconn() as conn:
            cur = conn.cursor()
            # verify that the user exists and not blocked
            if not auth.check_user_valid(cur, uid):
                fail()

            # generate new password hash
            pwsalt = secrets.token_bytes(config.PASSWORD_SALT_BYTES)
            pwhash = auth.get_pass_hash(newpassword, pwsalt)

            # delete existing password
            cur.execute(
                "delete from authdata where uid=%s and authtype in %s",
                (uid, ("pw", "pwsalt")))
            # insert new pass!
            cur.execute(("insert into authdata (uid,authtype,data) values"
                        "(%s, %s, %s), (%s, %s, %s)"),
                        (uid, "pw", pwhash, uid, "pwsalt", pwsalt.hex()))

        return "success"


@accmgr.route('/my')
class Account(Resource):
    """Handles account details."""

    @accmgr.response(200, 'Get account details success',
                     models.get_account_details)
    def get(self):
        """Get account details."""
        result = {field: None for field in FIELDS_WITH_DESC}
        # authenticate user
        uid = auth.authenticate(request)

        # get user data
        with db.get_dbconn() as conn:
            cur = conn.cursor()

            # collect the data of the user
            querystr = "select field, data from userprofiles where uid = %s"
            cur.execute(querystr, (uid,))
            for (field, value) in cur:
                if field in result:
                    result[field] = value

            querystr = ("select tag, karma from user_tags inner join tags on "
                        "user_tags.tid = tags.tid where uid = %s order by karma")
            cur.execute(querystr, (uid,))
            tags = {}
            total_karma = 0
            for (tag, karma) in cur:
                if karma > 0:
                    tags[tag] = karma
                    total_karma += karma

            result['tags'] = tags
            result['total_karma'] = total_karma

        return result

    @accmgr.response(200, 'update account details success')
    @accmgr.response(400, 'failed to validate data')
    @accmgr.expect(models.change_account_details)
    def put(self):
        """Update account details."""
        # authenticate user
        uid = auth.authenticate(request)
        body = get_request_body()

        # this function might make some modification to body
        validate_change_account_details(body, uid)

        with db.get_dbconn() as conn:
            cur = conn.cursor()

            # query current user profile data
            cur.execute("select field, data from userprofiles where uid=%s",
                        (uid,))
            currdata = {r[0]: r[1] for r in cur}

            # only allow username and desc only to be updated
            fields = set(("username", "description"))

            # determine which field to update. absent values = no update
            for (field, value) in body.items():
                # everything is string. ignore non strings.
                if (type(value) is str) and (field in fields):
                    if field not in currdata:
                        # insert
                        dbutils.simple_insert(cur, "userprofiles",
                                              ("uid", "field", "data"),
                                              value=(uid, field, value))
                    elif field in currdata and currdata[field] != value:
                        # update
                        cur.execute(("update userprofiles set data=%s where "
                                     "field=%s and uid=%s"),
                                    (value, field, uid))

        return "success", 200

@accmgr.route('/<username>/check')
class CheckUsername(Resource):
    @accmgr.doc(
        description="Check if username available or in correct format, On success return HTTP 200 with available username")
    @accmgr.response(
        400,
        'Wrong input format or input does not pass validation',
        models.validation_error)
    @accmgr.response(403, 'username already exists', models.validation_error)
    def post(self, username):
        # validate the ueername 
        username = account.validate_register_username(username)
        
        return username
        
@accmgr.route('/<username>')
class Account(Resource):
    @accmgr.response(200, 'Get account details success',
                     models.get_account_details)
    @accmgr.response(404, 'User not Found')
    def get(self, username):
        """Get account details."""
        result = {field: None for field in FIELDS_WITH_DESC}

        # get user data
        with db.get_dbconn() as conn:
            cur = conn.cursor()
            
            uid = account.get_userid(cur, username) 
            # collect the data of the user
            querystr = "select field, data from userprofiles where uid = %s"
            cur.execute(querystr, (uid,))
            for (field, value) in cur:
                if field in result:
                    result[field] = value

            querystr = ("select tag, karma from user_tags inner join tags on "
                        "user_tags.tid = tags.tid where uid = %s order by karma")
            cur.execute(querystr, (uid,))
            tags = {}
            total_karma = 0
            for (tag, karma) in cur:
                if karma > 0:
                    tags[tag] = karma
                    total_karma += karma

            result['tags'] = tags
            result['total_karma'] = total_karma

        return result
