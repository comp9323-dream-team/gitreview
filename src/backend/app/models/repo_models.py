"""Repo models."""

from mainapp import api
from flask_restx import fields

post_repo = api.model('post_repo', {
    'url': fields.String(
        description='URL of repo to add to GitReview',
        required=True),
    'name': fields.List(
        fields.String(description='Name for the repo'),
        required=True),
    'description': fields.List(
        fields.String(description='Description'),
        required=True),
    'tags': fields.List(
        fields.String(description='Tag for the repo'),
        required=True),
})

repo_key_settings = api.model('repo_key_settings', {
    "key": fields.String(
        description="Private SSH key to use with this repo, in plain text form",
        required=True),
    "key_type": fields.String(
        description="Private SSH key type, eg: OPENSSH_PRIVATE_KEY_V1",
        required=True)
})

tag_model = api.model('tag_model', {
    "tagid": fields.Integer(
        description="Tag id",
        required=True),
    "tag": fields.String(
        description="Repo tag",
        required=True)
})

repo_output = api.model('repo_output', {
    "repoid": fields.Integer(
        description="Repo id",
        required=True),
    "name": fields.String(
        description="Repo name",
        required=True),
    "description": fields.String(
        description="Repo description",
        required=True),
    "url": fields.String(
        description="Repo git url",
        required=True),
    "tags": fields.List(fields.Nested(tag_model))
})

repo_output_list = api.model('repo_output_list', {
    'repo_list': fields.List(fields.Nested(repo_output))
})

repo_owner = api.model('repo_owner', {
    "userid": fields.Integer(
        description="User id of a repo owner.",
        required=True),
    "username": fields.String(
        description="User name of a repo owner.",
        required=True)
})

repo_owners = api.model('repo_owners', {
    "repo_owners": fields.List(fields.Nested(repo_owner))
})


# repo = api.model('repo', {
#     'repo_id': fields.Integer(
#         description='Repo id',
#         required=True
#         ),
#     'name': fields.String(
#         description='Name of repo',
#         required=True
#         ),
#     'description': fields.String(
#         description='Description of repo',
#         required=True
#         ),
#     'tags': fields.List(
#         fields.String(
#         description="Repo tag",
#         required=True
#         ),
#     )
# })

# get_repo_user_id = api.model('get_repo_user_id', {
#     'repo_list': fields.List(
#         fields.Nested(repo),
#         description='Repo details',
#         required=True
#     )
# })
