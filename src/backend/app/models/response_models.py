"""Response_models."""

from mainapp import api
from flask_restx import fields

validation_error_item = api.model('validation_error_item', {
    'name': fields.String(
        description='Field name experiencing validation error.',
        required=True
    ),
    'reason': fields.String(
        description='Reason of the validation error.',
        required=True
    )
})

validation_error_list = api.model('validation_error_list', {
    'fields': fields.List(
        fields.Nested(validation_error_item),
        required=True)
})

validation_error = api.model('validation_error', {
    'message': fields.String(description='Error message')
})

generic_success_id = api.model('generic_success_id', {
  'id': fields.Integer(required=True)
})

comment_model = api.model('comment_model', {
    'comment': fields.String(
            description='Comment',
            required=True
    )
})