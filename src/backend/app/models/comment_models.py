"""Review models."""

from mainapp import api
from flask_restx import fields

commentid_field = fields.Integer(
    description='Comment id',
    required=True
)

revid_field = fields.Integer(
    description='Comment id',
    required=True
)

comment_field = fields.String(
    description='A comment',
    min_length=1,
    required=True
)

edited_field = fields.Boolean(
    description='True if the comment has been edited',
    required=True,
    default=True
)

removed_field = fields.Boolean(
    description='True if the comment has been removed',
    required=True,
    default=True
)

datetime_field = fields.DateTime(
    description='DateTime',
    required=True,
)

post_comment = api.model('post_comment', {
    'comment': comment_field
})

post_review_comment_response = api.model('post_review_comment_response', {
    'commentid': commentid_field,
    'comment': comment_field,
    'revid': revid_field,
    'datetime': datetime_field
})

review_comment = api.model('review_comment', {
    'commentid': commentid_field,
    'comment': comment_field,
    'revid': revid_field,
    'datetime': datetime_field,
    'edited': edited_field,
    'removed': removed_field
})

get_review_comment_list = api.model('get_review_comment_list', {
    'comments': fields.List(fields.Nested(review_comment))
})

put_review_comment_response = api.model('put_review_comment_response', {
    'commentid': commentid_field,
    'comment': comment_field,
    'datetime': datetime_field,
    'edited': edited_field,
})

delete_review_comment_response = api.model('delete_review_comment_response', {
    'commentid': commentid_field,
    'removed': removed_field,
    'datetime': datetime_field
})
