"""Repository models."""

from mainapp import api
from flask_restx import fields

rfrid_field = fields.Integer(
    description="RFR id.",
    required=True)

type_field = fields.String(
    description="Should be bo, tc or ps",
    enum=['bo', 'tc', 'ps'],
    required=True)

desc_field = fields.String(
    description="Description of the RFR",
    required=True)

createdate_field = fields.DateTime(
    description="Date the RFR was created",
    required=True
)

status_field = fields.String(
    description="status of a RFR",
    enum=['accepted','rejected'],
    required=True
)

rfr_owner_field = fields.String(
    description="Owner of the RFR",
    required=True
)

rate_field = fields.Integer(
    description="Can the repo owner rate their own repo? 1 if yes, 0 if not."
)

new_rfr_meta = api.model('new_rfr_meta', {
    'branch': fields.String(
        description="Base commit branch to be applied with revision",
        required=True),
    'type': type_field,
    'desc': desc_field,
    'data': fields.String(
        description="Target commit branch or patchset unidiff")
})

rfr_desc_model = api.model('rfr_desc_model', {
    'desc': desc_field
})


rfr_status = api.model('review_status', {
    'branch' : fields.String(
        description="Branch name to close the RFR",
        required=True),
    'status' : status_field
})

rfr_post_output = api.model('rfr_post_output', {
    'rfrid': rfrid_field
})

rfr_output = api.model('rfr_output', {
    "rfrid": rfrid_field,
    "owner": rfr_owner_field,
    "create_date": createdate_field,
    "status": status_field,
    "rate": rate_field,
    "type": type_field,
    "desc": desc_field
})


rfr_list = api.model('rfr_list', {
    'rfr_list': fields.List(fields.Nested(rfr_output))
})
