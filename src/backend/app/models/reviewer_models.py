"""Reviewer models."""

from mainapp import api
from flask_restx import fields

reviewer_request_list = api.model('reviewer_request_list', {
    "reviewers": fields.List(fields.String(required=True, description="username of the reviewer"))
})