"""Authentication models."""

from mainapp import api
from flask_restx import fields

auth_token = api.model('auth_token', {
  'token': fields.String(required=True)
})

auth_details = api.parser().add_argument(
    'Authorization',
    help="Your Authorization Token in the form 'Token <AUTH_TOKEN>'",
    location='headers'
)

# username/password only for now
login_details = api.model('login_details', {
  'email': fields.String(required=True),
  'password': fields.String(required=True),
})
