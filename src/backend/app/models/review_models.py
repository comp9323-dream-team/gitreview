"""Review models."""

from mainapp import api
from flask_restx import fields

line_model = api.model('line_model', {
    "line": fields.Integer(description="line number"),
    "content": fields.String(description="content of each line in the file")
})

inline_review_model = api.model('inline_review_model', {
    'filename': fields.String(description="name on the file"),
    'old': fields.List(fields.Nested(line_model)),
    'new': fields.List(fields.Nested(line_model))
})

new_review_meta = api.model('new_review_meta', {
    'overall_review': fields.String(
        description="Text of the overall RFR reviews"),
    'inline_reviews': fields.Nested(inline_review_model)

})

review_status = api.model('review_status', {
  'status' : fields.String(required=True, enum=['accepted','rejected'], description="status of a review")
})

review_vote = api.model('review_vote', {
  'vote' : fields.String(required=True, enum=['upvote','downvote'], description="vote a review")
})


revid_field = fields.Integer(
        description='Review id',
        required=True)

review_field = fields.String(
    description='Review string',
    required=True)

create_date_field = fields.DateTime(
    description='Date the review was created',
    required=True)

status_field = fields.String(
    description='Status: Has the review been accepted or rejected?',
    required=True)

published_field = fields.String(
    description='Is the review published?',
    required=True)

revid_response = api.model('revid_model', {
    'revid': revid_field

})

review_response = api.model('review_response', {
    'revid': revid_field,
    'review': review_field,
    'create_date': create_date_field,
    'status': status_field,
    'published': published_field
})

# review_list = api.model(
#     'review_list', fields.List(fields.Nested(review_response)))