"""Account management models."""

from mainapp import api
from flask_restx import fields

get_account_details = api.model('account_details', {
  'username': fields.String(),
  'description' : fields.String(),
  'profile_picture': fields.String(),
  'user_tags': fields.Wildcard(fields.Integer())
})

change_account_details = api.model('account_details', {
  'username': fields.String(),
  'description': fields.String()
})

register_account_details = api.model('register_account_details', {
  'email': fields.String(required=True),
  'username': fields.String(required=True)
})

iforgot_request = api.model('iforgot_request', {
  'email': fields.String(required=True)
})

iforgot_reset = api.model('iforgot_reset', {
  'token': fields.String(required=True),
  'password': fields.String(required=True)
})
