from mainapp import api
from flask_restx import fields

tag_autocomplete_model = api.model('tag_autocomplete_model', {
    'substr': fields.String(
        description="Substring to be use for search for tag")
})
