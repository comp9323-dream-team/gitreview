# Get the image
docker pull postgres


# Run the image
docker container rm -f pg-docker
docker run --rm --name pg-docker --network --ip=172.17.0.2 --hostname=pg-docker -e POSTGRES_PASSWORD=docker -d -p 5432:5432 -v $HOME/docker/volumes/postgres:/var/lib/postgresql/data postgres


# This is an empty db, with db init_db, username postgres and password docker--network
PGPASSWORD=docker psql -h localhost -U postgres -tc "DROP DATABASE IF EXISTS init_db"
PGPASSWORD=docker psql -h localhost -U postgres -tc "CREATE DATABASE init_db"
PGPASSWORD=docker psql -h localhost -U postgres -d init_db < ddl/ddl.psql
PGPASSWORD=docker psql -h localhost -U postgres -d init_db < ddl/basedata.psql

# building flask docker
docker build -t gitreview:dev .

# running flask docker
docker container rm -f backend-docker
docker run --name backend-docker -p 5000:5000 gitreview:dev
