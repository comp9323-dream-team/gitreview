/* eslint-disable linebreak-style */
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import CodeIcon from '@material-ui/icons/Code';

import { ReactComponent as RequestIcon } from '../assets/rfricon.svg';

export default function CustomTabs({ tab, repoId }) {
  const [value, setValue] = React.useState(tab);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Paper square>
      <Tabs
        value={value}
        indicatorColor="primary"
        textColor="primary"
        onChange={handleChange}
        aria-label="disabled tabs example"
      >
        <Tab
          label={(
            <div>
              <RequestIcon style={{ verticalAlign: 'top', paddingRight: '8px', paddingTop: '4px' }} />
              Request for Review
            </div>
        )}
          component={Link}
          to={`/repo/view/${repoId}/rfr`}
        />
        <Tab
          label={(
            <div>
              <CodeIcon fontSize="small" style={{ verticalAlign: 'top', paddingRight: '8px', paddingTop: '2px' }} />
              code
            </div>
        )}
          component={Link}
          to={`/repo/view/${repoId}/code`}
        />
      </Tabs>
    </Paper>
  );
}

CustomTabs.propTypes = {
  tab: PropTypes.number.isRequired,
  repoId: PropTypes.number.isRequired,
};
