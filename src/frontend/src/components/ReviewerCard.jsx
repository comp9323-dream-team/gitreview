/* eslint-disable  */
import React from 'react';
// import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
// import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import { makeStyles, Badge } from '@material-ui/core';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    padding: '20px',
    alignItems: 'center',
    marginBottom: '32px',
    width: '420px',
  },
  name: {
    margin: '0 50px 0 20px',
    fontSize: '18px',
    flexGrow: 1,
  },
  tag: {
    margin: '4px',
    padding: '4px',
  },
}));

function ReviewerCard({ username, karma, tags }) {
  const classes = useStyles();
  return (
    <Paper elevation={2} className={classes.root}>
      <Typography component="p" className={classes.name}>{username}</Typography>
      {tags.length ? tags.map((tag) => {
        for(const [ name, tagKarma] of Object.entries(tag)) {
          return (
        <Badge badgeContent={tagKarma} color="secondary">
          <Chip
          size="small"
          label={name}
          color="primary"
          className={classes.tag}
          />
        </Badge>
      )}
      }) : null}
    </Paper>
  );
}

export default ReviewerCard;
