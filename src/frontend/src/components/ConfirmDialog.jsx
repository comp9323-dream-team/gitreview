import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function ConfirmDialog(props) {
  const {
    caption, text, open, setOpen, actionYes, actionNo, okOnly,
  } = props;

  const handleConfirm = (confirmed) => () => {
    const action = confirmed ? actionYes : actionNo;
    if (action) {
      action();
    }
    setOpen(false);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{caption}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {text}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleConfirm(false)} color="primary" hidden={okOnly}>
          No
        </Button>
        <Button onClick={handleConfirm(true)} color="primary" autoFocus>
          {okOnly ? 'OK' : 'Yes'}
        </Button>
      </DialogActions>
    </Dialog>
  );
}

ConfirmDialog.propTypes = {
  caption: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  setOpen: PropTypes.func.isRequired,
  actionYes: PropTypes.func,
  actionNo: PropTypes.func,
  okOnly: PropTypes.bool,
};

ConfirmDialog.defaultProps = {
  actionYes: null,
  actionNo: null,
  okOnly: false,
};
