import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core';
import Chip from '@material-ui/core/Chip';

const useStyles = makeStyles((theme) => ({
  root: {
    textDecoration: 'none',
    position: 'relative',
    boxSizing: 'border-box',
  },
  description: {
    marginTop: theme.spacing(1),
    maxHeight: '3.6em',
  },
  repoNameHeading: {
    wordBreak: 'break-all',
    overflow: 'hidden',
    maxHeight: '4.8em',
  },
  tag: {
    padding: '4px',
    position: 'absolute',
    bottom: '15px',
  },
}));

function RepoCard(props) {
  const { className, repoObject } = props;
  const classes = useStyles();

  const getTagElement = () => {
    if (repoObject.tags) {
      if (repoObject.tags.length) {
        // first tag only
        return (
          <Chip size="small" label={repoObject.tags[0]} color="primary" className={classes.tag} />
        );
      }
    }
    return null;
  };

  return (
    <Link to={`/repo/view/${repoObject.repoid}/rfr`} className={classes.root}>
      <Card className={className}>
        <CardContent>
          <Typography className={classes.repoNameHeading} variant="subtitle1" color="primary" component="h2" gutterBottom>
            {repoObject.name || repoObject.url}
          </Typography>
          <Typography variant="caption" color="textSecondary" component="p" className={classes.description}>
            {repoObject.description || ''}
          </Typography>
          {getTagElement()}
        </CardContent>
      </Card>
    </Link>
  );
}

RepoCard.defaultProps = {
  className: '',
};

RepoCard.propTypes = {
  repoObject: PropTypes.shape({
    repoid: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    description: PropTypes.string,
    tags: PropTypes.arrayOf(PropTypes.string),
  }).isRequired,
  className: PropTypes.string,
};

export default RepoCard;
