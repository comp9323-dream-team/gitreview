/* eslint-disable linebreak-style */
/* eslint-disable no-unused-vars, no-debugger */
import React, { useEffect } from 'react';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import { makeStyles } from '@material-ui/core';
import NotificationsNoneIcon from '@material-ui/icons/NotificationsNone';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import Avatar from '@material-ui/core/Avatar';
import Axios from 'axios';
import { NavLink } from 'react-router-dom';

import logo from '../assets/gitreview_logo.png';
import ProfileMenu from './ProfileMenu';
import { newRequest, getErrorMessage, getEndpointUri } from '../libs/utils';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '64px',
    backgroundColor: '#DDDDDE',
    padding: '10px 35px 0px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  search: {
    width: '25em',
    height: '40px',
    backgroundColor: '#ffffff',
    color: '#000000',
    marginBottom: '10px',
  },
  icon: {
    color: '#ffffff',
    cursor: 'pointer',
  },
  notifIcon: {
    display: 'none',
  },
  avatar: {
    width: theme.spacing(4),
    height: theme.spacing(4),
    cursor: 'pointer',
  },
  profileContainer: {
    display: 'flex',
    // alignItems: 'center',
    '& > *': {
      margin: '8px',
    },
  },
  avatarContainer: {
    display: 'flex',
    // alignItems: 'center',
  },
}));

function Navbar() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [avatar, setAvatar] = React.useState(null);
  const [initial, setInitial] = React.useState('G');
  const [user, setUser] = React.useState(null);

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  useEffect(async () => {
    // load basic user info
    newRequest('account/my')
      .then((res) => {
        setUser(res.data);
        if (res.data.username) {
          setInitial(res.data.username[0].toUpperCase());
        }
      })
      .catch((err) => {
        console.log(`NavBar: error loading profile data: ${getErrorMessage(err)}`);
      });
    newRequest('photos/profpic/my', 'get', null, null, true)
      .then((res) => {
        const urlPic = window.URL.createObjectURL(res.data);
        setAvatar(urlPic);
      })
      .catch((err) => {
        console.log(`NavBar: error loading profile picture: ${getErrorMessage(err)}`);
      });
  }, []);

  return (
    <header>
      <div className={classes.root}>
        <NavLink to="/dashboard">
          <img src={logo} alt="GitReview Logo" width="200" height="30" />
        </NavLink>
        <OutlinedInput type="search" placeholder="Search repository or user" className={classes.search} />
        <div className={classes.profileContainer}>
          <NotificationsNoneIcon fontSize="large" className={`${classes.icon} ${classes.notifIcon}`} />
          <div className={classes.avatarContainer}>
            {avatar ? (
              <Avatar src={avatar || initial} className={classes.avatar} onClick={handleMenu} />
            ) : (
              <Avatar className={classes.avatar} onClick={handleMenu}>
                {initial}
              </Avatar>
            )}
            <ArrowDropDownIcon className={classes.icon} onClick={handleMenu} />
            <ProfileMenu anchorEl={anchorEl} setAnchorEl={setAnchorEl} user={user} />
          </div>
        </div>
      </div>
    </header>
  );
}

export default Navbar;
