import React from 'react';
import PropTypes from 'prop-types';
// import { Link, useRouteMatch } from 'react-router-dom';
import { List } from '@material-ui/core';
import ReviewItem from './ReviewItem';

function ReviewListTable(props) {
  const { reviews } = props;
  return (
    <>
      <List component="nav" aria-label="list-of-reviews">
        {reviews.map((review) => (
          <ReviewItem
            key={review.context}
            context={review.context}
            message={review.message}
            reviewer={review.reviewer}
            date={review.date}
          />
        ))}
      </List>
    </>
  );
}

ReviewListTable.defaultProps = {
  reviews: [],
};

ReviewListTable.propTypes = {
  reviews: PropTypes.arrayOf(PropTypes.object),
};

export default ReviewListTable;
