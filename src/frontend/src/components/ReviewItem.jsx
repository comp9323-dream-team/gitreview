/* eslint-disable react/prop-types */
import React from 'react';
import { Link } from 'react-router-dom';
import {
  Divider,
  IconButton,
  makeStyles,
  Paper,
  Typography,
  Button,
} from '@material-ui/core';
// import moment from 'moment';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import CreateIcon from '@material-ui/icons/Create';
// useRouteMatch
/*
const requestTimeFromNow = (date) => {
  const requestTime = moment(date);
  return moment(requestTime).fromNow();
};
*/
const useStyles = makeStyles(() => ({
  lisDiv: {
    alignItems: 'center',
  },
  upvote: {
    fill: 'green',
  },
  downvote: {
    fill: 'red',
  },
  root: {
    display: 'flex',
    flexDirection: 'column',
    padding: '12px',
    margin: '8px 0',
  },
  topRow: {
    display: 'flex',
    marginBottom: '4px',
    alignItems: 'center',
  },
  desc: {
    fontSize: '18px',
    fontWeight: '600',
    textDecoration: 'none',
    paddingLeft: '4px',
    color: 'black',
    '&:hover': {
      color: '#3f51b5',
    },
  },
  link: {
    color: 'black',
    textDecoration: 'none',
    '&:hover': {
      color: '#3f51b5',
    },
  },
  bottomeRow: {
    display: 'flex',
    paddingLeft: '20px',
  },
  button: {
    width: '50px',
  },
  widget: {
    alignSelf: 'flex-end',
  },
}));

function ReviewItem({ reviewObj }) {
  const classes = useStyles();
  // const { path } = useRouteMatch();

  return (
    <>
      <Paper className={classes.root} variant="outlined">
        <div className={classes.topRow}>
          <CreateIcon status={reviewObj.status} color={reviewObj.status === 'active' ? 'primary' : 'disabled'} />
          <Typography variant="subtitle1" component={Link} to={`/review/view/${reviewObj.revid}/`} className={classes.desc}>
            {reviewObj.reviewer}
            {' - '}
            {reviewObj.create_date}
          </Typography>
        </div>
        <div className={classes.bottomeRow}>
          <Typography component="p" variant="subtitle2" color="textSecondary">
            {
              (reviewObj?.review && reviewObj.review.length) ? reviewObj.review : '(No Review Description Provided)'
            }
          </Typography>
        </div>
        <div className={classes.widget}>
          <IconButton className={classes.button}>
            <ArrowDropUpIcon className={classes.upvote} />
          </IconButton>
          <IconButton className={classes.button}>
            <ArrowDropDownIcon className={classes.downvote} />
          </IconButton>
        </div>
        <div className={classes.widget}>
          <Button variant="outlined" color="primary">
            Comment
          </Button>
        </div>
      </Paper>
      <Divider />
    </>
  );
}

export default ReviewItem;
