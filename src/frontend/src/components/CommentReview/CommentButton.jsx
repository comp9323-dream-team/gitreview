/* eslint-disable react/prop-types */
import React, { useState, useCallback } from 'react';
import { Button, makeStyles } from '@material-ui/core';
import { useImmer } from './hooks';
import MarkdownComment from '../MarkdownComment';
// import { propTypes } from 'react-markdown';

const useStyles = makeStyles(() => ({
  input: {
    marginTop: '30px',
    marginBottom: '30px',
  },
  button: {
    marginBottom: '30px',
  },
}));

export const useConversations = () => {
  const [conversations, dispatch] = useImmer((state, action) => {
    switch (action.type) {
      case 'INIT':
        // eslint-disable-next-line no-param-reassign
        state[action.payload.key] = { comments: [] };
        break;
      case 'COMMENT': {
        const { key, content } = action.payload;
        const conversation = state[key];
        conversation.comments.push(content);
        break;
      }
      default:
        break;
    }
  }, {});
  const initConversation = useCallback((key) => dispatch({ type: 'INIT', payload: { key } }), [dispatch]);
  const addComment = useCallback((key, content) => dispatch({ type: 'COMMENT', payload: { key, content } }), [dispatch]);
  return [conversations, { initConversation, addComment }];
};

const Comment = ({ content }) => <div className="comment">{content}</div>;
/* Comment.propTypes = {
  content: propTypes.string.isRequired,
}; */

const Editor = ({ onSubmit }) => {
  const classes = useStyles();
  const [value, setValue] = useState('');
  // const updateValue = useCallback((e) => setValue(e.target.value), []);
  const submitDraft = useCallback(() => {
    onSubmit(value);
    setValue('');
  }, [value, onSubmit]);

  return (
    <div id="editor">
      <MarkdownComment />
      <Button className={classes.button} variant="outlined" color="primary" onClick={submitDraft}>
        Add Comment
      </Button>
    </div>
  );
};

/* Editor.propTypes = {
  onSubmit: propTypes.func.isRequired,
}; */

export const Conversation = ({ changeKey, comments, onSubmitComment }) => {
  const submitComment = useCallback((content) => onSubmitComment(
    changeKey, content,
  ), [changeKey, onSubmitComment]);

  return (
    <div className="conversation">
      {comments.map((comment) => (
        <Comment key={comment} content={comment} />
      ))}
      <Editor onSubmit={submitComment} />
    </div>
  );
};

/*
<TextField
        value={value}
        onChange={updateValue}
        fullWidth="true"
        multiline="true"
        rows="7"
        variant="outlined"
        className={classes.input}
        size="medium"
      />
TextField
*/
