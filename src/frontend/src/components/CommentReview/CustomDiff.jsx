/* eslint-disable react/prop-types */
/* eslint-disable no-shadow */
/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { propTypes } from 'react-markdown';
import { mapValues } from 'lodash';
import { diffLines, formatLines } from 'unidiff';
import {
  parseDiff,
  Diff,
  Hunk,
  getChangeKey,
} from 'react-diff-view';
import { makeStyles } from '@material-ui/core';
// import { useInput } from './hooks';
import { useConversations, Conversation } from './CommentButton';

import 'antd/dist/antd.min.css';
import 'react-diff-view/style/index.css';
// import './styles.css';

const useStyles = makeStyles(() => ({
  hunk: {
    marginTop: '15px',
    marginBottom: '15px',
  },
}));

const EMPTY_HUNKS = [];

function CustomDiff(props) {
  const classes = useStyles();
  const { oldfile, newfile } = props;
  const diffText = formatLines(diffLines(oldfile, newfile), { context: 3 });
  const [diff] = parseDiff(diffText, { nearbySequences: 'zip' });
  const { type, hunks } = diff;
  const [conversations, { initConversation, addComment }] = useConversations();
  const codeEvents = {
    onDoubleClick({ change }) {
      const key = getChangeKey(change);
      if (!conversations[key]) {
        initConversation(key);
      }
    },
  };
  const widgets = mapValues(conversations, ({ comments }, changeKey) => (
    <Conversation changeKey={changeKey} comments={comments} onSubmitComment={addComment} />
  ));

  return (
    <div>
      <Diff viewType="split" diffType={type} hunks={hunks || EMPTY_HUNKS}>
        {(hunks) => hunks.map((hunk) => (
          <Hunk
            key={hunk.content}
            hunk={hunk}
            codeEvents={codeEvents}
            widgets={widgets}
            className={classes.hunk}
          />
        ))}
      </Diff>
    </div>
  );
}

CustomDiff.propTypes = {
  oldfile: propTypes.string,
  newfile: propTypes.string,
};

CustomDiff.defaultProps = {
  oldfile: '',
  newfile: '',
};

export default CustomDiff;
