/* eslint-disable react/jsx-props-no-spreading */
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import Chip from '@material-ui/core/Chip';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
  chip: {
    margin: theme.spacing(0.5, 0.25),
  },
}));

export default function TextFieldWithChip({ ...props }) {
  const classes = useStyles();
  const {
    placeholder, tags, onChange, ...otherProps
  } = props;
  const [inputValue, setInputValue] = React.useState('');
  const [tagListInt, setTagListInt] = React.useState([]);

  useEffect(() => {
    setTagListInt(tags);
  }, [tags]);

  function listChange(newList) {
    setTagListInt(newList);
    if (onChange) {
      onChange(newList);
    }
  }

  function appendItem() {
    const newVal = inputValue.toLowerCase();
    setInputValue('');

    // ignore dupe values
    const duplicatedValues = tagListInt.indexOf(newVal);
    if (duplicatedValues !== -1) {
      return;
    }

    // ignore whitespace only inputs
    if (!newVal.length) {
      return;
    }

    listChange([...tagListInt, newVal]);
  }

  function handleKeyDown(event) {
    if (event.key === ' ') {
      event.preventDefault();
      appendItem();
    } else if (event.key === 'Enter') {
      appendItem();
    } else if (
      tagListInt.length
      && !inputValue.length
      && event.key === 'Backspace'
    ) {
      listChange(tagListInt.slice(0, tagListInt.length - 1));
    }
  }

  const handleDelete = (item) => () => {
    const newTagList = [...tagListInt];
    newTagList.splice(newTagList.indexOf(item), 1);
    listChange(newTagList);
  };

  function handleInputChange(event) {
    setInputValue(event.target.value);
  }

  return (
    <TextField
      InputProps={{
        startAdornment: tagListInt.map((item) => (
          <Chip
            key={item}
            tabIndex={-1}
            label={item}
            className={classes.chip}
            onDelete={handleDelete(item)}
          />
        )),
      }}
      onChange={handleInputChange}
      onKeyDown={handleKeyDown}
      onBlur={appendItem}
      value={inputValue}
      {...otherProps}
    />
  );
}
TextFieldWithChip.defaultProps = {
  tags: [],
  onChange: null,
  placeholder: '',
};
TextFieldWithChip.propTypes = {
  tags: PropTypes.arrayOf(PropTypes.string),
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
};
