/* eslint-disable linebreak-style */
/* eslint-disable */
import React from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { Link } from 'react-router-dom';

export default function CustomTabsRFR({ tab, rfrId }) {
  return (
    <Paper square>
      <Tabs
        value={tab}
        indicatorColor="primary"
        textColor="primary"
      >
        <Tab label="Overview" component={Link} to={`/rfr/${rfrId}/overview`} />
        <Tab label="Code Changes" component={Link} to={`/rfr/${rfrId}/code`} />
      </Tabs>
    </Paper>
  );
}

CustomTabsRFR.propTypes = {
  tab: PropTypes.number.isRequired,
};
