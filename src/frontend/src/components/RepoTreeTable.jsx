/* eslint-disable linebreak-style,jsx-a11y/anchor-is-valid,react/forbid-prop-types */
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import FolderIcon from '@material-ui/icons/Folder';
import DescriptionIcon from '@material-ui/icons/Description';
import ReplyIcon from '@material-ui/icons/Reply';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const useStyles = makeStyles({
  root: {
    marginTop: '20px',
  },
  treeLink: {
    display: 'flex',
    alignItems: 'center',
  },
  treeItemIcon: {
    marginRight: '8px',
  },
});

export default function RepoTreeTable({
  dirTree, onPathChange, onFileOpen, hidden,
}) {
  const classes = useStyles();
  const [parents, setParents] = React.useState([]);
  const [dirContent, setDirContent] = React.useState([]);

  React.useEffect(() => {
    setParents([]);
    setDirContent(dirTree);
  }, [dirTree]);

  React.useEffect(() => {
    // call event subscriber with an array of path name
    if (onPathChange) {
      onPathChange(parents.map((v) => v.name));
    }
  }, [parents]);

  const handleOpenLink = (obj) => {
    if (obj.type === 'TREE') {
      const newParents = [...parents, {
        name: obj.name,
        dir: [...dirContent],
      }];
      setDirContent(obj.items || []);
      setParents(newParents);
    } else if (onFileOpen) {
      const paths = parents.map((v) => v.name);
      onFileOpen(paths, obj.name);
    }
  };

  const handleParentOpenLink = () => {
    if (parents.length) {
      const newParentList = [...parents];
      const parentDir = newParentList.pop();
      setParents(newParentList);
      setDirContent(parentDir.dir);
    }
  };

  const upOneLevelItem = (
    <TableRow hidden={!parents.length}>
      <TableCell component="th" scope="row">
        <Link className={classes.treeLink} onClick={() => handleParentOpenLink()}>
          <ArrowUpwardIcon className={classes.treeItemIcon} />
          <span>(Up one level)</span>
        </Link>
      </TableCell>
    </TableRow>
  );

  const generateIcon = (kind) => {
    switch (kind) {
      case 'TREE':
        return (<FolderIcon className={classes.treeItemIcon} />);
      case 'FILE':
        return (<DescriptionIcon className={classes.treeItemIcon} />);
      case 'LINK':
        return (<ReplyIcon className={classes.treeItemIcon} />);
      default:
        return (<HelpOutlineIcon className={classes.treeItemIcon} />);
    }
  };

  const dirItems = dirContent.map((obj) => (
    <TableRow>
      <TableCell component="th" scope="row">
        <Link className={classes.treeLink} onClick={() => handleOpenLink(obj)}>
          {generateIcon(obj.type)}
          <span>{obj.name}</span>
        </Link>
      </TableCell>
    </TableRow>
  ));

  return (
    <TableContainer component={Paper} className={classes.root} hidden={hidden}>
      <Table aria-label="simple table">
        <TableBody>
          {upOneLevelItem}
          {dirItems}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

RepoTreeTable.propTypes = {
  dirTree: PropTypes.object.isRequired,
  onPathChange: PropTypes.func,
  onFileOpen: PropTypes.func,
  hidden: PropTypes.bool,
};

RepoTreeTable.defaultProps = {
  hidden: false,
  onPathChange: null,
  onFileOpen: null,
};
