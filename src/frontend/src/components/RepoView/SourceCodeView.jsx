import React from 'react';
import {
  Button, Typography, Container, makeStyles, LinearProgress, Breadcrumbs, Paper,
} from '@material-ui/core';
import FolderOpenIcon from '@material-ui/icons/FolderOpen';
import SaveAltIcon from '@material-ui/icons/SaveAlt';
import PropTypes from 'prop-types';
import BranchSelect from '../BranchSelect';
import ConfirmDialog from '../ConfirmDialog';
import RepoTreeTable from '../RepoTreeTable';
import FetchButton from '../FetchButton';
import CodeViewer from '../CodeViewer';
import { getErrorMessage, newRequest, getEndpointUri } from '../../libs/utils';

const useStyles = makeStyles(() => ({
  root: {
  },
  controlBox: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '16px',
    alignItems: 'center',
  },
  codeCont: {
    marginTop: '16px',
    overflowX: 'hidden',
  },
}));

export default function SourceCodeView({ className, repoId }) {
  const classes = useStyles();
  const [isLoading, setIsLoading] = React.useState(false);
  const [isFetching, setIsFetching] = React.useState(false);

  const [updateHeads, setUpdateHeads] = React.useState(0);
  const [selectedHead, setSelectedHead] = React.useState('');

  const [msgBoxTitle, setMsgBoxTitle] = React.useState('');
  const [msgBoxText, setMsgBoxText] = React.useState('');
  const [msgBoxOpen, setMsgBoxOpen] = React.useState(false);

  const [browseDir, setBrowseDir] = React.useState([]);
  const [browsePath, setBrowsePath] = React.useState([]);
  const [fileContent, setFileContent] = React.useState(null);
  const [isBinary, setIsBinary] = React.useState(false);
  const [downloadTarget, setDownloadTarget] = React.useState(null);

  const getTree = async (commit) => {
    setIsLoading(true);
    try {
      const res = await newRequest(`repo/${repoId}/browse/${commit}`);
      setBrowseDir(res.data);
    } catch (err) {
      setMsgBoxTitle('Error Browsing Repo');
      setMsgBoxText(getErrorMessage(err));
      setMsgBoxOpen(true);
    } finally {
      setIsLoading(false);
    }
  };

  const handleBranchChange = (newVal) => {
    setSelectedHead(newVal);
    if (newVal !== selectedHead && newVal?.length) {
      getTree(newVal);
    }
  };

  const handleFileOpen = async (path, fileName) => {
    setIsLoading(true);
    setFileContent(null);
    setIsBinary(false);
    const newPath = [...path, fileName];
    const downloadTargetNewUri = getEndpointUri(`repo/${repoId}/download/${selectedHead}/${newPath.join('/')}`);
    try {
      const res = await newRequest(`repo/${repoId}/lines/${selectedHead}/${newPath.join('/')}`);
      setDownloadTarget(downloadTargetNewUri);
      setBrowsePath(newPath);
      setFileContent(res.data);
    } catch (err) {
      if (err.response.status === 415) {
        setIsBinary(true);
        setDownloadTarget(downloadTargetNewUri);
        setFileContent([]);
        setBrowsePath(newPath);
      } else {
        setMsgBoxTitle('Error Opening File');
        setMsgBoxText(getErrorMessage(err));
        setMsgBoxOpen(true);
      }
    } finally {
      setIsLoading(false);
    }
  };

  const handleBackToFolder = () => {
    setFileContent(null);
    const newPath = [...browsePath];
    newPath.pop();
    setBrowsePath(newPath);
  };

  return (
    <Container className={`${className} ${classes.root}`}>
      <ConfirmDialog
        caption={msgBoxTitle}
        text={msgBoxText}
        setOpen={setMsgBoxOpen}
        open={msgBoxOpen}
        okOnly
      />
      <div className={classes.controlBox}>
        <div>
          <BranchSelect
            repoId={repoId}
            onChange={handleBranchChange}
            forceUpdate={updateHeads}
            disabled={isFetching || isLoading}
          />
          <Typography variant="body1">
            <small>
              <code>{selectedHead || ''}</code>
            </small>
          </Typography>
        </div>
        <FetchButton
          repoId={repoId}
          setIsFetchingProgress={setIsFetching}
          onFetchSuccess={() => setUpdateHeads(updateHeads + 1)}
        />
      </div>
      <LinearProgress fullWidth hidden={!isLoading} />
      <Breadcrumbs>
        {
          browsePath.map((obj) => (
            <Typography>{obj}</Typography>
          ))
        }
      </Breadcrumbs>
      <RepoTreeTable
        dirTree={browseDir}
        onPathChange={(p) => setBrowsePath(p)}
        onFileOpen={handleFileOpen}
        hidden={fileContent}
      />
      <Button
        style={{ margin: '8px' }}
        variant="contained"
        color="secondary"
        hidden={!fileContent}
        onClick={handleBackToFolder}
        startIcon={<FolderOpenIcon />}
      >
        Back To Folder
      </Button>
      <a
        target="_blank"
        href={downloadTarget}
        rel="noreferrer"
      >
        <Button
          style={{ margin: '8px' }}
          variant="contained"
          color="primary"
          hidden={!fileContent}
          startIcon={<SaveAltIcon />}
        >
          Download
        </Button>
      </a>
      <Paper hidden={!fileContent} className={classes.codeCont}>
        {
          isBinary
            ? ('Cannot view binary file. You can still download the file.')
            : (<CodeViewer left={fileContent} />)
        }
      </Paper>
    </Container>
  );
}

SourceCodeView.propTypes = {
  className: PropTypes.string,
  repoId: PropTypes.number.isRequired,
};

SourceCodeView.defaultProps = {
  className: '',
};
