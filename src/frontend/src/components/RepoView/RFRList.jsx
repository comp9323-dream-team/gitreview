/* eslint-disable react/prop-types */
/* eslint-disable */
import React, { useEffect, useState } from 'react';
import {
  Button, Typography, Container, makeStyles, LinearProgress,
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import { newRequest, getErrorMessage } from '../../libs/utils';
import Notification from '../UserCreation/Notification';
import RFRItem from '../RFRItem';

const useStyles = makeStyles(() => ({
  root: {
  },
  controlBox: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '16px',
  },
  rfrList: {
    display: 'flex',
    flexDirection: 'column',
  },
}));
export default function RFRList({ className, repoId }) {
  const classes = useStyles();
  const [rfrLoading, setRfrLoading] = useState(false);
  const [rfrs, setRfrs] = useState([]);
  const [notify, setNotify] = useState({
    isOpen: false,
    message: '',
    type: '',
  });

  useEffect(async () => {
    setRfrLoading(true);
    try {
      const res = await newRequest(`rfr/byrepo/${repoId}`);
      console.log(res.data);
      setRfrs(res.data.rfr_list || []);
    } catch (err) {
      setNotify({
        isOpen: true,
        message: `Error getting your list of Request for Reviews: ${getErrorMessage(err)}`,
        type: 'error',
      });
    } finally {
      setRfrLoading(false);
    }
  }, [repoId]);

  return (
    <Container className={`${className} ${classes.root}`}>
      <Notification notify={notify} setNotify={setNotify} />
      <div className={classes.controlBox}>
        <Typography variant="h5" component="h2">List of RFRs</Typography>
        <Link to={`/repo/${repoId}/newrfr`}>
          <Button variant="contained" color="primary">
            Add new RFR
          </Button>
        </Link>
      </div>
      <div className={classes.rfrList}>
        <LinearProgress fullWidth style={{ width: '100%' }} hidden={!rfrLoading} />
        { (rfrs.length)
          ? rfrs.map((rfr) => (
            <RFRItem rfrObj={rfr} />
          ))

          : (
            <Typography align="center" style={{ width: '100%' }}>{rfrLoading ? '(loading)' : 'No Request for Review available'}</Typography>)}
      </div>
    </Container>
  );
}
