import React from 'react';
import PropTypes from 'prop-types';
import {
  Button, LinearProgress,
} from '@material-ui/core';
import ConfirmDialog from './ConfirmDialog';
import { getErrorMessage, newRequest } from '../libs/utils';

export default function FetchButton({
  repoId, setIsFetchingProgress, onFetchSuccess, disabled, className,
}) {
  const [isFetching, setIsFetching] = React.useState(false);
  const [countdown, setCountdown] = React.useState(0);

  const [msgBoxTitle, setMsgBoxTitle] = React.useState('');
  const [msgBoxText, setMsgBoxText] = React.useState('');
  const [msgBoxOpen, setMsgBoxOpen] = React.useState(false);

  // power of closure!
  React.useEffect((() => {
    let timeoutHandle = null;
    return () => {
      if (countdown) {
        if (!timeoutHandle) {
          timeoutHandle = setTimeout(() => {
            setCountdown(countdown - 1);
            timeoutHandle = null;
          }, 1000);
        }
      } else if (timeoutHandle) {
        clearTimeout(timeoutHandle);
        timeoutHandle = null;
      }
    };
  })(), [countdown]);

  const setIsFetchingAll = (status) => {
    setIsFetching(status);
    if (setIsFetchingProgress) {
      setIsFetchingProgress(status);
    }
  };

  const checkFetchCompletion = async () => {
    try {
      const res = await newRequest(`repo/${repoId}/fetch`);
      const { data } = res;
      if (data.busy) {
        // busy: try again later.
        setTimeout(checkFetchCompletion, 1000);
      } else {
        // finish!
        setIsFetchingAll(false);
        setCountdown(data.allow_retry);
        // check error status
        if (data.last_status) {
          setMsgBoxTitle('Fetch Error');
          setMsgBoxText(data.last_status);
          setMsgBoxOpen(true);
        } else if (onFetchSuccess) {
          onFetchSuccess();
        }
      }
    } catch (err) {
      setMsgBoxTitle('Error Getting Fetch Status');
      setMsgBoxText(getErrorMessage(err));
      setIsFetchingAll(false);
    }
  };

  const handleFetch = async () => {
    setIsFetchingAll(true);
    try {
      await newRequest(`repo/${repoId}/fetch`, 'post');
      // OK. repeatedly check for completion status.
      checkFetchCompletion();
    } catch (err) {
      if (err?.response?.status === 429) {
        checkFetchCompletion();
      } else {
        setMsgBoxTitle('Error Initialising Fetch');
        setMsgBoxText(getErrorMessage(err));
        setMsgBoxOpen(true);
        setIsFetchingAll(false);
      }
    }
  };

  const fetchButtonCaption = () => {
    if (isFetching) {
      return 'Fetching...';
    }
    if (countdown) {
      return `Can Fetch Again in ${countdown}`;
    }
    return 'Fetch';
  };

  return (
    <>
      <ConfirmDialog
        caption={msgBoxTitle}
        text={msgBoxText}
        setOpen={setMsgBoxOpen}
        open={msgBoxOpen}
        okOnly
      />
      <div>
        <Button
          variant="contained"
          color="primary"
          className={className}
          disabled={isFetching || countdown || disabled}
          onClick={handleFetch}
        >
          {fetchButtonCaption()}
        </Button>
        <LinearProgress hidden={!isFetching} />
      </div>
    </>
  );
}

FetchButton.propTypes = {
  repoId: PropTypes.number.isRequired,
  setIsFetchingProgress: PropTypes.func,
  onFetchSuccess: PropTypes.func,
  disabled: PropTypes.bool,
  className: PropTypes.string,
};

FetchButton.defaultProps = {
  onFetchSuccess: null,
  setIsFetchingProgress: null,
  disabled: false,
  className: '',
};
