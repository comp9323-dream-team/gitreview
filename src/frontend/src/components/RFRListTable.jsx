import React from 'react';
import PropTypes from 'prop-types';
// import { Link, useRouteMatch } from 'react-router-dom';
import { List } from '@material-ui/core';
import RFRItem from './RFRItem';

function RFRListTable(props) {
  const { rfrs } = props;
  return (
    <>
      <List component="nav" aria-label="list-of-rfr">
        {rfrs.map((rfr) => (
          // <Link to={`${path}/${rfr.name}`}>
          <RFRItem key={rfr.name} name={rfr.name} date={rfr.date} />
          // </Link>
        ))}
      </List>
    </>
  );
}

RFRListTable.defaultProps = {
  rfrs: [],
};

RFRListTable.propTypes = {
  rfrs: PropTypes.arrayOf(PropTypes.object),
};

export default RFRListTable;
