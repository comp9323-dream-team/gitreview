/* eslint-disable react/prop-types */
import React from 'react';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { useHistory } from 'react-router-dom';
import ConfirmDialog from './ConfirmDialog';
import { newRequest } from '../libs/utils';

function ProfileMenu({ anchorEl, setAnchorEl }) {
  const history = useHistory();
  const [logoutOpen, setLogoutOpen] = React.useState(false);
  const handleClose = () => {
    setAnchorEl(null);
  };

  const confirmLogout = () => {
    setLogoutOpen(true);
    handleClose();
  };

  const logout = () => {
    newRequest('auth/logout')
      .then(() => {
        localStorage.clear();
        history.push('/');
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const profile = () => {
    newRequest('account/my')
      .then((res) => {
        history.push(`/profile/${res.data.username}`);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={profile}>Profile</MenuItem>
        <MenuItem onClick={confirmLogout}>Logout</MenuItem>
      </Menu>
      <ConfirmDialog
        open={logoutOpen}
        setOpen={setLogoutOpen}
        caption="Logout GitReview"
        text="Are you sure you want to log out?"
        actionYes={logout}
      />
    </>
  );
}

export default ProfileMenu;
