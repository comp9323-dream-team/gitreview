import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';

export default function InputDialog(props) {
  const {
    caption, text, placeholder, open, setOpen, onSave, onCancel,
  } = props;

  const [inputValue, setInputValue] = React.useState('');

  React.useEffect(() => {
    setInputValue('');
  }, [open]);

  const handleChange = (e) => {
    setInputValue(e.target.value);
  };

  const handleConfirm = () => {
    if (onSave) {
      onSave(inputValue);
    }
    setOpen(false);
  };

  const handleDismiss = () => {
    if (onCancel) {
      onCancel();
    }
    setOpen(false);
  };

  return (
    <Dialog
      open={open}
      onClose={handleDismiss}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{caption}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {text}
        </DialogContentText>
        <TextField
          autoFocus
          margin="dense"
          label={placeholder}
          value={inputValue}
          onChange={handleChange}
          fullWidth
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleDismiss} color="primary">
          Cancel
        </Button>
        <Button onClick={handleConfirm} color="primary">
          OK
        </Button>
      </DialogActions>
    </Dialog>
  );
}

InputDialog.propTypes = {
  caption: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  open: PropTypes.bool.isRequired,
  setOpen: PropTypes.func.isRequired,
  onSave: PropTypes.func,
  onCancel: PropTypes.func,
};

InputDialog.defaultProps = {
  placeholder: '',
  onSave: null,
  onCancel: null,
};
