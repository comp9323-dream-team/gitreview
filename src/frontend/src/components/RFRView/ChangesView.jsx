/* eslint-disable */
import React from 'react';
import {
  Button, Typography, Grid, makeStyles,
  LinearProgress, Breadcrumbs, Paper,
} from '@material-ui/core';
import TwinFolderView from '../TwinFolderView';
import ConfirmDialog from '../ConfirmDialog';
import CodeViewer from '../CodeViewer';
import { newRequest, getErrorMessage } from '../../libs/utils';

const useStyles = makeStyles({
  folderViewCont: {
    maxHeight: '400px',
    overflowY: 'scroll',
  }
});

export default function ChangesView({ rfrObj }) {
  const classes = useStyles();
  const [loading, setLoading] = React.useState(false);
  const [fileLoading, setFileLoading] = React.useState(false);
  const [dirTree, setDirTree] = React.useState([]);
 
  const [browsePath, setBrowsePath] = React.useState([]);
  const [filePath, setFilePath] = React.useState(null);
  const [fileContents, setFileContents] = React.useState(null);

  const [errTitle, setErrTitle] = React.useState('');
  const [errMsg, setErrMsg] = React.useState('');
  const [errDialogOpen, setErrDialogOpen] = React.useState(false);

  React.useEffect(async () => {
    setLoading(true);
    setDirTree([]);
    try {
      const res = await newRequest(`rfr/${rfrObj.rfrid}/browse`);
      setDirTree(res.data);
    } catch (err) {
      setErrTitle('Error Retrieving Directory Tree');
      setErrMsg(getErrorMessage(err));
      setErrDialogOpen(true);
    } finally {
      setLoading(false);
    }
  }, [rfrObj]);

  React.useEffect(async () => {
    if (filePath && filePath.length) {
      setFileLoading(true);
      setFileContents(null);
      try {
        const res = await newRequest(`rfr/${rfrObj.rfrid}/item/${filePath.join('/')}`);
        const { data } = res;
        // compatibility w/ our code viewer
        if (data.right && !data.left) {
          data.left = data.right;
          data.right = null;
        }
        setFileContents(data);
      } catch (err) {
        setErrTitle('Error Retrieving File');
        setErrMsg(getErrorMessage(err));
        setErrDialogOpen(true);
      } finally {
        setFileLoading(false);
      }
    }
  }, [filePath]);

  return (
    <>
      <ConfirmDialog
        caption={errTitle}
        text={errMsg}
        setOpen={setErrDialogOpen}
        open={errDialogOpen}
        okOnly
      />
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <LinearProgress hidden={!loading} style={ {margin: '16px'} } fullWidth />
        </Grid>
        <Grid item xs={12}>
          <Typography variant="h5">Browse</Typography>
        </Grid>
        <Grid item xs={12}>
          <Breadcrumbs>
            {
              (browsePath || []).map((obj) => (
                <Typography>{obj}</Typography>
              ))
            }
          </Breadcrumbs>
        </Grid>
        <Grid item xs={12} className={classes.folderViewCont}>
          <TwinFolderView
            dirTree={dirTree}
            onPathChange={(v) => setBrowsePath(v)}
            onFileOpen={(paths, fileName) => setFilePath([...paths, fileName])}
          />
        </Grid>
        <Grid item xs={12}>
          <Typography variant="h5">View Differences</Typography>
        </Grid>
        <Grid item xs={12}>
          <Breadcrumbs>
            {
              (filePath || []).map((obj) => (
                <Typography>{obj}</Typography>
              ))
            }
          </Breadcrumbs>
        </Grid>
        <Grid item xs={12}>
          <LinearProgress hidden={!fileLoading} style={ {margin: '16px'} } fullWidth />
        </Grid>
        <Grid item xs={12}>
          <Paper>
            {
              fileContents
                ? (
                  <CodeViewer
                    left={fileContents.left}
                    right={fileContents.right}
                  />
                )
                : '(Click on a file to view its differences)'
            }
          </Paper>
        </Grid>
      </Grid>
    </>
  )
}