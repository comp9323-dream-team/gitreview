/* eslint-disable */
import React from 'react';
import {
  Button, Typography, Container, Grid, makeStyles, Paper, LinearProgress
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import ReactMarkdown from 'react-markdown';
import { newRequest, getErrorMessage } from '../../libs/utils';
import ReviewListTable from '../ReviewListTable';
import RFRItem from '../RFRItem';
import ReviewItem from '../ReviewItem';

const useStyles = makeStyles(() => ({
  mainCont: {
    padding: '16px',
  },
  card: {
    minHeight:'100px',
    padding:'20px',
    marginTop:'20px'
  },
  title: {
    marginTop:'20px'
  },
  reviewList: {
    display: 'flex',
    flexDirection: 'column',
  },
}));

function createData(context, message, reviewer, date) {
  return {
    context, message, reviewer, date,
  };
}

const reviewsTwo = [
  {
    "revid": 1632145062,
    "reviewer": "Wincent",
    "review": "This is a review: Plura mihi bona sunt, inclinet, amari petere vellent. Ab illo tempore, ab est sed immemorabili. Ullamco laboris nisi ut aliquid ex ea commodi consequat. Quae vero auctorem tractata ab fiducia dicuntur. At nos hinc posthac, sitientis piros Afros. Petierunt uti sibi concilium totius Galliae in diem certam indicere. Morbi fringilla convallis sapien, id pulvinar odio volutpat. A communi observantia non est recedendum.",
    "create_date": "2021-08-11",
    "status": "active",
    "published": false
  },
  {
    "revid": 1013171315,
    "reviewer": "James",
    "review": "komeng apdetan lagi!! Napa akeh errorne? Aku lara sirah nalika maca",
    "create_date": "2021-08-11",
    "status": "not active",
    "published": true
  }
];


const rfrTypeMap = {
  tc: 'Target Commit',
  ps: 'Patchset',
  bo: 'Base Commit Only',
  na: '(Unknown)',
  er: '(Error loading information)',
}

export default function Overview({ rfrObj }) {
  const classes = useStyles();

  const [rfrMore, setRfrMore] = React.useState({});

  // Added
  const [notify, setNotify] = React.useState({
    isOpen: false,
    message: '',
    type: '',
  });

  // Added 
  const [reviewLoading, setReviewLoading] = React.useState(false);
  const [reviews, setReviews] = React.useState([]);

  React.useEffect(async () => {
    setReviewLoading(true);
    try {
      const res = await newRequest(`review/byrfr/${rfrObj.rfrid}`);
      console.log(res.data);
      setReviews(res.data || []);
    } catch (err) {
      setNotify({
        isOpen: true,
        message: `Error getting your list of Reviews: ${getErrorMessage(err)}`,
        type: 'error',
      });
    } finally {
      setReviewLoading(false);
    }
  }, []);


  React.useEffect(async () => {
    setRfrMore({});
    if (rfrObj) {
      try {
        const res = await newRequest(`rfr/${rfrObj.rfrid}/details`);
        setRfrMore(res.data);
      } catch (err) {
        setRfrMore({
          type: 'er',
        })
      }
    }
  }, [rfrObj]);

  return (
    <Grid container className={classes.mainCont} spacing={1}>
      <Grid item xs={12}>
        <Typography variant="h5" className={classes.title}>Basic Information</Typography>
      </Grid>
      <Grid item xs={12}>
        <Paper className={classes.card} elevation={3}>
          <Grid container>
            <Grid item xs={12} className={classes.item}>
              <Typography variant="caption">Type</Typography>
              <Typography variant="subtitle1">
                {rfrTypeMap[rfrMore?.type || 'na']}
              </Typography>
            </Grid>
            <Grid item xs={12} className={classes.item}>
              <Typography variant="caption">Base Commit</Typography>
              <Typography variant="subtitle1">
                <code>{rfrMore?.basecommit || '(unknown)'}</code>
              </Typography>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
      <Grid item xs={12}>
        <Typography variant="h5" className={classes.title}>Description</Typography>
      </Grid>
      <Grid item xs={12}>
        <Paper className={classes.card} elevation={3}>
          <ReactMarkdown>
            {rfrObj.desc || '(No description provided)'}
          </ReactMarkdown>
        </Paper>
      </Grid>
      <Grid item xs={12}>
        <Typography variant="h5" className={classes.title}>Reviews</Typography>
      </Grid>
      <Grid item xs={12}>
        <div className={classes.reviewList}>
          <LinearProgress fullWidth style={{ width: '100%' }} hidden={!reviewLoading} />
          { (reviews.length)
            ? reviews.map((review) => (
              <ReviewItem reviewObj={review} />
            ))

            : (
              <Typography align="center" style={{ width: '100%' }}>{reviewLoading ? '(loading)' : 'No Reviews available'}</Typography>)}
        </div>
      </Grid>
    </Grid>
  )
}
