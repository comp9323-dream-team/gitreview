/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import { Grid, LinearProgress, makeStyles } from '@material-ui/core';
import { newRequest, getErrorMessage } from '../libs/utils';
import Notification from './UserCreation/Notification';

const useStyles = makeStyles(() => ({
  root: {
    margin: '16px',
    display: 'flex',
    flexDirection: 'column',
    '& > div': {
      width: '280px',
      margin: '16px 0',
    },
    '& > label': {
      fontWeight: '600',
    },
    // alignItems: 'center',
    // justifyContent: 'center',
  },
  button: {
    marginRight: '12px',
  },
}));

function EditProfile({ user, setEdit, setParamUsername }) {
  const [username, setUsername] = useState(user.username);
  const [description, setDescription] = useState(user.description);
  const [loading, setLoading] = useState(false);
  const [notify, setNotify] = useState({
    isOpen: false,
    message: '',
    type: '',
  });
  const classes = useStyles();
  const history = useHistory();

  const exitEdit = () => {
    setEdit(false);
  };

  const updateProfile = (e) => {
    e.preventDefault();
    const data = {
      username,
      description,
      profile_picture: user.profile_picture,
    };
    setLoading(true);
    newRequest('account/my', 'put', data)
      .then(() => {
        setNotify({
          isOpen: true,
          type: 'success',
          message: 'Profile updated successfully.',
        });
        exitEdit();
        setParamUsername(username);
      })
      .then(() => {
        history.replace(`/profile/${username.toLowerCase()}`);
      })
      .catch((err) => {
        setNotify({
          isOpen: true,
          type: 'error',
          message: `Error updating profile: ${getErrorMessage(err)}`,
        });
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <>
      <Notification notify={notify} setNotify={setNotify} />
      <form noValidate className={classes.root}>
        <InputLabel htmlFor="username">Username</InputLabel>
        <TextField
          variant="outlined"
          margin="normal"
          name="username"
          id="username"
          value={username}
          onChange={(event) => setUsername(event.target.value)}
        />
        <InputLabel htmlFor="description">Description</InputLabel>
        <TextField
          variant="outlined"
          margin="normal"
          name="description"
          id="description"
          multiline
          rows={4}
          value={description}
          onChange={(event) => setDescription(event.target.value)}
        />
        <Grid container spacing={2}>
          <Grid item xs={6}>
            <Button
              type="submit"
              variant="contained"
              color="primary"
              className={classes.button}
              onClick={updateProfile}
              fullWidth
              disabled={loading}
            >
              Save
            </Button>
          </Grid>
          <Grid item xs={6}>
            <Button
              type="submit"
              variant="contained"
              className={classes.button}
              onClick={exitEdit}
              fullWidth
              disabled={loading}
            >
              Cancel
            </Button>
          </Grid>
          <Grid item xs={12}>
            <LinearProgress fullWidth hidden={!loading} />
          </Grid>
        </Grid>
      </form>
    </>
  );
}

export default EditProfile;
