/* eslint-disable */

import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import FolderIcon from '@material-ui/icons/Folder';
import DescriptionIcon from '@material-ui/icons/Description';
import ReplyIcon from '@material-ui/icons/Reply';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import './TwinFolderView.css';

const useStyles = makeStyles({
  treeLink: {
    display: 'flex',
    alignItems: 'center',
  },
  treeItemIcon: {
    marginRight: '8px',
  },
});

export default function TwinFolderView({
  dirTree, onPathChange, onFileOpen, hidden,
}) {
  const classes = useStyles();
  const [parents, setParents] = React.useState([]);
  const [dirContent, setDirContent] = React.useState([]);

  React.useEffect(() => {
    setParents([]);
    setDirContent(dirTree);
  }, [dirTree]);

  React.useEffect(() => {
    // call event subscriber with an array of path name
    if (onPathChange) {
      onPathChange(parents.map((v) => v.name));
    }
  }, [parents]);

  const generateIcon = (kind) => {
    switch (kind) {
      case 'UP':
        return (<ArrowUpwardIcon className={classes.treeItemIcon} />);
      case 'TREE':
        return (<FolderIcon className={classes.treeItemIcon} />);
      case 'FILE':
        return (<DescriptionIcon className={classes.treeItemIcon} />);
      case 'LINK':
        return (<ReplyIcon className={classes.treeItemIcon} />);
      default:
        return (<HelpOutlineIcon className={classes.treeItemIcon} />);
    }
  };

  const handleOpenLink = (obj) => {
    if (obj.type === 'TREE') {
      const newParents = [...parents, {
        name: obj.name,
        dir: [...dirContent],
      }];
      setDirContent(obj.items || []);
      setParents(newParents);
    } else if (onFileOpen) {
      const paths = parents.map((v) => v.name);
      onFileOpen(paths, obj.name);
    }
  };

  const handleParentOpenLink = () => {
    if (parents.length) {
      const newParentList = [...parents];
      const parentDir = newParentList.pop();
      setParents(newParentList);
      setDirContent(parentDir.dir);
    }
  };

  const generateTreeItem = (treeItem) => {
    let leftVisible = true;
    let rightVisible = true;
    let color = null;

    switch (treeItem.change) {
      case 'M':
        color = 'darkorange';
        break;
      case '+':
        color = 'green';
        leftVisible = false;
        break;
      case '-':
        color = 'red';
        rightVisible = false;
        break;
      case 'U':
        color = 'darkblue';
        break;
    }

    let handleClick = () => handleOpenLink(treeItem);
    if (treeItem.type === 'UP') {
      handleClick = handleParentOpenLink;
    }

    return (
      <TableRow
        className="gr-tfv-tablerow"
        onClick={handleClick}
      >
        <TableCell
          className="gr-tfv-tablecell"
          style={{
            borderRight: '2px solid #ccc',
            color: color,
          }}
          component="th"
          scope="row"
        >
          <div
            className="fb"
            style={{
              visibility: leftVisible ? 'visible' : 'hidden',
            }}
          >
            {generateIcon(treeItem.type)}
            <Typography style={{color:color}} variant="subtitle1">{treeItem.name}</Typography>
          </div>
        </TableCell>
        <TableCell
          className="gr-tfv-tablecell"
          style={{
            borderLeft: '2px solid #ccc',
            color: color,
          }}
          component="th"
          scope="row"
        >
          <div
            className="fb"
            style={{
              visibility: rightVisible ? 'visible' : 'hidden',
            }}
          >
            {generateIcon(treeItem.type)}
            <Typography style={{color:color}} variant="subtitle1">{treeItem.name}</Typography>
          </div>
        </TableCell>
      </TableRow>
    )
  }

  return (
    <TableContainer component={Paper} hidden={hidden}>
      <Table>
        <TableBody>
          {
            parents.length
              ? generateTreeItem({
                  name: '(Up one level)',
                  change: 'U',
                  type: 'UP'
                })
              : null
          }
          {dirContent?.map(generateTreeItem)}
        </TableBody>
      </Table>

    </TableContainer>
  );
}