import { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { getAuthToken, newRequest } from '../libs/utils';

function useAuth() {
  // load default value from localStorage
  const [token, setToken] = useState(getAuthToken());
  const [user, setUser] = useState(false);
  const [loading, setLoading] = useState(true);
  const history = useHistory();

  useEffect(() => {
    if (token != null) {
      // someone is setting the token: update localStorage ASAP
      // important because the following request will use localStorage also
      localStorage.setItem('gitreview-token', token);
    }
    let isMounted = true;
    const checkToken = () => {
      if (!token) {
        setLoading(false);
        history.push('/login');
      } else {
        newRequest('auth/whoami')
          .then((response) => {
            if (isMounted) {
              console.log(`auth check: ${response}`);
              setUser(true);
            }
          })
          .catch((err) => {
            console.log(err);
          })
          .finally(() => {
            if (isMounted) {
              setLoading(false);
            }
          });
      }
    };
    checkToken();
    return () => {
      isMounted = false;
    };
  }, [token]);

  return {
    user, loading, token, setToken,
  };
}

export default useAuth;
