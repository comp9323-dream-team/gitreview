/* eslint-disable linebreak-style */
import React from 'react';
import { Snackbar, makeStyles } from '@material-ui/core';
import PropTypes from 'prop-types';
import { Alert } from '@material-ui/lab';

const useStyles = makeStyles(() => ({
  snackbar: {
    marginTop: '280px',
  },
  alert: {
    width: '551px',
  },
}));

function Notification(props) {
  const { notify, setNotify } = props;
  const classes = useStyles();

  const handleClose = () => {
    setNotify({
      ...notify,
      isOpen: false,
    });
  };
  return (
    <Snackbar
      open={notify.isOpen}
      anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      className={classes.snackbar}
    >
      <Alert className={classes.alert} severity={notify.type} onClose={handleClose}>
        {notify.message}
      </Alert>
    </Snackbar>
  );
}

Notification.propTypes = {
  notify: PropTypes.shape({
    isOpen: PropTypes.bool,
    type: PropTypes.string,
    message: PropTypes.string,
  }).isRequired,
  setNotify: PropTypes.func.isRequired,
};

export default Notification;
