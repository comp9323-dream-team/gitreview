import React from 'react';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  main: {
    position: 'absolute',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
}));

function FormContainer() {
  const classes = useStyles();
  return (
    <>
      <Container component="main" maxwidth="xs" className={classes.main}>
        <h2>HIIII</h2>
      </Container>
    </>
  )
};

export default FormContainer;