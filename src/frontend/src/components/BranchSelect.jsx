/* eslint-disable react/forbid-prop-types */
import React from 'react';
import {
  MenuItem, InputLabel, Select, FormControl, ListSubheader,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import ConfirmDialog from './ConfirmDialog';
import InputDialog from './InputDialog';
import { getErrorMessage, newRequest } from '../libs/utils';

const useStyles = makeStyles((theme) => ({
  formControl: {
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

export default function BranchSelect({
  repoId, forceUpdate, onChange, disabled,
}) {
  const classes = useStyles();
  const [branch, setBranch] = React.useState('');
  const [loading, setLoading] = React.useState(false);
  const [errMsg, setErrMsg] = React.useState('');
  const [openErrDialog, setOpenErrDialog] = React.useState(false);
  const [openManualInput, setOpenManualInput] = React.useState(false);
  const [optionItems, setOptionItems] = React.useState([]);

  const caption = loading ? '(loading)' : 'Heads';

  const handleChange = (event) => {
    const val = event.target.value;
    if (val === 'manual') {
      setOpenManualInput(true);
    } else if (val !== branch) {
      setBranch(event.target.value);
      if (onChange) {
        onChange(val);
      }
    }
  };

  const handleManualInputConfirm = (v) => {
    setBranch('manual');
    if (onChange) {
      onChange(v);
    }
  };

  const genMenuItem = (heads, refSpec, shortName = null) => (
    <MenuItem value={heads[refSpec]}>{shortName || refSpec}</MenuItem>
  );

  const manualOption = (
    <MenuItem value="manual"><em>Manual commit hash</em></MenuItem>
  );

  React.useEffect(async () => {
    setLoading(true);
    let allItems = [manualOption];
    try {
      const res = await newRequest(`repo/${repoId}/heads`);
      const { heads } = res.data;

      // filtered
      let headItem = null;
      const branches = [];
      const tags = [];
      const others = [];
      // eslint-disable-next-line no-restricted-syntax
      for (const refName of Object.keys(heads)) {
        // only show refspecs that we know
        if (refName === 'HEAD') {
          headItem = genMenuItem(heads, refName);
        } else if (refName.startsWith('refs/heads/')) {
          branches.push(genMenuItem(heads, refName, refName.substr(11)));
        } else if (refName.startsWith('refs/tags/')) {
          tags.push(genMenuItem(heads, refName, refName.substr(10)));
        } else {
          others.push(genMenuItem(heads, refName));
        }
      }

      if (headItem) {
        allItems.push(headItem);
      }
      if (branches.length) {
        allItems.push((<ListSubheader>Branches</ListSubheader>));
        allItems = allItems.concat(branches);
      }
      if (tags.length) {
        allItems.push((<ListSubheader>Tags</ListSubheader>));
        allItems = allItems.concat(tags);
      }
      if (others.length) {
        allItems.push((<ListSubheader>Other Refs</ListSubheader>));
        allItems = allItems.concat(others);
      }
      if (!allItems.length) {
        allItems.unshift((<MenuItem value=""><em>(No refs found)</em></MenuItem>));
      } else {
        allItems.unshift((<MenuItem value=""><em>(Select a head)</em></MenuItem>));
      }
    } catch (err) {
      setErrMsg(getErrorMessage(err));
      setOpenErrDialog(true);
    } finally {
      setLoading(false);
    }
    setOptionItems(allItems);
  }, [repoId, forceUpdate]);

  return (
    <>
      <ConfirmDialog
        caption="Error Fetching Heads"
        text={errMsg}
        setOpen={setOpenErrDialog}
        open={openErrDialog}
        okOnly
      />
      <InputDialog
        caption="Enter a Commit Hash"
        text="Enter the full string of the commit hash. If it is not available, try to fetch the repository."
        placeholder="Commit hash"
        setOpen={setOpenManualInput}
        open={openManualInput}
        onSave={handleManualInputConfirm}
      />
      <FormControl variant="outlined" className={classes.formControl}>
        <InputLabel id="branch-select-label">{caption}</InputLabel>
        <Select
          label={caption}
          value={branch}
          onChange={handleChange}
          disabled={loading || disabled}
        >
          {optionItems}
        </Select>
      </FormControl>
    </>
  );
}

BranchSelect.propTypes = {
  repoId: PropTypes.number.isRequired,
  forceUpdate: PropTypes.object,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
};

BranchSelect.defaultProps = {
  onChange: null,
  forceUpdate: null,
  disabled: false,
};
