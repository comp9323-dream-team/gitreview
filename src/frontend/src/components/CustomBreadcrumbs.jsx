/* eslint-disable linebreak-style */
import React from 'react';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() => ({
  title: {
    marginTop: '54px',
    marginBottom: '54px',
  },
  active: {
    fontWeight: '700',
  },
}));

// Template code from material-ui
function handleClick(event) {
  event.preventDefault();
  console.log('You clicked a breadcrumb.');
}

function CustomBreadcrumbs() {
  const classes = useStyles();
  return (
    <Breadcrumbs aria-label="breadcrumb" className={classes.title}>
      <Link color="inherit" href="/" onClick={handleClick}>
        johndoe
      </Link>
      {/* <Link color="inherit" href="/getting-started/installation/" onClick={handleClick}>
        sample
      </Link> */}
      <Typography color="textPrimary" className={classes.active}>sample</Typography>
    </Breadcrumbs>
  );
}
export default CustomBreadcrumbs;
