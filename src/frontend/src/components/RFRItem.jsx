/* eslint-disable react/prop-types */
import React from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { Paper, Typography, makeStyles } from '@material-ui/core';
import RequestIcon from './RequestIcon';

const requestTimeFromNow = (date) => {
  const requestTime = moment(date);
  return moment(requestTime).fromNow();
};

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    padding: '12px',
    margin: '8px 0',
  },
  topRow: {
    display: 'flex',
    marginBottom: '4px',
    alignItems: 'center',
  },
  desc: {
    fontSize: '18px',
    fontWeight: '600',
    textDecoration: 'none',
    paddingLeft: '4px',
    color: 'black',
    '&:hover': {
      color: '#3f51b5',
    },
  },
  link: {
    color: 'black',
    textDecoration: 'none',
    '&:hover': {
      color: '#3f51b5',
    },
  },
  bottomeRow: {
    display: 'flex',
    paddingLeft: '20px',
  },
}));
function RFRItem({ rfrObj }) {
  const classes = useStyles();
  return (
    <>
      <Paper className={classes.root} variant="outlined">
        <div className={classes.topRow}>
          <RequestIcon status={rfrObj.status} color={rfrObj.status === 'open' ? 'green' : 'red'} />
          <Typography variant="subtitle1" component={Link} to={`/rfr/${rfrObj.rfrid}/overview`} className={classes.desc}>
            {
              (rfrObj?.desc && rfrObj.desc.length) ? rfrObj.desc : '(No RFR description provided)'
            }
          </Typography>
        </div>
        <div className={classes.bottomeRow}>
          <Typography component="p" variant="subtitle2" color="textSecondary">
            requested
            {' '}
            {requestTimeFromNow(rfrObj.create_date)}
            { rfrObj.owner ? (
              <>
                {' '}
                by
                {' '}
                <Link className={classes.link} to={`/profile/${rfrObj.owner}`}>{rfrObj.owner}</Link>
              </>
            ) : null}
          </Typography>
        </div>
      </Paper>
    </>
  );
}

export default RFRItem;
