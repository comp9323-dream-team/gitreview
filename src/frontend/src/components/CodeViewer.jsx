/* eslint-disable */
import React from 'react';
import {
  IconButton, TextField
} from '@material-ui/core';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import CheckIcon from '@material-ui/icons/Check';
import './CodeViewer.css';
import { setRef } from '@material-ui/core';
import { generateParagraphs } from '../libs/reactUtils';

const styles = {
  table: {
    fontFamily: 'source-code-pro, Menlo, Monaco, Consolas, \'Courier New\', monospace',
    tableLayout: 'fixed',
    width: '100%',
  },
  lineno: {
    userSelect: 'none',
    textAlign: 'right',
    width: '1ch',
    overflow: 'none',
    background: '#eee',
    borderRight: '1px solid #ccc',
    color: '#777',
    verticalAlign: 'text-top',
  },
  content: {
    width: '1px',
    verticalAlign: 'text-top',
    overflowWrap: 'break-word',
  }
};

// resolves to CSS
const classNames = {
  editable: 'gr-cv-editable',
  contentDiv: 'gr-cv-contentdiv',
  commentBox: 'gr-cv-comment-box',
  commentBoxPerma: 'gr-cv-comment-perma',
  commentBoxParagraphs: 'gr-cv-comment-box-pars',
  commentCtrlBox: 'gr-cv-comment-ctrl-box',
  commentCtrlBtn: 'gr-cv-comment-ctrl-btn',
  fontNormal: 'gr-cv-font-normal',
  codeLine: 'gr-cv-codeline',
}

export default function CodeViewer({
  left, right,
  // comments must be map of (l/r)-lineno, values are pure string
  comments, editableComments, setEditableComments,
  editable, className,
  onCommentUpdate, onCommentDelete,
}) {

  const [editorActivePosition, setEditorActivePosition] = React.useState(null);
  const [commentsEditorValue, setCommentsEditorValue] = React.useState('');
  const [commentsEditorTouched, setCommentsEditorTouched] = React.useState(false);

  const getLinenoWidth = () => {
    const lc = left?.length || 1;
    const rc = right?.length || 1;
    const ret = Math.max(lc, rc, 1);
    return Math.trunc(Math.log10(ret)) + 1;
  }

  const handleRowClick = (pos) => {
    setCommentsEditorValue(editableComments[pos] || '');
    setCommentsEditorTouched(false);
    setEditorActivePosition(pos);
  }

  const handleEraseComment = () => {
    if (editableComments) {
      const newComments = {...editableComments};
      delete newComments[editorActivePosition];
      if (setEditableComments) {
        setEditableComments(newComments);
      }
    }
    if (onCommentDelete) {
      onCommentDelete(editorActivePosition);
    }
    setEditorActivePosition(null);
  }

  const handleSaveComment = () => {
    if (commentsEditorTouched) {
      const savedValue = commentsEditorValue.trim();
      if (savedValue.length) {
        if (editableComments) {
          const newComments = {
            ...editableComments,
            [editorActivePosition]: commentsEditorValue,
          };
          if (setEditableComments) {
            setEditableComments(newComments);
          }
        }
        if (onCommentUpdate) {
          onCommentUpdate(editorActivePosition, commentsEditorValue);
        }
      }
    }
    setEditorActivePosition(null);
  }

  const handleTextareaKeydown = (e) => {
    if (e.code === 'Escape') {
      setEditorActivePosition(null);
    } else if ((e.code === 'Enter') && (e.ctrlKey)) {
      handleSaveComment();
    }
  }

  const handleTextareaValueChange = (e) => {
    setCommentsEditorTouched(true);
    setCommentsEditorValue(e.target.value);
  }

  const editor = (
    <div className={classNames.commentBox}>
      <textarea
        autoFocus
        className={classNames.fontNormal}
        value={commentsEditorValue}
        onKeyDown={handleTextareaKeydown}
        onChange={handleTextareaValueChange}
      />
      <div className={classNames.commentCtrlBox}>
        <IconButton
          style={{ color: 'white' }}
          size="small"
          onClick={handleEraseComment}
        >
          <DeleteForeverIcon />
        </IconButton>
        <IconButton
          style={{ color: 'white' }}
          size="small"
          onClick={handleSaveComment}
        >
          <CheckIcon />
        </IconButton>
      </div>
    </div>
  );

  const getCommentBoxElement = (pos) => {
    if (pos === editorActivePosition) {
      return editor;
    } else if (pos in (comments || {})) {
      return (
        <div className={`${classNames.commentBox} ${classNames.commentBoxParagraphs} ${classNames.commentBoxPerma} ${classNames.fontNormal}`}>
          {generateParagraphs(comments[pos])}
        </div>
      );
    } else if (pos in (editableComments || {})) {
      return (
        <div className={`${classNames.commentBox} ${classNames.commentBoxParagraphs} ${classNames.fontNormal}`}>
          {generateParagraphs(editableComments[pos])}
        </div>
      );
    }
  }

  const tableRows = () => {
    // adjust style
    const linenoWidth = getLinenoWidth() + 1;
    const linenoStylePri = {
      ...styles.lineno,
      width: `${linenoWidth}ch`,
    }
    const linenoStyleNull = {
      ...linenoStylePri,
      background: '#ccc',
      borderRight: '1px solid #aaa',
    }
    const linenoStyleAdd = {
      ...linenoStylePri,
      background: 'rgb(204,255,216)',
      borderRight: '1px solid rgb(171,242,188)',
    }
    const linenoStyleDel = {
      ...linenoStylePri,
      background: 'rgb(255,220,224)',
      borderRight: '1px solid rgb(255,185,193)',
    }
    const maxContentWidth = (left && right) ? '50%' : '100%';
    const contentStylePri = {
      ...styles.content,
      width: `calc(${maxContentWidth} - ${linenoWidth}ch)`,
    }
    const contentStyleAdd = {
      ...contentStylePri,
      background: 'rgb(230,255,236)',
    }
    const contentStyleDel = {
      ...contentStylePri,
      background: 'rgb(255,238,240)',
    }
    const contentStyleNull = {
      ...contentStylePri,
      background: '#eee',
    }

    const generateColumns = (obj, posLetter, linenoObj) => {
      if (obj) {
        // line no used internally is 0 based
        const pos = `${posLetter}-${linenoObj.val}`;

        const adorn = obj[0] === 'N' ? (<span>&nbsp;</span>) : obj[0];
        let linenoStyle = linenoStylePri;
        let contentStyle = contentStylePri;
        switch (obj[0]) {
          case '+':
            linenoStyle = linenoStyleAdd;
            contentStyle = contentStyleAdd;
            break;
          case '-':
            linenoStyle = linenoStyleDel;
            contentStyle = contentStyleDel;
            break;
          default:
            break;
        }

        // can't edit if another row is in editing position
        const rowEditable = editable && !editorActivePosition && !(pos in (comments || {}));

        return [
          (<td style={linenoStyle}>{++linenoObj.val}{adorn}</td>),
          (
            <td
              style={contentStyle}
              className={rowEditable ? classNames.editable : ''}
              onClick={rowEditable ? () => handleRowClick(pos) : null}
            >
              <div className={classNames.contentDiv}>
                <pre className={classNames.codeLine}>{obj[1]}</pre>
                {getCommentBoxElement(pos)}
              </div>
            </td>
          ),
        ]
      } else {
        // these null lines are uncommentable and serves as indicator only
        return [
          (<td style={linenoStyleNull}>&nbsp;</td>),
          (<td style={contentStyleNull}>&nbsp;</td>),
        ]
      }
    }

    let linenoLeft = { val: 0 };
    let linenoRight = { val: 0 };

    // always use left as reference. either left only or left & right
    // left empty == no-op, even if right is filled
    if (left) {
      return left.map((obj, arrIdx) => {
        if (left && right) {
          // both L&R active
          return (
            <tr>
              {generateColumns(obj, 'l', linenoLeft)}
              {generateColumns(right[arrIdx], 'r', linenoRight)}
            </tr>
          )
        } else {
          // single file mode
          return (
            <tr>
              {generateColumns(obj, 'l', linenoLeft)}
            </tr>
          )
        }
      })
    }
    return null;
  }

  return (
    <table style={styles.table}>
      {tableRows()}
    </table>
  );
}