import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import useAuth from './useAuth';

// eslint-disable-next-line react/prop-types
function PrivateRoute({ children, ...rest }) {
  const { user, loading } = useAuth();

  return (
    (loading ? (<div>Loading...</div>) : (
      <Route
    // eslint-disable-next-line react/jsx-props-no-spreading
        {...rest}
        render={() => (user ? (
          children
        ) : (
          <Redirect to="/login" />
        ))}
      />
    )
    ));
}

export default PrivateRoute;
