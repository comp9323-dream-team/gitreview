import React from 'react';
import { CircularProgress, Grid, makeStyles } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import { useParams } from 'react-router-dom';
import Navbar from '../../components/Navbar';
import CustomTabs from '../../components/CustomTabs';
import { getErrorMessage, newRequest } from '../../libs/utils';
import RFRList from '../../components/RepoView/RFRList';
import SourceCodeView from '../../components/RepoView/SourceCodeView';

const useStyles = makeStyles(() => ({
  root: {
    // marginLeft: '148px',
  },
  title: {
    marginTop: '54px',
    marginBottom: '40px',
  },
  breadcrumb: {
    marginTop: '54px',
    marginBottom: '54px',
  },
  active: {
    fontWeight: '700',
  },
  branchRow: {
    display: 'flex',
    justifyContent: 'space-between',
    float: 'right',
    margin: '20px 0',
  },
  tag: {
    padding: '4px',
    marginTop: '32px',
    marginRight: '8px',
  },
  breadcrumbItemActive: {
    fontWeight: 'bold',
  },
  loadingContainer: {
    textAlign: 'center',
    margin: '1cm',
  },
  contentContainer: {
    padding: '1cm',
  },
}));

function RepoView() {
  const { repoid, mode } = useParams();
  const [mainLoading, setMainLoading] = React.useState(false);
  const [repo, setRepo] = React.useState(null);
  const [errMsg, setErrMsg] = React.useState('');

  React.useEffect(async () => {
    setMainLoading(true);
    try {
      const res = await newRequest(`repo/${repoid}`);
      setRepo(res.data);
    } catch (err) {
      setErrMsg(getErrorMessage(err));
    } finally {
      setMainLoading(false);
    }
  }, [repoid]);

  const classes = useStyles();

  const tab = mode === 'rfr' ? 0 : 1;

  const rfrViewContent = (
    <RFRList className={classes.contentContainer} repoId={repoid} />
  );

  const codeViewContent = (
    <SourceCodeView className={classes.contentContainer} repoId={repoid} />
  );

  const content = tab === 0 ? rfrViewContent : codeViewContent;

  const mainContent = (
    <Container className={classes.root} maxWidth="lg">
      <div className={classes.title}>
        <Typography variant="h3" component="h2" className={classes.title}>{repo?.name}</Typography>
        <Typography variant="body2" component="p">{repo?.description}</Typography>
        {
          repo?.tags
            ? repo.tags.map((tag) => <Chip size="small" label={tag.tag} color="primary" className={classes.tag} />)
            : <></>
        }
      </div>
      <CustomTabs repoId={repoid} tab={tab} />
      {content}
    </Container>
  );

  const mainLandingPage = (
    <Grid container>
      <Grid item xs={12} hidden={!mainLoading} className={classes.loadingContainer}>
        <CircularProgress />
      </Grid>
      <Grid item xs={12} hidden={mainLoading} className={classes.loadingContainer}>
        <Typography variant="h4">Error Loading Repository</Typography>
        <Typography variant="subtitle1">{errMsg}</Typography>
      </Grid>
    </Grid>
  );
  return (
    <>
      <Navbar />
      {!repo ? mainLandingPage : mainContent}
    </>
  );
}

export default RepoView;
