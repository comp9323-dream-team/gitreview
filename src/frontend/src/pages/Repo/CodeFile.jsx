/* eslint-disable linebreak-style */
import React from 'react';
import ReactMarkdown from 'react-markdown';
import { Light as SyntaxHighlighter } from 'react-syntax-highlighter';
import { useParams } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Container, makeStyles } from '@material-ui/core';
import Navbar from '../../components/Navbar';
import CustomBreadcrumbs from '../../components/CustomBreadcrumbs';
import BranchSelect from '../../components/BranchSelect';
// import PropTypes from 'prop-types';
import gitignore from '../../gitignore';
import input from '../../readme';

const useStyles = makeStyles(() => ({
  root: {
    // marginLeft: '148px',
  },
  branchRow: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: '20px 0',
  },
  refreshButton: {
    height: '50px',
  },
  filename: {
    marginTop: '60px',
  },
  readme: {
    padding: 'calc(1em + 1ex)',
    margin: 'calc(1em + 1ex) 0',
  },
  gitignore: {
    padding: 'calc(1em + 1ex) 0',
  },
}));

// const file1 = {
//   type: 'text',
//   data: gitignore,
// };

// const readme = {
//   type: 'markdown',
//   data: input,
// };

export default function CodeFile() {
  // const { type, data } = file
  const classes = useStyles();
  const { file } = useParams();
  return (
    <>
      <Navbar />
      <Container className={classes.root} maxWidth="lg">
        <CustomBreadcrumbs />
        <div className={classes.branchRow}>
          <BranchSelect />
          <Button variant="contained" color="primary" className={classes.refreshButton}>
            View Commit
          </Button>
        </div>

        <Typography component="h1" variant="h5" className={classes.filename}>{file}</Typography>
        <Paper evelation={3}>
          {(file === 'README.md')
            ? (
              <ReactMarkdown className={classes.readme}>
                {input}
              </ReactMarkdown>
            )
            : (
              <SyntaxHighlighter showLineNumbers className={classes.gitignore}>
                {gitignore}
              </SyntaxHighlighter>
            )}
        </Paper>
      </Container>
    </>
  );
}

// CodeFile.propTypes = {
//   file: PropTypes.shape({
//     type: PropTypes.string,
//     data: PropTypes.string,
//   }).isRequired,
// };
