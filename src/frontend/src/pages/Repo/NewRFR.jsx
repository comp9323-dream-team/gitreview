/* eslint-disable linebreak-style */
import React from 'react';
import { useParams, useHistory } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Radio from '@material-ui/core/Radio';
import Grid from '@material-ui/core/Grid';
import LinearProgress from '@material-ui/core/LinearProgress';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';

import Navbar from '../../components/Navbar';
import BranchSelect from '../../components/BranchSelect';
import FetchButton from '../../components/FetchButton';
import ConfirmDialog from '../../components/ConfirmDialog';
import Notification from '../../components/UserCreation/Notification';

import { newRequest, getErrorMessage, readFileBase64 } from '../../libs/utils';

const useStyles = makeStyles(() => ({
  root: {
    // marginLeft: '148px',
  },
  title: {
    marginTop: '54px',
    marginBottom: '20px',
  },
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '80%',
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
  },
  formLabel: {
    marginTop: '16px',
    marginBottom: '8px',
  },
  button: {
    marginTop: '16px',
  },
  info: {
    fontSize: '18px',
    marginTop: '40px',
    padding: '8px',
    '& ul': {
      paddingLeft: 0,
    },
    '& li': {
      margin: '10px 0 0px',
      fontWeight: '700',
      listStyleType: 'none',
    },
  },
  fetchButton: {
    width: '100%',
  },
}));

function NewRFR() {
  const classes = useStyles();
  const [kind, setKind] = React.useState('bo');
  const [desc, setDesc] = React.useState('');
  const [baseCommitHash, setBaseCommitHash] = React.useState('');
  const [targetCommitHash, setTargetCommitHash] = React.useState('');
  const [patchsetFile, setPatchsetFile] = React.useState(null);
  const [patchsetFileName, setPatchsetFileName] = React.useState('');
  const [loading, setLoading] = React.useState(false);
  const [isFetching, setIsFetching] = React.useState(false);
  const [forceUpdateHeads, setForceUpdateHeads] = React.useState(0);
  const [errMsg, setErrMsg] = React.useState('');
  const [openErrDialog, setOpenErrDialog] = React.useState(false);
  const [notify, setNotify] = React.useState({
    isOpen: false,
    message: '',
    type: '',
  });

  const { repoid } = useParams();
  const history = useHistory();

  const selectPatchset = async () => {
    // open file picker
    let fileHandle;
    try {
      [fileHandle] = await window.showOpenFilePicker({
        multiple: false,
        types: [
          {
            description: 'Git unidiff patchset',
          },
        ],
      });
    } catch {
      console.log('File selection cancelled.');
      return;
    }

    const data = await fileHandle.getFile();
    setPatchsetFile(data);
    setPatchsetFileName(fileHandle.name);
  };

  const handleNewRFR = async () => {
    setLoading(true);
    try {
      const reqObj = {
        branch: baseCommitHash,
        type: kind,
        desc,
        data: '',
      };
      if (kind === 'tc') {
        reqObj.data = targetCommitHash;
      } else if (kind === 'ps') {
        reqObj.data = await readFileBase64(patchsetFile);
      }

      const res = await newRequest(`rfr/byrepo/${repoid}`, 'post', reqObj);
      const newRfrId = res.data.rfrid;

      setNotify({
        isOpen: true,
        message: 'RFR created!',
        type: 'success',
      });

      setTimeout(() => history.push(`/rfr/${newRfrId}/overview`), 1000);
    } catch (err) {
      setErrMsg(getErrorMessage(err));
      setOpenErrDialog(true);
    } finally {
      setLoading(false);
    }
  };

  const getOptionContent = () => {
    switch (kind) {
      case 'bo':
        return null;
      case 'tc':
        return (
          <>
            <BranchSelect
              repoId={repoid}
              onChange={(v) => setTargetCommitHash(v)}
              disabled={isFetching || loading}
              forceUpdate={forceUpdateHeads}
            />
            <code><small>{targetCommitHash}</small></code>
          </>
        );
      case 'ps':
        // we're going to use the new filepicker API
        return (
          <>
            <Button
              variant="outlined"
              color="default"
              startIcon={<CloudUploadIcon />}
              className={classes.button}
              onClick={selectPatchset}
            >
              Upload Unidiff Patchset
            </Button>
            <small>{patchsetFileName}</small>
          </>
        );
      default:
        return 'Unknown option';
    }
  };

  // determine if the supplied extra info (in addition to base commit) are set
  const uploadOk = () => {
    let ret = true;
    switch (kind) {
      case 'tc':
        ret = !!targetCommitHash;
        break;
      case 'ps':
        ret = !!patchsetFile;
        break;
      default:
        break;
    }
    return ret && baseCommitHash;
  };

  React.useEffect(() => {
    switch (kind) {
      case 'tc':
        setTargetCommitHash('');
        break;
      case 'ps':
        setPatchsetFile(null);
        setPatchsetFileName('');
        break;
      default:
        break;
    }
  },
  [kind]);

  const handleChange = (event) => {
    setKind(event.target.value);
  };

  return (
    <>
      <Navbar />
      <ConfirmDialog
        caption="Error Submitting RFR"
        text={errMsg}
        setOpen={setOpenErrDialog}
        open={openErrDialog}
        okOnly
      />
      <Notification notify={notify} setNotify={setNotify} />
      <Container className={classes.root} maxWidth="lg">
        <Typography component="h1" variant="h5" className={classes.title}>Create new Request for Review (RFR)</Typography>
        <Grid container style={{ width: '100%' }} spacing={4}>
          <Grid item xs={6}>
            <form noValidate className={classes.form}>
              <FormControl component="fieldset">
                <FormLabel className={classes.formLabel}>Base Commit</FormLabel>
                <BranchSelect
                  repoId={repoid}
                  onChange={(v) => setBaseCommitHash(v)}
                  disabled={isFetching || loading}
                  forceUpdate={forceUpdateHeads}
                />
                <code><small>{baseCommitHash}</small></code>
              </FormControl>
              <FormControl component="fieldset">
                <FormLabel className={classes.formLabel}>Select Type</FormLabel>
                <RadioGroup value={kind} onChange={handleChange}>
                  <FormControlLabel value="bo" control={<Radio />} label="Base commit only" />
                  <FormControlLabel value="tc" control={<Radio />} label="Target commit" />
                  <FormControlLabel value="ps" control={<Radio />} label="Patchset" />
                </RadioGroup>
                {getOptionContent()}
              </FormControl>
              <FormControl>
                <FormLabel className={classes.formLabel}>RFR Description</FormLabel>
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  name="description"
                  type="text"
                  id="description"
                  multiline
                  rows={10}
                  onChange={(e) => setDesc(e.target.value)}
                  placeholder="Request for Review Description"
                />
              </FormControl>
              <Button
                variant="contained"
                color="primary"
                className={classes.button}
                disabled={!uploadOk() || loading}
                onClick={handleNewRFR}
              >
                save
              </Button>
              <LinearProgress fullWidth hidden={!loading} />
            </form>
          </Grid>
          <Grid item xs={6}>
            <Paper elevation={2} className={classes.info}>
              <Typography variant="subtitle2" component="p">
                Cannot find the branch or commit you are looking for?
                Click here to refresh from remote repository.
              </Typography>
              <FetchButton
                repoId={repoid}
                className={classes.fetchButton}
                setIsFetchingProgress={setIsFetching}
                onFetchSuccess={() => setForceUpdateHeads(forceUpdateHeads + 1)}
                disabled={loading}
              />
            </Paper>
            <Paper elevation={2} className={classes.info}>
              <ul>
                <li>Base commit</li>
                <Typography variant="subtitle2" component="p">
                  The base Git commit that the RFR will be applied to if the RFR is approved.
                </Typography>
                <li>Target commit</li>
                <Typography variant="subtitle2" component="p">
                  The target Git commit to be merged to the base commit if the RFR is approved.
                  Typically, only those who are authorised to perform changes to the Git
                  repository can create an RFR with this type.
                </Typography>
                <li>Patchset</li>
                <Typography variant="subtitle2" component="p">
                  A set of patches contained in a particular RFR.
                  Must be in Git&apos;s unidiff format.
                </Typography>
              </ul>
            </Paper>
          </Grid>
        </Grid>
      </Container>
    </>
  );
}

export default NewRFR;
