/* eslint-disable linebreak-style */
import { React } from 'react';
import ReactMarkdown from 'react-markdown';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { makeStyles, Typography } from '@material-ui/core';
import Navbar from '../../components/Navbar';
import CustomBreadcrumbs from '../../components/CustomBreadcrumbs';
import CustomTabs from '../../components/CustomTabs';
import RepoTreeTable from '../../components/RepoTreeTable';
import BranchSelect from '../../components/BranchSelect';
import input from '../../readme';

const useStyles = makeStyles(() => ({
  root: {
    // marginLeft: '148px',
  },
  title: {
    marginTop: '54px',
    marginBottom: '54px',
  },
  active: {
    fontWeight: '700',
  },
  branchRow: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: '20px 0',
  },
  refreshButton: {
    height: '50px',
  },
  readmeTitle: {
    marginTop: '82px',
  },
  readme: {
    padding: 'calc(1em + 1ex)',
    margin: 'calc(1em + 1ex)',
  },
}));

// const input = '**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**';
function SourceCode() {
  const classes = useStyles();
  const [isFetching, setIsFetching] = React.useState(false);
  const [timeOut, setTimeOut] = React.useState(false);
  // const [tab, setTab] = useState(0);

  // const changeTab = (newValue) => {
  //   setTabs(newValue);
  // };

  return (
    <>
      <Navbar />
      <Container className={classes.root} maxWidth="lg">
        {/* <Breadcrumbs aria-label="breadcrumb" className={classes.title}>
          <Link color="inherit" href="/code">
            John Doe
          </Link>
          <Typography color="textPrimary" className={classes.active}>Sample project</Typography>
        </Breadcrumbs> */}
        <CustomBreadcrumbs />
        <CustomTabs tab={1} />
        <div className={classes.branchRow}>
          <BranchSelect />
          <div>
            <Button variant="contained" color="primary" className={classes.refreshButton}>
              {isFetching ? 'Fetching...' : (timeOut ? `Fetch in ${timeOut}` : 'Fetch')}
            </Button>
            <LinearProgress />
          </div>
        </div>
        <RepoTreeTable />
        <Typography variant="h5" component="h1" className={classes.readmeTitle}>README.md</Typography>
        <Paper elevation={3}>
          <ReactMarkdown className={classes.readme}>{input}</ReactMarkdown>
        </Paper>
      </Container>
    </>
  );
}

export default SourceCode;
