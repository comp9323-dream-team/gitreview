/* eslint-disable react/no-children-prop */
/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-multi-str */
/* eslint-disable linebreak-style */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-await-in-loop */
import React from 'react';
import ReactMarkdown from 'react-markdown';
import { useHistory, useParams } from 'react-router-dom';
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { vs } from 'react-syntax-highlighter/dist/esm/styles/prism';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import {
  LinearProgress, makeStyles, CircularProgress, Link,
} from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Navbar from '../components/Navbar';
import Notification from '../components/UserCreation/Notification';
import { getErrorMessage, newRequest, handleAnchorNavigate } from '../libs/utils';
import { privateKeySteps } from '../libs/instructions';

const components = {
  code({
    node, inline, className, children, ...props
  }) {
    const match = /language-(\w+)/.exec(className || '');
    return !inline && match ? (
      <SyntaxHighlighter style={vs} language={match[1]} PreTag="div" children={String(children).replace(/\n$/, '')} {...props} />
    ) : (
      <code className={className} {...props}>
        {children}
      </code>
    );
  },
};

const useStyles = makeStyles(() => ({
  root: {
    // marginLeft: '148px',
  },
  title: {
    marginTop: '54px',
  },
  subTitle: {
    marginBottom: '54px',
  },
  centerText: {
    textAlign: 'center',
  },
}));

// at the moment, can only edit remote settings
function RepoSettings() {
  const classes = useStyles();
  const { repoid } = useParams();
  const history = useHistory();

  const [mainLoading, setMainLoading] = React.useState(true);
  const [loading, setLoading] = React.useState(false);
  const [formState, setFormState] = React.useState({});
  const [notify, setNotify] = React.useState({
    isOpen: false,
    message: '',
    type: '',
  });
  const [repoObj, setRepoObj] = React.useState(null);
  const [refreshCounter, setRefreshCounter] = React.useState(0);

  React.useEffect(async () => {
    try {
      const res = await newRequest('repo/byuser/my');
      // find matching ID
      for (const myrepo of res.data) {
        if (String(myrepo.repoid) === repoid) {
          // this is it! request remote settings
          const obj = {
            repoid: myrepo.repoid,
            url: myrepo.url,
            name: myrepo.name,
          };
          try {
            const remData = await newRequest(`repo/${repoid}/remote`);
            obj.existingKey = remData.data.key;
          } catch (err) {
            obj.existingKey = err.response.status === 404
              ? '(Not yet configured)'
              : `(Error retreiving key:${getErrorMessage(err)})`;
          }
          setRepoObj(obj);
          break;
        }
      }
    } catch (err) {
      setNotify({
        isOpen: true,
        message: `Error loading repository settings: ${getErrorMessage(err)}`,
        type: 'error',
      });
    } finally {
      setMainLoading(false);
    }
  }, [refreshCounter]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);

    try {
      await newRequest(`repo/${repoObj.repoid}/remote`, 'post', {
        key: formState.key,
        key_type: 'OPENSSH_PRIVATE_KEY_V1',
      });
      setNotify({
        isOpen: true,
        message: 'Key settings saved!',
        type: 'success',
      });
      setRefreshCounter(refreshCounter + 1);
    } catch (err) {
      setNotify({
        isOpen: true,
        message: `Error saving key: ${getErrorMessage(err)}`,
        type: 'error',
      });
    } finally {
      setLoading(false);
    }
  };

  const handleFieldChangeGeneric = (fieldName, value) => {
    setFormState({
      ...formState,
      [fieldName]: value,
    });
  };

  const handleTextfieldChange = (e) => {
    handleFieldChangeGeneric(e.target.name, e.target.value);
  };

  const renderBasedOnState = () => {
    if (mainLoading) {
      return (
        <Grid container alignItems="center" justify="center" style={{ padding: 20 }}>
          <CircularProgress />
        </Grid>
      );
    }
    if (repoObj) {
      return (
        <Container component="main" maxWidth="lg" className={classes.root}>
          <Typography gutterBottom align="left" variant="h4" component="h1" className={classes.title}>Edit Repository Settings</Typography>
          <Typography variant="subtitle1" className={classes.subTitle}>You can use this page to configure the remote settings of your repository.</Typography>
          <Grid container spacing={5}>
            <Grid item xs={6}>
              <form noValidate onSubmit={handleSubmit}>
                <InputLabel htmlFor="url">Repository URL</InputLabel>
                <TextField
                  variant="outlined"
                  margin="normal"
                  fullWidth
                  type="text"
                  InputProps={{
                    readOnly: true,
                  }}
                  value={repoObj.url || '(Unknown)'}
                />
                <InputLabel htmlFor="url">Repository Name</InputLabel>
                <TextField
                  variant="outlined"
                  margin="normal"
                  fullWidth
                  type="text"
                  InputProps={{
                    readOnly: true,
                  }}
                  value={repoObj.name || '(Unknown)'}
                />
                <InputLabel htmlFor="url">Existing Private Key Signature</InputLabel>
                <TextField
                  variant="outlined"
                  margin="normal"
                  fullWidth
                  type="text"
                  InputProps={{
                    readOnly: true,
                  }}
                  value={repoObj.existingKey || '(Unknown)'}
                />
                <InputLabel htmlFor="key">New Private Key</InputLabel>
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  name="key"
                  type="text"
                  id="key"
                  multiline
                  rows={10}
                  onChange={handleTextfieldChange}
                  placeholder="Begins with '---Begin OPENSSH PRIVATE KEY---'"
                />
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  disabled={loading || !formState.key}
                  fullWidth
                >
                  Save
                </Button>
                <LinearProgress fullWidth hidden={!loading} />
              </form>
            </Grid>
            <Grid item xs={6}>
              <div>
                <Typography align="center" component="h2" variant="h5" gutterBottom> Adding SSH key to GitReview </Typography>
                {/* eslint-disable-next-line max-len */}
                <ReactMarkdown components={components}>{privateKeySteps}</ReactMarkdown>
              </div>
            </Grid>
          </Grid>
        </Container>
      );
    }
    // not loading and no repoObj == failed
    return (
      <Grid container spacing={2} style={{ padding: 20 }}>
        <Grid xs={12} className={classes.centerText} item>
          <Typography variant="h2">Cannot Open Repository Settings</Typography>
        </Grid>
        <Grid xs={12} className={classes.centerText} item>
          <Typography variant="h5">You might not have permission to setup this repository.</Typography>
        </Grid>
        <Grid xs={12} className={classes.centerText} item>
          <Link href="/dashboard" onClick={handleAnchorNavigate(history)}>Click here to go back to your dashboard.</Link>
        </Grid>
      </Grid>
    );
  };

  return (
    <>
      <Navbar />
      <Notification notify={notify} setNotify={setNotify} />
      {renderBasedOnState()}
    </>
  );
}

export default RepoSettings;
