/* eslint-disable linebreak-style */
import { React, useState } from 'react';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';
import LinearProgress from '@material-ui/core/LinearProgress';
import Typography from '@material-ui/core/Typography';
import { useHistory, useParams } from 'react-router-dom';
import { ReactComponent as ReactLogo } from '../assets/logo.svg';
import Notification from '../components/UserCreation/Notification';
import { handleAnchorNavigate, getErrorMessage, newRequest } from '../libs/utils';

const useStyles = makeStyles((theme) => ({
  main: {
    position: 'absolute',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    marginBottom: theme.spacing(6),
  },
  loginContainer: {
    boxShadow: 'rgb(0 0 0 / 10%) 0px 0px 10px',
    padding: '32px 48px',
    borderRadius: '5px',
    width: '483px',
  },

  button: {
    marginTop: '24px',
  },
  forgetPasswordLink: {
    textAlign: 'center',
    marginTop: '16px',
  },
  registerContainer: {
    paddingTop: '32px',
    marginTop: '16px',
    borderTop: '1px solid rgb(213, 216, 222)',
    textAlign: 'center',
  },
}));

function ResetPasswordVerify() {
  const classes = useStyles();
  const history = useHistory();
  const { token } = useParams();
  const [loading, setLoading] = useState(false);
  const [newpassword, setNewpassword] = useState({
    password: '',
    confirmPassword: '',
  });
  const [notify, setNotify] = useState({
    isOpen: false,
    message: '',
    type: '',
  });

  const verifyToken = async () => {
    try {
      console.log(token);
      const res = await newRequest(`account/iforgot/verify/${token}`);
      console.log(res);
      return res.data.token;
    } catch (err) {
      setNotify({
        isOpen: true,
        message: getErrorMessage(err),
        type: 'error',
      });
      return false;
    }
  };

  const resetPassword = (message) => {
    setNotify({
      isOpen: true,
      message,
      type: 'error',
    });
    setNewpassword({
      password: '',
      confirmPassword: '',
    });
  };

  const validateInput = (pass1, pass2) => {
    if (pass1 === '' || pass2 === '') {
      resetPassword('Password should not be empty');
      return false;
    }
    if (pass1 !== pass2) {
      resetPassword("Password confirmation doesn't match the password ");
      return false;
    }
    return true;
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    const { password, confirmPassword } = newpassword;
    if (validateInput(password, confirmPassword)) {
      setLoading(true);
      try {
        const newToken = await verifyToken();
        if (newToken) {
          await newRequest('account/iforgot/resetpw', 'post', {
            token: newToken,
            password,
          })
            .then(() => {
              setNotify({
                isOpen: true,
                message: 'Password change success!',
                type: 'success',
              });
              setTimeout(() => {
                history.push('/login');
              }, 2000);
            })
            .catch((err) => {
              setNotify({
                isOpen: true,
                message: getErrorMessage(err),
                type: 'error',
              });
            });
        }
      } finally {
        setLoading(false);
      }
    }
  };

  return (
    <>
      <Notification notify={notify} setNotify={setNotify} />
      <Container component="main" maxwidth="xs" className={classes.main}>
        <ReactLogo className={classes.logo} />
        <form noValidate className={classes.loginContainer}>
          <Typography gutterBottom align="center" variant="h6" component="h1">Change you password</Typography>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            value={newpassword.password}
            onChange={(e) => setNewpassword({
              ...newpassword,
              password: e.target.value,
            })}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="confirmPassword"
            label="Confirm password"
            type="password"
            id="confirmPassword"
            value={newpassword.confirmPassword}
            onChange={(e) => setNewpassword({
              ...newpassword,
              confirmPassword: e.target.value,
            })}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={handleSubmit}
            disabled={loading}
          >
            Change password
          </Button>
          <LinearProgress fullWidth hidden={!loading} />
          <div className={classes.registerContainer}>
            <Typography>
              <Link href="/login" onClick={handleAnchorNavigate(history)}>
                Return to log in
              </Link>
            </Typography>
          </div>
        </form>
      </Container>
    </>
  );
}

export default ResetPasswordVerify;
