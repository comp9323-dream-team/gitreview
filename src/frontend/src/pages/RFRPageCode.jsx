/* eslint-disable */
import React from 'react';
import {
  Container, makeStyles, Grid, Typography, Paper,
} from '@material-ui/core';
import { TreeView, TreeItem } from '@material-ui/lab/';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import DescriptionIcon from '@material-ui/icons/Description';
// import ReactMarkdown from 'react-markdown';
// import SyntaxHighlighter from 'react-syntax-highlighter';
// import { docco } from 'react-syntax-highlighter/dist/cjs/styles/hljs';
// import { useParams } from 'react-router-dom';
import CustomBreadcrumbs from '../components/CustomBreadcrumbs';
import Navbar from '../components/Navbar';
import CustomTabsRFR from '../components/CustomTabsRFR';
import BranchSelect from '../components/BranchSelect';
import pythonold from '../pythonold';
import pythonnew from '../pythonnew';
import 'react-diff-view/style/index.css';
import CustomDiff from '../components/CommentReview/CustomDiff';

const useStyles = makeStyles(() => ({
  root: {
    paddingBottom: '100px',
  },
  branchRow: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: '20px 0',
  },
  treeview: {
    marginTop: '10px',
    marginLeft: '50px',
    height: 120,
    flexGrow: 1,
    maxWidth: 400,
  },
  title: {
    marginBottom: '30px',
  },
  flexcolscroll: {
    flexGrow: 1,
    overflow: 'auto',
    minHeight: 300,
    marginLeft: 50,
    marginRight: 50,
    marginBottom: 100,
  },
  readme: {
    padding: 'calc(1em + 1ex)',
    margin: 'calc(1em + 1ex)',
  },
}));
/**
const before = [
  ['', 'print("hello world!")'],
  null,
  ['', '// This is a comment'],
  ['-', '// This should not be here'],
  ['-', '// This will be modified'],
];

const after = [
  ['', 'print("hello world!")'],
  ['+', '// This was not here before!'],
  ['', '// This is a comment'],
  null,
  ['+', '// This has been modified!'],
];
 */

function RFRPage() {
  const classes = useStyles();
  const nodeSelectHandler = () => {
    console.log('A file is clicked');
  };
  // const { rfr } = useParams();
  // const codeString = 'print("Hello World!")';
  return (
    <div className={classes.root}>
      <Navbar />
      <Container className={classes.root} maxWidth="lg">
        <CustomBreadcrumbs />
        <CustomTabsRFR tab={1} />
        <div className={classes.branchRow}>
          {/** TODO: This should be a Commit Select, not Branchj */}
          <BranchSelect />
        </div>
        <Grid container spacing={10}>
          <Grid item sm={12} md={6}>
            <Typography variant="h5" component="h1" className={classes.title}>Old</Typography>
            <Paper elevation={3} className={classes.flexcolscroll}>
              <TreeView
                className={classes.treeview}
                defaultCollapseIcon={<ExpandMoreIcon />}
                defaultExpandIcon={<ChevronRightIcon />}
                defaultEndIcon={<DescriptionIcon />}
                onNodeSelect={nodeSelectHandler}
                // selected="2"
              >
                <TreeItem nodeId="1" label="Applications">
                  <TreeItem nodeId="2" label="Calendar" />
                  <TreeItem nodeId="3" label="Chrome" />
                  <TreeItem nodeId="4" label="Webstorm" />
                </TreeItem>
                <TreeItem nodeId="5" label="Documents">
                  <TreeItem nodeId="10" label="OSS" />
                  <TreeItem nodeId="6" label="Material-UI">
                    <TreeItem nodeId="7" label="src">
                      <TreeItem nodeId="8" label="index.js" />
                      <TreeItem nodeId="9" label="tree-view.js" />
                      <TreeItem nodeId="10" label="index.js" />
                      <TreeItem nodeId="11" label="tree-view.js" />
                      <TreeItem nodeId="12" label="index.js" />
                      <TreeItem nodeId="13" label="tree-view.js" />
                    </TreeItem>
                  </TreeItem>
                  <TreeItem nodeId="14" label="Gian-File">
                    <TreeItem nodeId="15" label="Wincent.js" />
                    <TreeItem nodeId="16" label="Front-End.js" />
                    <TreeItem nodeId="17" label="GitReview.js" />
                  </TreeItem>
                </TreeItem>
              </TreeView>
            </Paper>
          </Grid>
          <Grid item sm={12} md={6}>
            <Typography variant="h5" component="h1" className={classes.title}>New</Typography>
            <Paper elevation={3} className={classes.flexcolscroll}>
              <TreeView
                className={classes.treeview}
                defaultCollapseIcon={<ExpandMoreIcon />}
                defaultExpandIcon={<ChevronRightIcon />}
                defaultEndIcon={<DescriptionIcon />}
              >
                <TreeItem nodeId="1" label="Applications">
                  <TreeItem nodeId="2" label="Calendar" />
                  <TreeItem nodeId="3" label="Chrome" />
                  <TreeItem nodeId="4" label="Webstorm" />
                </TreeItem>
                <TreeItem nodeId="5" label="Documents">
                  <TreeItem nodeId="10" label="OSS" />
                  <TreeItem nodeId="6" label="Material-UI">
                    <TreeItem nodeId="7" label="src">
                      <TreeItem nodeId="8" label="index.js" />
                      <TreeItem nodeId="9" label="tree-view.js" />
                      <TreeItem nodeId="10" label="index.js" />
                      <TreeItem nodeId="11" label="tree-view.js" />
                      <TreeItem nodeId="12" label="index.js" />
                      <TreeItem nodeId="13" label="tree-view.js" />
                    </TreeItem>
                  </TreeItem>
                  <TreeItem nodeId="14" label="Gian-File">
                    <TreeItem nodeId="15" label="Wincent.js" />
                    <TreeItem nodeId="16" label="Front-End.js" />
                    <TreeItem nodeId="17" label="GitReview.js" />
                  </TreeItem>
                </TreeItem>
              </TreeView>
            </Paper>
          </Grid>
        </Grid>
        <Typography variant="h5" component="h1" className={classes.title}>Code</Typography>
        
      </Container>
    </div>
  );
}

export default RFRPage;

/*
const OLD_REMOVED = [3, 5];
const NEW_ADDED = [2, 4];

        <Grid container spacing={5}>
          <Grid item sm={12} md={6}>
            <Paper elevation={3}>
              <SyntaxHighlighter
                language="python"
                style={docco}
                className={classes.code}
                showLineNumbers="True"
                wrapLines="True"
                // wrapLongLines="True"
                lineProps={(lineNumber) => {
                  const style = { display: 'block' };
                  if (OLD_REMOVED.includes(lineNumber)) {
                    style.backgroundColor = '#ffecec';
                  }
                  return { style };
                }}
              >
                {pythonold}
              </SyntaxHighlighter>
            </Paper>
          </Grid>
          <Grid item sm={12} md={6}>
            <Paper elevation={3}>
              <SyntaxHighlighter
                language="python"
                style={docco}
                className={classes.code}
                showLineNumbers="True"
                wrapLines="True"
                // wrapLongLines="True"
                lineProps={(lineNumber) => {
                  const style = { display: 'block' };
                  if (NEW_ADDED.includes(lineNumber)) {
                    style.backgroundColor = '#dbffdb';
                  }
                  return { style };
                }}
              >
                {pythonnew}
              </SyntaxHighlighter>
            </Paper>
          </Grid>
        </Grid>
*/
