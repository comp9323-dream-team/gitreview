/* eslint-disable linebreak-style */
import { React, useState } from 'react';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import LinearProgress from '@material-ui/core/LinearProgress';
import { useHistory } from 'react-router-dom';
import logo from '../assets/gitreview_logo.png';
import Notification from '../components/UserCreation/Notification';
import { handleAnchorNavigate, getErrorMessage, newRequest } from '../libs/utils';

const useStyles = makeStyles((theme) => ({
  main: {
    position: 'absolute',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  alert: {
    width: '50%',
    marginBottom: '20px',
  },
  logo: {
    marginBottom: theme.spacing(6),
  },
  registerContainer: {
    boxShadow: 'rgb(0 0 0 / 10%) 0px 0px 10px',
    padding: '32px 48px',
    borderRadius: '5px',
    width: '483px',
  },

  button: {
    marginTop: '24px',
  },
  forgetPasswordLink: {
    textAlign: 'center',
    marginTop: '16px',
  },
  loginContainer: {
    paddingTop: '32px',
    marginTop: '16px',
    borderTop: '1px solid rgb(213, 216, 222)',
    textAlign: 'center',
  },
  loading: {
    marginTop: '100px',
  },
}));

function Register() {
  const classes = useStyles();
  const history = useHistory();
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [loading, setLoading] = useState(false);

  const [notify, setNotify] = useState({
    isOpen: false,
    message: '',
    type: '',
  });
  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);
    newRequest('account/register', 'post', {
      email,
      username,
    })
      .then(() => {
        setNotify({
          isOpen: true,
          message: 'Register success, please check your email!',
          type: 'success',
        });
        setTimeout(() => {
          history.push('/login');
        }, 2000);
      })
      .catch((err) => {
        setNotify({
          isOpen: true,
          message: getErrorMessage(err),
          type: 'error',
        });
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <>
      <Notification notify={notify} setNotify={setNotify} />
      <Container component="main" maxwidth="xs" className={classes.main}>
        <img src={logo} alt="GitReview Logo" width="410" height="58" className={classes.logo} />
        <form noValidate className={classes.registerContainer}>
          <Typography gutterBottom align="center" variant="h6" component="h1">Register New GitReview Account</Typography>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            value={email}
            type="email"
            autoFocus
            onChange={(e) => setEmail(e.target.value)}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="username"
            label="Username"
            name="username"
            autoComplete="username"
            value={username}
            type="username"
            autoFocus
            onChange={(e) => setUsername(e.target.value)}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={handleSubmit}
            disabled={loading}
          >
            Register
          </Button>
          <LinearProgress fullWidth hidden={!loading} />
          <div className={classes.loginContainer}>
            <Typography>
              <Link href="/login" onClick={handleAnchorNavigate(history)}>
                Have an account? Login instead!
              </Link>
            </Typography>
          </div>
        </form>
      </Container>
    </>
  );
}

export default Register;
