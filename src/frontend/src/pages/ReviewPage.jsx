/* eslint-disable */
import React from 'react';
import {
  Container, makeStyles, Typography, Button, Paper,
  CircularProgress, Grid, Tabs, Tab, TextField, Breadcrumbs,
  LinearProgress, Accordion, AccordionSummary, AccordionDetails,
  Table, TableBody, TableCell, TableContainer, TableHead, TableRow,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import DescriptionIcon from '@material-ui/icons/Description';
import { Link, useParams, useHistory } from 'react-router-dom';
import Navbar from '../components/Navbar';
import TwinFolderView from '../components/TwinFolderView';
import CodeViewer from '../components/CodeViewer';
import ConfirmDialog from '../components/ConfirmDialog';
import Notification from '../components/UserCreation/Notification';
import { newRequest, getErrorMessage } from '../libs/utils';
import { generateParagraphs } from '../libs/reactUtils';

const useStyles = makeStyles(() => ({
  root: {},
  commentOverviewPar: {
    margin: 0
  },
  commentOverviewTableCell: {
    padding: 0,
    '& > p': {
      margin: 0,
    },
  },
  folderViewCont: {
    maxHeight: '400px',
    overflowY: 'scroll',
  },
}));

export default function ReviewPage() {
  const { rfrid, revid } = useParams();
  const classes = useStyles();
  const history = useHistory();

  // not to be confused with rfrid
  const [rfrId, setRfrId] = React.useState(null);

  const [viewMode, setViewMode] = React.useState(0);
  const [loading, setLoading] = React.useState(0);
  const [fileLoading, setFileLoading] = React.useState(0);

  const [errTitle, setErrTitle] = React.useState('');
  const [errMsg, setErrMsg] = React.useState('');
  const [openErrDialog, setOpenErrDialog] = React.useState(false);

  const [confirmTitle, setConfirmTitle] = React.useState('');
  const [confirmMessage, setConfirmMessage] = React.useState('');
  const [confirmOpen, setConfirmOpen] = React.useState(false);
  const [confirmAction, setConfirmAction] = React.useState(null);

  const [notify, setNotify] = React.useState({
    isOpen: false,
    message: '',
    type: '',
  });

  const [editMode, setEditMode] = React.useState(false);
  const [published, setPublished] = React.useState(false);
  const [allowPublish, setAllowPublish] = React.useState(false);

  const [dirTree, setDirTree] = React.useState([]);
  const [fileContents, setFileContents] = React.useState(null);

  const [browsePath, setBrowsePath] = React.useState([]);
  const [filePath, setFilePath] = React.useState([]);

  // inline review variables
  // comments retreived from server. these are not editable.
  const [permaComments, setPermaComments] = React.useState({});
  // comments created by user during this session. they're editable and deletable.
  const [newComments, setNewComments] = React.useState({});
  // combination of both perma and new comments. used to show in "all comments" tab.
  const [allComments, setAllComments] = React.useState({});
  // permanent comments to show on CodeViewer
  const [displayPermaComments, setDisplayPermaComments] = React.useState({});
  // new comments to show on CodeViewer
  const [displayNewComments, setDisplayNewComments] = React.useState({});

  const [overallReview, setOverallReview] = React.useState('');

  const showErrorDialog = (title, msg) => {
    setOpenErrDialog(true);
    setErrTitle(title);
    setErrMsg(msg);
  }

  const showConfirmDialog = (title, msg, action) => {
    setConfirmOpen(true);
    setConfirmTitle(title);
    setConfirmMessage(msg);
    setConfirmAction(() => action);
  }

  const clientReviewToServerReview = () => {
    // new comments only
    return Object.keys(newComments).map((filename) => {
      const comm = {
        filename,
        old: [],
        new: [],
      };
      const commentData = newComments[filename];
      for (const lineName of Object.keys(commentData)) {
        const lineNo = parseInt(lineName.substr(2));
        switch (lineName[0]) {
          case 'r':
          case 'R':
            comm.new.push({
              line: lineNo,
              content: commentData[lineName],
            });
            break;
          case 'l':
          case 'L':
            comm.old.push({
              line: lineNo,
              content: commentData[lineName],
            });
            break;
          default:
            break;
        }
      }
      return comm;
    });
  }

  const doSave = async () => {
    setLoading(true);
    let res;
    let newRevId = null;

    const body = {
      overall_review: overallReview,
      inline_reviews: clientReviewToServerReview(),
    };

    try {
      if (revid) {
        // update existing
        res = await newRequest(`review/${revid}`, 'put', body);
      } else {
        // create new
        res = await newRequest(`review/byrfr/${rfrid}`, 'post', body);
        newRevId = res.data.revid;
      }
      setNotify({
        isOpen: true,
        message: 'Review saved.',
        type: 'success',
      });
      if (newRevId) {
        setTimeout(() => {
          history.push(`/review/view/${newRevId}`);
        }, 1500);
      } else {
        await updateCurrentView();
      }
    } catch (err) {
      showErrorDialog("Error Saving Review", getErrorMessage(err));
    } finally {
      setLoading(false);
    }
  }

  const doPublish = () => {
    setLoading(true);
    newRequest(`review/publish/${revid}`, 'put')
      .then(() => {
        setAllowPublish(false);
        setNotify({
          isOpen: true,
          message: 'Review published.',
          type: 'success',
        })
      })
      .catch((err) => {
        showErrorDialog("Error Publishing Review", getErrorMessage(err));
      })
      .finally(() => {
        setLoading(false);
      });
  }

  const askSaveReview = () => {
    showConfirmDialog(
      "Save Review?",
      "You won't be able to edit or delete your inline reviews after they are saved.",
      doSave
    );
  };

  const askPublishReview = () => {
    showConfirmDialog(
      "Publish Review?",
      "Others will be able to see your review once it is published.",
      doPublish
    );
  };

  const handleCommentUpdate = (pos, content) => {
    const newAllComments = {...allComments};
    const key = filePath.join('/');
    if (!(key in newAllComments)) {
      newAllComments[key] = {};
    }
    newAllComments[key][pos] = content;
    setAllComments(newAllComments);
  }

  const handleCommentDelete = (pos) => {
    const newAllComments = {...allComments};
    const key = filePath.join('/');
    if (key in newAllComments) {
      delete newAllComments[key][pos];
    }
    setAllComments(newAllComments);
  }

  const handleViewFile = (e, filename) => {
    e.stopPropagation();
    setFilePath(filename.split('/'));
    setViewMode(0);
  }

  const serverReviewToClientReview = (data) => {
    const ret = {};
    for (const obj of (data?.inline_reviews || [])) {
      const subRet = {};
      for (const [prefix, lineArr] of [['l-', obj.old], ['r-', obj.new]]) {
        for (const commentObj of lineArr) {
          subRet[`${prefix}${commentObj.line}`] = commentObj.content;
        }
      }
      ret[obj.filename] = subRet;
    }
    return ret;
  }

  const updateCurrentView = async () => {
    setLoading(true);
    setOverallReview('');
    setBrowsePath([]);
    setFilePath([]);
    setFileContents(null);

    setPermaComments({});
    setNewComments({});
    setAllComments({});
    setDisplayPermaComments({});
    setDisplayNewComments({});

    let res;
    try {
      // get current user info for ownership purposes
      res = await newRequest('account/my');
      const myUsername = res.data.username;

      // for fetching directory list
      let usedRfrId = null;

      if (rfrid) {
        // create new review from RFR
        usedRfrId = rfrid;
        setRfrId(rfrid);
        res = await newRequest(`rfr/${rfrid}`);
        if (res.data?.owner === myUsername) {
          showErrorDialog('Cannot Create Review', 'Cannot review your own RFR.');
        } else {
          // possible!
          setEditMode(true);
        }
      } else {
        // view existing review
        // first, get review info
        res = await newRequest(`review/${revid}`);
        const { data } = res;
        setOverallReview(data?.overall_review || '');
        let canEdit = (data?.status === 'active')
          && (myUsername === data?.owner);
        setEditMode(canEdit);
        setPublished(data.published);
        setAllowPublish(!data.published);
        setRfrId(data.rfrid);
        usedRfrId = data.rfrid;

        const inlineReviews = serverReviewToClientReview(data?.inline_reviews);
        setPermaComments(inlineReviews);
        setAllComments(JSON.parse(JSON.stringify(inlineReviews)));
      }

      res = await newRequest(`rfr/${usedRfrId}/browse`);
      setDirTree(res.data);
    } catch (err) {
      showErrorDialog("Error Retrieving Review Information", getErrorMessage(err));
    } finally {
      setLoading(false);
    }
  }

  // update all new comments whenever display new comments is updated
  // ALL comments will be handled by CodeViewer's event handler for efficiency reasons
  React.useEffect(() => {
    if (displayNewComments && Object.keys(displayNewComments).length) {
      const newNewComments = {
        ...newComments,
        [filePath.join('/')]: {...displayNewComments},
      }
      setNewComments(newNewComments);
    }
  }, [displayNewComments]);

  React.useEffect(updateCurrentView, [rfrid, revid]);

  React.useEffect(async () => {
    if (filePath && filePath.length) {
      setFileLoading(true);
      setFileContents(null);
      setDisplayPermaComments({});
      setDisplayNewComments({});
      try {
        const fileName = filePath.join('/');
        const res = await newRequest(`rfr/${rfrId}/item/${fileName}`);
        const { data } = res;
        // compatibility w/ our code viewer
        if (data.right && !data.left) {
          data.left = data.right;
          data.right = null;
        }
        setFileContents(data);
        setDisplayPermaComments(permaComments[fileName] || {});
        setDisplayNewComments(newComments[fileName] || {});
      } catch (err) {
        showErrorDialog('Error Retreiving File', getErrorMessage(err));
      } finally {
        setFileLoading(false);
      }
    }
  }, [filePath]);

  const generateAllCommentsRow = (commentDict) => {
    return Object.keys(commentDict).map((linePos) => {
      const lineNo = linePos.substr(2);
      let letter = '';
      switch (linePos[0]) {
        case 'r':
        case 'R':
          letter = ' (N)';
          break;
        case 'l':
        case 'L':
          letter = ' (O)';
          break;
      }
      return (
        <TableRow>
          <TableCell className={classes.commentOverviewTableCell}>
            {`${lineNo}${letter}`}
          </TableCell>
          <TableCell className={classes.commentOverviewTableCell}>
            {generateParagraphs(commentDict[linePos])}
          </TableCell>
        </TableRow>
      );
    });
  }

  const generateAllCommentsAccordion = () => {
    return Object.keys(allComments).map((filename) => {
      const commentDict = allComments[filename];
      if (Object.keys(commentDict).length) {
        return (
          <Accordion style={{width:'100%'}}>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
              <Grid container>
                <Grid item xs={9}>
                  <Typography className={classes.heading}>{filename}</Typography>
                </Grid>
                <Grid item xs={3} style={{textAlign: 'right'}}>
                  <Button
                    onClick={(e) => handleViewFile(e, filename)}
                    size="small"
                    startIcon={<DescriptionIcon />}
                    variant="outlined"
                  >
                    View File
                  </Button>
                </Grid>
              </Grid>
            </AccordionSummary>
            <AccordionDetails>
              <TableContainer>
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell className={classes.commentOverviewTableCell}>Line</TableCell>
                      <TableCell className={classes.commentOverviewTableCell}>Comment</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {generateAllCommentsRow(commentDict)}
                  </TableBody>
                </Table>
              </TableContainer>
            </AccordionDetails>
          </Accordion>
        );
      }
      return null;
    });
  }

  return (
    <>
      <Navbar />
      <ConfirmDialog
        caption={errTitle}
        text={errMsg}
        setOpen={setOpenErrDialog}
        open={openErrDialog}
        okOnly
      />
      <ConfirmDialog
        caption={confirmTitle}
        text={confirmMessage}
        setOpen={setConfirmOpen}
        open={confirmOpen}
        actionYes={confirmAction}
      />
      <Notification notify={notify} setNotify={setNotify} />
      <Container className={classes.root} maxWidth={false}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <Typography variant="h3">Review</Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="h6">
              <Link
                rel="noopener noreferrer"
                target="_blank"
                to={`/rfr/${rfrId}/overview`}
              >
                Open RFR
              </Link>
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Container>
              <Grid container spacing={1}>
                <Grid item xs={12}>
                  <Typography variant="h5">Overall Review</Typography>
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    label="Overall RFR Review"
                    placeholder="Write your thouhgts about this RFR here"
                    multiline
                    fullWidth
                    disabled={!editMode || published}
                    value={overallReview}
                    onChange={(e) => setOverallReview(e.target.value)}
                    maxRows={3}
                  />
                </Grid>
                <Grid item xs={6} hidden={!editMode}>
                  <Button
                    variant="contained"
                    disabled={loading}
                    color="primary"
                    fullWidth
                    onClick={askSaveReview}
                  >
                    Save Review Contents
                  </Button>
                </Grid>
                <Grid item xs={6} hidden={!editMode || !allowPublish}>
                  <Button
                    fullWidth
                    variant="outlined"
                    disabled={loading}
                    color="primary"
                    onClick={askPublishReview}
                  >
                    Publish Review
                  </Button>
                </Grid>
                <Grid item xs={12}>
                  <LinearProgress fullWidth hidden={!loading} />
                </Grid>
              </Grid>
            </Container>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="h5">Detailed Review</Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="body">
              You can add and view comments about each lines in the files from this RFR here.
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Paper square>
              <Tabs
                value={viewMode}
                onChange={(_, v) => setViewMode(v)}
                indicatorColor="primary"
                textColor="primary"
              >
                <Tab label="Browse and Comment" />
                <Tab label="All Comments" />
              </Tabs>
            </Paper>
          </Grid>
          <Grid item container xs={12} spacing={2} hidden={viewMode !== 0}>
            <Grid item xs={12}>
              <Breadcrumbs>
                {
                  (browsePath || []).map((obj) => (
                    <Typography>{obj}</Typography>
                  ))
                }
              </Breadcrumbs>
            </Grid>
            <Grid item xs={12} className={classes.folderViewCont}>
              <TwinFolderView
                dirTree={dirTree}
                onPathChange={(v) => setBrowsePath(v)}
                onFileOpen={(paths, fileName) => setFilePath([...paths, fileName])}
              />
            </Grid>
            <Grid item xs={12}>
              <Breadcrumbs>
                {
                  (filePath || []).map((obj) => (
                    <Typography>{obj}</Typography>
                  ))
                }
              </Breadcrumbs>
            </Grid>
            <Grid item xs={12}>
              <LinearProgress fullWidth hidden={!fileLoading} />
            </Grid>
            <Grid item xs={12}>
              <Breadcrumbs></Breadcrumbs>
            </Grid>
            <Grid item xs={12}>
              {
                fileContents
                  ? (
                      <CodeViewer
                        left={fileContents.left}
                        right={fileContents.right}
                        editable={editMode}
                        comments={displayPermaComments}
                        editableComments={displayNewComments}
                        setEditableComments={setDisplayNewComments}
                        onCommentUpdate={handleCommentUpdate}
                        onCommentDelete={handleCommentDelete}
                      />
                    )
                  : '(Click on a file to view its differences and comments)'
              }
            </Grid>
          </Grid>
          <Grid item container xs={12} spacing={2} hidden={viewMode !== 1}>
            <Container style={{ paddingTop:'24px' }}>
              <Grid container spacing={3}>
                <Grid item xs={6} hidden={true}>
                  <Button fullWidth variant="outlined" color="primary">Expand All</Button>
                </Grid>
                <Grid item xs={6} hidden={true}>
                  <Button fullWidth variant="outlined" color="secondary">Collapse All</Button>
                </Grid>
                <Grid item xs={12}>
                  {generateAllCommentsAccordion()}
                </Grid>
              </Grid>
            </Container>
          </Grid>
        </Grid>
      </Container>
    </>
  );
};