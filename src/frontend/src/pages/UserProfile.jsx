/* eslint-disable  */
/* eslint-disable no-restricted-syntax */
/* eslint-disable linebreak-style */
import React, { useEffect, useState } from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {
  Button, Avatar, IconButton, CircularProgress, Divider, makeStyles, Badge, LinearProgress,
} from '@material-ui/core';
import Chip from '@material-ui/core/Chip';
import { Link, useParams } from 'react-router-dom';
import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';

import Navbar from '../components/Navbar';
import RepoCard from '../components/RepoCard';
import EditProfile from '../components/EditProfile';
import { newRequest, getErrorMessage } from '../libs/utils';
import ConfirmDialog from '../components/ConfirmDialog';
import Notification from '../components/UserCreation/Notification';
import RFRItem from '../components/RFRItem';

const useStyles = makeStyles((theme) => ({
  header: {
    display: 'flex',
    alignItems: 'center',
    margin: '52px 0',
  },
  addIcon: {
    marginLeft: '8px',
    cursor: 'pointer',
  },
  viewAll: {
    textAlign: 'right',
    flexGrow: '1',
    textDecoration: 'none',
  },
  repoList: {
    display: 'flex',
    flexWrap: 'wrap',
    alignContent: 'center',
  },
  repoItem: {
    boxSizing: 'border-box',
    margin: '0 10px 40px 0',
    width: '220px',
    height: '180px',
    '& div.MuiChip-root': {
      bottom: '50px',
    },
  },
  rfrList: {
    display: 'flex',
    flexDirection: 'column',
  },
  rfritem: {
    marginBottom: '40px',
  },
  profile: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  avatar: {
    width: '256px',
    height: '256px',
  },
  addPhotoIcon: {
    width: '50%',
    height: '50%',
  },
  addImageButton: {
    margin: '52px 0 32px',
  },
  description: {
    display: 'inline-block',
    width: '350px',
    lineHeight: '1.7rem',
    marginLeft: '50px',
  },
  button: {
    width: '40%',
    margin: '16px',
    textTransform: 'none',
  },
  tags: {
    width: '80%',
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    padding: '8px',
    '& > *': {
      margin: theme.spacing(0.5),
    },
  },
  profileSeparator: {
    width: '100%',
    marginTop: '16px',
    marginBottom: '16px',
  },
  karmaBadge: {
    border: '1px solid white',
    borderRadius: '50%',
  },
}));

function UserProfile() {
  const classes = useStyles();
  const [paramUsername, setParamUsername] = useState(useParams().username);
  const [user, setUser] = useState(null);
  const [myProfile, setMyProfile] = useState(true);
  const [loading, setLoading] = useState(true);
  const [photoLoading, setPhotoLoading] = useState(false);
  const [tags, setTags] = useState([]);
  const [edit, setEdit] = useState(false);
  const [avatar, setAvatar] = useState(null);
  const [deleteProfpicConfirmShow, setDeleteProfpicConfirmShow] = useState(false);
  const [notify, setNotify] = useState({
    isOpen: false,
    message: '',
    type: '',
  });
  const [repoLoading, setRepoLoading] = React.useState(false);
  const [repos, setRepos] = React.useState([]);

  const [rfrLoading, setRfrLoading] = React.useState(false);
  const [rfrs, setRfrs] = React.useState([]);

  React.useEffect(async () => {
    setRfrLoading(true);
    try {
      const res = await newRequest(`rfr/other/${paramUsername}`);
      console.log(res.data);
      setRfrs(res.data.rfr_list || []);
    } catch (err) {
      setNotify({
        isOpen: true,
        message: `Error getting your list of Request for Reviews: ${getErrorMessage(err)}`,
        type: 'error',
      });
    } finally {
      setRfrLoading(false);
    }
  }, [paramUsername]);

  const loadUserRepos = async () => {
    setRepoLoading(true);
    try {
      const res = await newRequest(`repo/byuser/${paramUsername}`);
      setRepos(res.data);
    } catch (err) {
      setNotify({
        isOpen: true,
        message: `Error getting your repositories: ${getErrorMessage(err)}`,
        type: 'error',
      });
    } finally {
      setRepoLoading(false);
    }
  };

  React.useEffect(async () => {
    loadUserRepos();
  }, [paramUsername]);

  const displayTags = (tagsObject) => {
    const tagEls = [];
    for (const [tag, karma] of Object.entries(tagsObject)) {
      const myObj = (
        <Badge badgeContent={karma} color="secondary">
          <Chip label={tag} color="primary" />
        </Badge>
      );
      tagEls.push(myObj);
    }
    setTags(tagEls);
  };

  const getOtherUser = () => newRequest(`account/${paramUsername}`)
    .then((res) => res.data)
    .catch((err) => {
      console.log(err);
    });

  const loadPhoto = async () => {
    try {
      const res = await newRequest('photos/profpic/my', 'get', null, null, true);
      const urlPic = window.URL.createObjectURL(res.data);
      setAvatar(urlPic);
    } catch (err) {
      console.log('Error loading photo.');
      setAvatar(null);
    }
  };

  const handleDeletePhoto = async () => {
    setPhotoLoading(true);
    setAvatar(null);
    try {
      await newRequest('photos/profpic', 'delete');
    } catch {
      console.log('Error delete profile picture.');
    } finally {
      loadPhoto().finally(() => setPhotoLoading(false));
    }
  };

  const handleAddPhoto = async () => {
    // open file picker
    let fileHandle;
    try {
      [fileHandle] = await window.showOpenFilePicker({
        multiple: false,
        types: [
          {
            description: 'Images',
            accept: {
              'image/*': ['.png', '.gif', '.jpeg', '.jpg', '.webp', '.heif', '.heic'],
            },
          },
        ],
      });
    } catch {
      console.log('File selection cancelled.');
      return;
    }

    setPhotoLoading(true);
    setAvatar(null);
    const data = await fileHandle.getFile();
    try {
      await newRequest('photos/profpic', 'post', data, 'application/octet-stream');
    } catch (err) {
      setNotify({
        isOpen: true,
        type: 'error',
        message: getErrorMessage(err),
      });
    } finally {
      loadPhoto().finally(() => setPhotoLoading(false));
    }
  };

  useEffect(async () => {
    let isMounted = true;
    newRequest('account/my')
      .then(async (res) => {
        if (isMounted) {
          if (res.data.username !== paramUsername) {
            console.log(res.data.username,paramUsername);
            const otherUser = await getOtherUser();
            setUser(otherUser);
            displayTags(otherUser.tags);
            setMyProfile(false);
          } else {
            setUser(res.data);
            setTags([]);
            displayTags(res.data.tags);
            setMyProfile(true);
          }
        }
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        if (isMounted) {
          setLoading(false);
        }
      });
    return () => { isMounted = false; };
  }, [edit, paramUsername]);

  useEffect(() => {
    setPhotoLoading(true);
    loadPhoto().finally(() => setPhotoLoading(false));
  }, [paramUsername]);

  return (
    (loading) ? (<div>Loading...</div>)
      : (
        <>
          <Navbar />
          <ConfirmDialog
            caption="Delete Profile Picture"
            text="Confirm delete profile picture?"
            open={deleteProfpicConfirmShow}
            setOpen={setDeleteProfpicConfirmShow}
            actionYes={handleDeletePhoto}
          />
          <Notification notify={notify} setNotify={setNotify} />
          <Container maxWidth="lg" className={classes.root}>
            <Grid container direction="row" justifyContent="flex-start">
              <Grid item xs={7}>
                <div className={classes.header}>
                  <Typography component="h1" variant="h5">Repositories</Typography>
                  <Link hidden to="/repos" className={classes.viewAll}>
                    <Typography component="h1" variant="subtitle2">View all</Typography>
                  </Link>
                </div>
                <div className={classes.repoList}>
                  { (repos.length > 0)
                    ? repos.map((repo) => (
                      <RepoCard
                        className={classes.repoItem}
                        repoObject={repo}
                      />
                    ))
                    : (
                      <Typography align="center" style={{ width: '100%' }}>{repoLoading ? '(loading)' : 'No repositories available'}</Typography>)}
                </div>
                <div className={classes.header}>
                  <Typography component="h1" variant="h5">Activities</Typography>
                  <Link hidden to="/rfrs" className={classes.viewAll}>
                    <Typography component="h1" variant="subtitle2">View all</Typography>
                  </Link>
                </div>
                <div className={classes.rfrList}>
                  <LinearProgress fullWidth style={{ width: '100%' }} hidden={!rfrLoading} />
                  { (rfrs.length)
                    ? rfrs.map((rfr) => (
                      <RFRItem rfrObj={rfr} />
                    ))

                    : (
                      <Typography align="center" style={{ width: '100%' }}>{rfrLoading ? '(loading)' : 'No Request for Review available'}</Typography>)}
                </div>
              </Grid>
              <Grid item xs={5} className={classes.profile}>
                <IconButton
                  className={classes.addImageButton}
                  disabled={photoLoading && myProfile}
                  onClick={handleAddPhoto}
                >
                  <Avatar alt="protrait" src={avatar} className={classes.avatar}>
                    <AddAPhotoIcon className={classes.addPhotoIcon} hidden={photoLoading} />
                    <CircularProgress hidden={!photoLoading} />
                  </Avatar>
                </IconButton>
                {myProfile
                  ? (
                    <Grid container spacing={2}>
                      <Grid item xs={6}>
                        <Button
                          variant="contained"
                          color="primary"
                          fullWidth
                          disabled={photoLoading}
                          onClick={handleAddPhoto}
                        >
                          Add Photo
                        </Button>
                      </Grid>
                      <Grid item xs={6}>
                        <Button
                          variant="contained"
                          color="secondary"
                          fullWidth
                          disabled={photoLoading || !avatar}
                          onClick={() => setDeleteProfpicConfirmShow(true)}
                        >
                          Delete Photo
                        </Button>
                      </Grid>
                    </Grid>
                  ) : null}

                <Divider className={classes.profileSeparator} />
                <div className={classes.profile}>
                  {edit ? <EditProfile user={user} setParamUsername={setParamUsername} setEdit={setEdit} />
                    : (
                      <>
                        <Typography component="h2" variant="h5" gutterTop gutterBottom>{user.username}</Typography>
                        <Typography component="p" variant="body1" gutterBottom>
                          {`Karma ${user.total_karma}`}
                        </Typography>
                        <div className={classes.tags}>
                          {tags}
                        </div>
                        <Typography component="p" variant="body1" color="textSecondary" className={classes.description} gutterBottom>
                          {user.description}
                        </Typography>
                        {myProfile
                          ? (
                            <Button
                              variant="contained"
                              color="primary"
                              className={classes.button}
                              onClick={() => setEdit(true)}
                            >
                              Edit Profile
                            </Button>
                          ) : null}

                      </>
                    )}
                </div>
              </Grid>
            </Grid>
          </Container>
        </>
      )
  );
}

export default UserProfile;
