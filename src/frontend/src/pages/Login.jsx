/* eslint-disable no-nested-ternary */
/* eslint-disable linebreak-style */
import { React, useState, useEffect } from 'react';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import LinearProgress from '@material-ui/core/LinearProgress';
import { makeStyles } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import { Redirect, useHistory } from 'react-router-dom';
import logo from '../assets/gitreview_logo.png';
import Notification from '../components/UserCreation/Notification';
import useAuth from '../components/useAuth';
import {
  handleAnchorNavigate, getErrorMessage, newRequest,
} from '../libs/utils';

const useStyles = makeStyles((theme) => ({
  main: {
    position: 'absolute',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  alert: {
    width: '50%',
    marginBottom: '20px',
  },
  logo: {
    marginBottom: theme.spacing(6),
  },
  loginContainer: {
    boxShadow: 'rgb(0 0 0 / 10%) 0px 0px 10px',
    padding: '32px 48px',
    borderRadius: '5px',
    width: '483px',
  },

  button: {
    marginTop: '24px',
  },
  forgetPasswordLink: {
    textAlign: 'center',
    marginTop: '16px',
  },
  registerContainer: {
    paddingTop: '32px',
    marginTop: '16px',
    borderTop: '1px solid rgb(213, 216, 222)',
    textAlign: 'center',
  },
}));

function Login() {
  const classes = useStyles();
  const history = useHistory();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const [notify, setNotify] = useState({
    isOpen: false,
    message: '',
    type: '',
  });
  const authObj = useAuth();

  useEffect(() => {
    if (authObj.user) {
      history.push('/dashboard');
    }
  }, [authObj.user]);

  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);
    newRequest('auth/login', 'post', {
      email,
      password,
    })
      .then((response) => {
        const { data } = response;
        authObj.setToken(data.token);
      })
      .catch((err) => {
        setNotify({
          isOpen: true,
          message: getErrorMessage(err),
          type: 'error',
        });
        setPassword('');
      })
      .finally(() => setLoading(false));
  };

  return (
    (authObj.loading ? (
      <div>Loading...</div>
    ) : (!authObj.user
      ? (
        <>
          <Notification notify={notify} setNotify={setNotify} />
          <Container component="main" maxwidth="xs" className={classes.main}>
            <img src={logo} alt="GitReview Logo" width="410" height="58" className={classes.logo} />
            <form noValidate className={classes.loginContainer}>
              <Typography gutterBottom align="center" variant="h6" component="h1">Sign in to GitReview</Typography>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                value={email}
                type="email"
                autoFocus
                onChange={(e) => setEmail(e.target.value)}
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.button}
                onClick={handleSubmit}
                disabled={loading}
              >
                Sign In
              </Button>
              <LinearProgress fullWidth hidden={!loading} />
              <div className={classes.forgetPasswordLink}>
                <Typography>
                  <Link href="/resetpassword/request" onClick={handleAnchorNavigate(history)}>
                    Forget your password?
                  </Link>
                </Typography>
              </div>
              <div className={classes.registerContainer}>
                <Typography>
                  <Link href="/register" onClick={handleAnchorNavigate(history)}>
                    Don&apos;t have an account? Register now
                  </Link>
                </Typography>
              </div>
            </form>
          </Container>
        </>
      )
      : <Redirect to="/dashboard" />
    ))
  );
}

export default Login;
