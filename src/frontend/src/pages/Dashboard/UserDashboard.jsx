import React from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import { LinearProgress, makeStyles } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { Link } from 'react-router-dom';

import Navbar from '../../components/Navbar';
import RepoCard from '../../components/RepoCard';
import { getErrorMessage, newRequest } from '../../libs/utils';
import Notification from '../../components/UserCreation/Notification';
import RFRItem from '../../components/RFRItem';

const useStyles = makeStyles(() => ({
  repo: {
    paddingBottom: '48px',
    borderBottom: '3px solid rgb(213, 216, 222)',
  },
  header: {
    display: 'flex',
    alignItems: 'center',
    margin: '25px 0',
  },
  addIcon: {
    marginLeft: '8px',
    cursor: 'pointer',
  },
  viewAll: {
    textAlign: 'right',
    flexGrow: '1',
    textDecoration: 'none',
  },
  repoList: {
    display: 'flex',
    flexWrap: 'wrap',
    maxHeight: '410px',
    overflow: 'hidden',
  },
  repoItem: {
    marginLeft: '20px',
    marginTop: '20px',
    marginBottom: '5px',
    width: '220px',
    height: '180px',
  },
  rfrList: {
    display: 'flex',
    flexDirection: 'column',
  },
  rfritem: {
    marginBottom: '40px',
  },
}));
function UserDashboard() {
  const classes = useStyles();
  const [repoLoading, setRepoLoading] = React.useState(false);
  const [repos, setRepos] = React.useState([]);
  const [notify, setNotify] = React.useState({
    isOpen: false,
    message: '',
    type: '',
  });

  const loadMyRepos = async () => {
    setRepoLoading(true);
    try {
      const res = await newRequest('repo/byuser/my');
      setRepos(res.data);
    } catch (err) {
      setNotify({
        isOpen: true,
        message: `Error getting your repositories: ${getErrorMessage(err)}`,
        type: 'error',
      });
    } finally {
      setRepoLoading(false);
    }
  };

  React.useEffect(async () => {
    loadMyRepos();
  }, []);

  const [rfrLoading, setRfrLoading] = React.useState(false);
  const [rfrs, setRfrs] = React.useState([]);

  React.useEffect(async () => {
    setRfrLoading(true);
    try {
      const res = await newRequest('rfr/my');
      console.log(res.data);
      setRfrs(res.data.rfr_list || []);
    } catch (err) {
      setNotify({
        isOpen: true,
        message: `Error getting your list of Request for Reviews: ${getErrorMessage(err)}`,
        type: 'error',
      });
    } finally {
      setRfrLoading(false);
    }
  }, []);

  return (
    <>
      <Navbar />
      <Notification notify={notify} setNotify={setNotify} />
      <Container maxWidth="lg">
        <div className={classes.repo}>
          <div className={classes.header}>
            <Typography component="h5" variant="h5">Your Repositories</Typography>
            <Link to="/repo/import">
              <AddIcon className={classes.addIcon} />
            </Link>
            <Link hidden to="/repos" className={classes.viewAll}>
              <Typography component="h1" variant="subtitle2">View all</Typography>
            </Link>
          </div>
          <div className={classes.repoList}>
            <LinearProgress fullWidth style={{ width: '100%' }} hidden={!repoLoading} />
            { (repos.length)
              ? repos.map((repo) => (
                <RepoCard
                  className={classes.repoItem}
                  repoObject={repo}
                />
              ))
              : (
                <Typography align="center" style={{ width: '100%' }}>{repoLoading ? '(loading)' : 'No repositories available'}</Typography>)}
          </div>
        </div>
        <div>
          <div className={classes.header}>
            <Typography component="h1" variant="h5">Request for Review</Typography>
            <Link to="/rfrs" hidden className={classes.viewAll}>
              <Typography component="h1" variant="subtitle2">View all</Typography>
            </Link>
          </div>
          <div className={classes.rfrList}>
            <LinearProgress fullWidth style={{ width: '100%' }} hidden={!rfrLoading} />
            { (rfrs.length)
              ? rfrs.map((rfr) => (
                <RFRItem rfrObj={rfr} />
              ))

              : (
                <Typography align="center" style={{ width: '100%' }}>{rfrLoading ? '(loading)' : 'No Request for Review available'}</Typography>)}
          </div>
        </div>
      </Container>
    </>
  );
}

export default UserDashboard;
