/* eslint-disable linebreak-style */
import { React, useState } from 'react';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import LinearProgress from '@material-ui/core/LinearProgress';
import InputLabel from '@material-ui/core/InputLabel';
import { useHistory } from 'react-router-dom';
import { ReactComponent as ReactLogo } from '../assets/logo.svg';
import Notification from '../components/UserCreation/Notification';
import { handleAnchorNavigate, getErrorMessage, newRequest } from '../libs/utils';

const useStyles = makeStyles((theme) => ({
  main: {
    position: 'absolute',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    marginBottom: theme.spacing(6),
  },
  loginContainer: {
    boxShadow: 'rgb(0 0 0 / 10%) 0px 0px 10px',
    padding: '32px 48px',
    borderRadius: '5px',
    width: '483px',
  },
  emailInstruction: {
    paddingTop: '10px',
  },
  button: {
    marginTop: '24px',
  },
  forgetPasswordLink: {
    textAlign: 'center',
    marginTop: '16px',
  },
  registerContainer: {
    paddingTop: '32px',
    marginTop: '16px',
    borderTop: '1px solid rgb(213, 216, 222)',
    textAlign: 'center',
  },
  loading: {
    marginTop: '100px',
  },
}));

function ResetPasswordRequest() {
  const classes = useStyles();
  const history = useHistory();
  const [email, setEmail] = useState('');
  const [loading, setLoading] = useState(false);
  const [notify, setNotify] = useState({
    isOpen: false,
    message: '',
    type: '',
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);
    newRequest('account/iforgot/request', 'post', { email })
      .then(() => {
        setNotify({
          isOpen: true,
          message: 'Forget password request successful, please check your email!',
          type: 'success',
        });
        setTimeout(() => {
          history.push('/login');
        }, 2000);
      })
      .catch((err) => {
        setNotify({
          isOpen: true,
          message: getErrorMessage(err),
          type: 'error',
        });
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <>
      {/* {loading ? (<CircularProgress />) : null} */}
      <Notification notify={notify} setNotify={setNotify} />
      <Container component="main" maxwidth="xs" className={classes.main}>
        <ReactLogo className={classes.logo} />
        <form noValidate className={classes.loginContainer}>
          <Typography gutterBottom align="center" variant="h6" component="h1">Reset your password</Typography>
          <InputLabel htmlFor="email" className={classes.emailInstruction}>
            Enter your email address and we will send you a password reset link
          </InputLabel>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email address"
            name="email"
            autoComplete="email"
            value={email}
            type="email"
            autoFocus
            onChange={(e) => setEmail(e.target.value)}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={handleSubmit}
            disabled={loading}
          >
            Reset password
          </Button>
          <LinearProgress fullWidth hidden={!loading} />
          <div className={classes.registerContainer}>
            <Typography>
              <Link href="/login" onClick={handleAnchorNavigate(history)}>
                Return to log in
              </Link>
            </Typography>
          </div>
        </form>
      </Container>
    </>
  );
}

export default ResetPasswordRequest;
