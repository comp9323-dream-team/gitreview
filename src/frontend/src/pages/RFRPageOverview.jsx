/* eslint-disable */
import React from 'react';
import {
  Container, makeStyles, Typography, Button, Paper,
  CircularProgress, Grid, Backdrop
} from '@material-ui/core';
import { Link, useParams, useHistory } from 'react-router-dom';
import CustomBreadcrumbs from '../components/CustomBreadcrumbs';
import Navbar from '../components/Navbar';
import CustomTabsRFR from '../components/CustomTabsRFR';
import { newRequest, getErrorMessage } from '../libs/utils';
import Overview from '../components/RFRView/Overview';
import ChangesView from '../components/RFRView/ChangesView';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import Notification from '../components/UserCreation/Notification';

const useStyles = makeStyles(() => ({
  root: {
    // marginLeft: '148px',
  },
  branchRow: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: '20px 0',
  },
  treeview: {
    marginTop: '10px',
    marginLeft: '50px',
    height: 120,
    flexGrow: 1,
    maxWidth: 400,
  },
  mainTitle: {
    margin: '16px',
  },
  title: {
    marginBottom: '30px',
  },
  flexcolscroll: {
    flexGrow: 1,
    overflow: 'auto',
    minHeight: 300,
    marginLeft: 50,
    marginRight: 50,
    marginBottom: 100,
  },
  text: {
    padding: 'calc(1em + 1ex)',
    margin: 'calc(1em + 1ex)',
  },
  refreshButton: {
    height: '50px',
  },
  loadingContainer: {
    textAlign: 'center',
    margin: '1cm',
  },
  changeRfrButton: {
    marginRight: '10px',
  },
  buttonGroup: {
    padding: '18px',
    marginBottom: '16px',
  },
  backdrop: {
    color: '#fff',
  },
}));

function RFRPageOverview() {
  const classes = useStyles();
  const { rfrid, mode } = useParams();
  const history = useHistory();

  const [rfrObj, setRfrObj] = React.useState(null);
  const [repoObj, setRepoObj] = React.useState({});
  const [mainLoading, setMainLoading] = React.useState(true);
  const [errMsg, setErrMsg] = React.useState('');
  const [userObj, setUserObj] = React.useState(null);
  const [ChangeRFR, setChangeRFR] = React.useState(false);
  const [notify, setNotify] = React.useState({
    isOpen: false,
    message: '',
    type: '',
  });
  

  React.useEffect(async () => {
    setMainLoading(true);
    try {
      const res = await newRequest(`rfr/${rfrid}`);
      console.log(res.data);
      const repoRes = await newRequest(`repo/${res.data.repoid}`);
      const userRes = await newRequest('account/my');
      setRfrObj(res.data);
      setRepoObj(repoRes.data);
      setUserObj(userRes.data);
      console.log(res.data.owner, userRes.data.username)
      
    } catch (err) {
      setErrMsg(getErrorMessage(err));
    }
    finally {
      setMainLoading(false);
    }
  }, [rfrid]);

  const acceptRFR = async () => {
    let branch = "";
    if(rfrObj.type == "ps") {
      // ask for branch
    }
    const data = {
        branch: branch,
        "status": "accepted"
    }
    try {
      const res = await newRequest(`rfr/${rfrid}/status`, 'put', data);
      if (res.data) {
        history.push(`/repo/view/${rfrObj.repoid}/rfr`);
      }
    } catch (err) {
      setNotify({
        isOpen: true,
        type: 'error',
        message: `Error updating RFR status: ${getErrorMessage(err)}`,
      });
    }
  };

  const rejectRFR = async () => {
    try {
      const data = {
        branch: "",
        status: "rejected"
      }
      const res = await newRequest(`rfr/${rfrid}/status`, 'put', data);
      if (res.data) {
        history.push(`/repo/view/${rfrObj.repoid}/rfr`);
      }
    } catch (err) {
      setNotify({
        isOpen: true,
        type: 'error',
        message: `Error updating RFR status: ${getErrorMessage(err)}`,
      });
    }
  };

  function changeRfrStatus() {
    setChangeRFR(!ChangeRFR);
  }


  const tabSel = mode === 'overview' ? 0 : 1;

  const overviewObj = (
    <Overview
      rfrObj={rfrObj}
    />
  )

  const codeObj = (
    <ChangesView
      rfrObj={rfrObj} />
  )

  const mainContent = (
    <Container className={classes.root} maxWidth={false}>
      <Typography variant="h3" component="h1" className={classes.mainTitle}>RFR</Typography>
      <Link to={`/repo/view/${repoObj.repoid}/rfr`}>
        <Typography variant="h4" component="h1" className={classes.mainTitle}>{repoObj.name}</Typography>
      </Link>
      <Notification notify={notify} setNotify={setNotify} />
      <div className={classes.buttonGroup}>
        <Button variant="contained" color="primary" className={classes.changeRfrButton} onClick={() => history.push(`/review/new/${rfrid}`)}>
          Review This RFR
        </Button>
        {!mainLoading && userObj.username === rfrObj.owner ? (
          <>
          <Button variant="contained" color="primary" className={classes.changeRfrButton} onClick={changeRfrStatus}>
            Change RFR Status
          </Button>
          <Button variant="contained" color="secondary" onClick={() => history.push(`/rfr/${rfrid}/invite-reviewer`)}>
            Invite Reviewer
          </Button>
          </>
          ) :
          null
        }
      </div>
      <CustomTabsRFR tab={tabSel} rfrId={rfrid} />
      {tabSel === 0 ? overviewObj : codeObj}
      <Dialog
        open={ChangeRFR}
        onClose={changeRfrStatus}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Ready to change RFR Status?"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            You can accept or reject an RFR - accepting will merge the changes in the RFR into your repository. Do this if you are satisfied with the reviews.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button color="primary" onClick={acceptRFR}>
            Accept RFR
          </Button>
          <Button color="primary" onClick={rejectRFR}>
            Reject RFR
          </Button>
        </DialogActions>
      </Dialog>
    </Container>
  )

  const mainLandingPage = (
    <Grid container>
      <Grid item xs={12} hidden={!mainLoading} className={classes.loadingContainer}>
        <CircularProgress />
      </Grid>
      <Grid item xs={12} hidden={mainLoading} className={classes.loadingContainer}>
        <Typography variant="h4">Error Loading RFR</Typography>
        <Typography variant="subtitle1">{errMsg}</Typography>
      </Grid>
    </Grid>
  );

  return (
    <>
      <Navbar />
      {!rfrObj ? mainLandingPage : mainContent}
    </>
  );
}

export default RFRPageOverview;
