/* eslint-disable react/no-children-prop */
/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-multi-str */
/* eslint-disable linebreak-style */
import React from 'react';
import ReactMarkdown from 'react-markdown';
import { useHistory } from 'react-router-dom';
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { vs } from 'react-syntax-highlighter/dist/esm/styles/prism';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { LinearProgress, makeStyles } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import TextFieldWithChip from '../components/TextFieldWithChip';
import Navbar from '../components/Navbar';
import Notification from '../components/UserCreation/Notification';
import { getErrorMessage, newRequest } from '../libs/utils';
import { privateKeySteps } from '../libs/instructions';

const components = {
  code({
    node, inline, className, children, ...props
  }) {
    const match = /language-(\w+)/.exec(className || '');
    return !inline && match ? (
      <SyntaxHighlighter style={vs} language={match[1]} PreTag="div" children={String(children).replace(/\n$/, '')} {...props} />
    ) : (
      <code className={className} {...props}>
        {children}
      </code>
    );
  },
};

const useStyles = makeStyles(() => ({
  root: {
    // marginLeft: '148px',
  },
  title: {
    marginTop: '54px',
    marginBottom: '54px',
  },
}));

function SetupRemote() {
  const classes = useStyles();
  const history = useHistory();

  const [loading, setLoading] = React.useState(false);
  const [formState, setFormState] = React.useState({});
  const [notify, setNotify] = React.useState({
    isOpen: false,
    message: '',
    type: '',
  });

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!formState.tags) {
      setNotify({
        isOpen: true,
        message: 'Please define at least one tag!',
        type: 'error',
      });
      return;
    }

    setLoading(true);
    // first, create the repo
    try {
      const res = await newRequest('repo', 'post', formState);
      const newRepoId = res.data.id;
      try {
        // don't need the result
        await newRequest(`repo/${newRepoId}/remote`, 'post', {
          key: formState.key,
          key_type: 'OPENSSH_PRIVATE_KEY_V1',
        });
        setNotify({
          isOpen: true,
          message: 'Repository created!',
          type: 'success',
        });
        // go back to dashboard
        setTimeout(() => {
          history.push('/dashboard');
        }, 2000);
      } catch {
        setNotify({
          isOpen: true,
          message: 'Your repo is successfully created, but we cannot accept your SSH key. Please review your SSH key in repo\'s settings.',
          type: 'warning',
        });
        setTimeout(() => {
          history.push(`/repo/settings/${newRepoId}`);
        }, 2000);
      }
    } catch (err) {
      setNotify({
        isOpen: true,
        message: `Error creating repository: ${getErrorMessage(err)}`,
        type: 'error',
      });
    } finally {
      setLoading(false);
    }
  };

  const handleFieldChangeGeneric = (fieldName, value) => {
    setFormState({
      ...formState,
      [fieldName]: value,
    });
  };

  const handleTextfieldChange = (e) => {
    handleFieldChangeGeneric(e.target.name, e.target.value);
  };

  return (
    <>
      <Navbar />
      <Notification notify={notify} setNotify={setNotify} />
      <Container component="main" maxWidth="lg" className={classes.root}>
        <Typography gutterBottom align="left" variant="h4" component="h1" className={classes.title}>Import Git Repository</Typography>
        <Grid container spacing={5}>
          <Grid item xs={6}>
            <form noValidate onSubmit={handleSubmit}>
              <InputLabel htmlFor="url">SSH Repository Address</InputLabel>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="url"
                type="text"
                onChange={handleTextfieldChange}
                placeholder="git@github.com:JohnDoe/sample.git"
              />
              <InputLabel htmlFor="name">Short Name</InputLabel>
              <TextField
                variant="outlined"
                margin="normal"
                fullWidth
                name="name"
                type="text"
                onChange={handleTextfieldChange}
                placeholder="Enter a name to make it easy to remember"
              />
              <InputLabel htmlFor="description">Description</InputLabel>
              <TextField
                variant="outlined"
                margin="normal"
                fullWidth
                name="description"
                type="text"
                multiline
                rows={3}
                onChange={handleTextfieldChange}
                placeholder="Enter the optional repository description here"
              />
              <InputLabel htmlFor="key">Private Key</InputLabel>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="key"
                type="text"
                multiline
                rows={10}
                onChange={handleTextfieldChange}
                placeholder="Begins with '---Begin OPENSSH PRIVATE KEY---'"
              />
              <InputLabel htmlFor="tags">Tags</InputLabel>
              <TextFieldWithChip
                placeholder="Enter tags separated by space"
                fullWidth
                variant="outlined"
                onChange={(v) => handleFieldChangeGeneric('tags', v)}
                margin="normal"
              />
              <Button
                type="submit"
                variant="contained"
                color="primary"
                disabled={loading}
                fullWidth
              >
                Save
              </Button>
              <LinearProgress fullWidth hidden={!loading} />
            </form>
          </Grid>
          <Grid item xs={6}>
            <div>
              <Typography align="center" component="h2" variant="h5" gutterBottom> Adding SSH key to GitReview </Typography>
              {/* eslint-disable-next-line max-len */}
              <ReactMarkdown components={components}>{privateKeySteps}</ReactMarkdown>
            </div>
          </Grid>
        </Grid>
      </Container>
    </>
  );
}

export default SetupRemote;
