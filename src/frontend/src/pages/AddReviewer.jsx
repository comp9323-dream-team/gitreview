/* eslint-disable */
import React, { useState, useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import { makeStyles, CircularProgress } from '@material-ui/core';
import Button from '@material-ui/core/Button';

import Navbar from '../components/Navbar';
import ReviewerCard from '../components/ReviewerCard';
import { getErrorMessage, newRequest } from '../libs/utils';
import Notification from '../components/UserCreation/Notification';

const useStyles = makeStyles(() => ({
  title: {
    marginTop: '54px',
    marginBottom: '54px',
  },
  label: {
    marginBottom: '20px',
  },
  search: {
    marginBottom: '54px',
  },
  searchField: {
    width: '80%',
    height: '40px',
    backgroundColor: '#ffffff',
    color: '#000000',
  },
  add: {
    height: '40px',
    width: '60px',
    marginLeft: '20px',
  },
}));

function AddReviewer() {
  const classes = useStyles();
  const { rfrid } = useParams();
  const history = useHistory();

  const [searchUser, setSearchUser] = useState('');
  const [suggestedList, SetSuggestedList] = useState([]);
  const [addedList, SetAddedList] = useState([]);
  const [suggestedLoading, setSuggestedLoading] = useState(true);
  const [notify, setNotify] = useState({
    isOpen: false,
    message: '',
    type: '',
  });

  const displaySuggestedList = (suggestedObj) => {
    console.log(suggestedObj);
    const userEls = [];
    for(const [username, userObj] of Object.entries(suggestedObj)) {
      const myObj = (
        <ReviewerCard
          key={username}
          username={username}
          karma={userObj.karma}
          tags={userObj.tags}
        />
      );
      userEls.push(myObj);
    }
    SetSuggestedList(userEls);
    setSuggestedLoading(false);
  };

  useEffect(async () => {
    try {
      const res = await newRequest(`reviewer/${rfrid}`);
      console.log(res.data);
      displaySuggestedList(res.data);
    } catch (err) {
      setNotify({
        isOpen: true,
        type: 'error',
        message: getErrorMessage(err),
      });
      setSuggestedLoading(false);
    }
  }, [rfrid]);


  const handleSubmit = async (e) => {
    const users = searchUser.split(',');
    e.preventDefault();
    const data = {
      reviewers: users
    }
    try {
      const res = await newRequest(`reviewer/${rfrid}`, 'post', data);
      setNotify({
        isOpen: true,
        type: 'success',
        message: 'Reviewers invited successfully.',
      });
    } catch (err) {
      setNotify({
        isOpen: true,
        type: 'error',
        message: getErrorMessage(err),
      });
    };
  };

  return (
    <>
      <Navbar />
      <Notification notify={notify} setNotify={setNotify} />
      <Container maxWidth="lg">
        <Typography component="h1" variant="h4" gutterBottom className={classes.title}>Invite Reviewer</Typography>
        <Grid container spacing={10}>
          <Grid item xs={6}>
            <Typography component="p" variant="h6" className={classes.label}>Search Name</Typography>
            <div className={classes.search}>
              <OutlinedInput value={searchUser} placeholder="john, peter" className={classes.searchField} onChange={(e) => setSearchUser(e.target.value)} />
              <Button
                variant="contained"
                color="primary"
                className={classes.add}
                onClick={handleSubmit}
              >
                Send
              </Button>
              {/* <Button variant="contained" color="primary" className={classes.add}>Add</Button> */}
            </div>
            {/* <Typography component="p" variant="h6" className={classes.label}>Added Reviewer</Typography>
            {addedList.map((reviewer) => (
              <ReviewerCard
                key={reviewer.name}
                avatar={reviewer.avatar}
                name={reviewer.name}
                tags={reviewer.tags}
              />
            ))} */}
          </Grid>
          <Grid item xs={6}>
            <Typography component="p" variant="h6" className={classes.label}>Suggested Reviewer</Typography>
            {suggestedLoading
              ? <CircularProgress />
              : suggestedList}
          </Grid>
        </Grid>
        <Button
          variant="contained"
          color="primary"
          className={classes.send}
          onClick={() => history.goBack()}
        >
          Back
        </Button>
      </Container>
    </>
  );
}

export default AddReviewer;
