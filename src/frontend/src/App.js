// import logo from './logo.svg';
// import './App.css';
import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import PrivateRoute from './components/PrivateRoute';
import Login from './pages/Login';
import Register from './pages/Register';
import ResetPasswordRequest from './pages/ResetPasswordRequest';
import ResetPasswordVerify from './pages/ResetPasswordVerify';
import UserProfile from './pages/UserProfile';
import SetupRemote from './pages/SetupRemote';
import RepoSettings from './pages/RepoSettings';
import RepoView from './pages/Repo/RepoView';
import NewRFR from './pages/Repo/NewRFR';
import UserDashboard from './pages/Dashboard/UserDashboard';
// eslint-disable-next-line
import RFRPageCode from './pages/RFRPageCode';
import AddReviewer from './pages/AddReviewer';
import RFRPageOverview from './pages/RFRPageOverview';
import ReviewPage from './pages/ReviewPage';

function App() {
  return (
    <Router>
      <Switch>
        {/* Sample Routes */}
        <Route exact path="/johndoe/sample/rfr/:rfr/invite-reviewer">
          <AddReviewer />
        </Route>
        {/* Connected Routes */}
        <Route exact path="/">
          <Login />
        </Route>
        <Route path="/login">
          <Login />
        </Route>
        <Route path="/register">
          <Register />
        </Route>
        <Route path="/resetpassword/request">
          <ResetPasswordRequest />
        </Route>
        <Route path="/resetpassword/verify/:token">
          <ResetPasswordVerify />
        </Route>
        <PrivateRoute exact path="/dashboard">
          <UserDashboard />
        </PrivateRoute>
        <Route exact path="/profile/:username">
          <UserProfile />
        </Route>

        <PrivateRoute exact path="/repo/import">
          <SetupRemote />
        </PrivateRoute>
        <PrivateRoute exact path="/repo/settings/:repoid">
          <RepoSettings />
        </PrivateRoute>
        <Route exact path="/repo/view/:repoid/:mode">
          <RepoView />
        </Route>

        <Route exact path="/repo/:repoid/newrfr">
          <NewRFR />
        </Route>
        <Route exact path="/rfr/:rfrid/invite-reviewer">
          <AddReviewer />
        </Route>
        <Route exact path="/rfr/:rfrid/:mode">
          <RFRPageOverview />
        </Route>
        <Route exact path="/review/view/:revid">
          <ReviewPage />
        </Route>
        <Route exact path="/review/new/:rfrid">
          <ReviewPage />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
