/* eslint-disable import/prefer-default-export */

export const privateKeySteps = `1. Open Git Bash.
2. Enter \`ls -al ~/.ssh\` to see if existing SSH keys are present: 
\`\`\`sh 
$ ls -al ~/.ssh 
# Lists the files in your .ssh directory, if they exist 
\`\`\` 
3. Check the directory listing to see if you already have a private SSH key. By default, the filename of the private key are \`id_rsa\`. 
4. Enter \`cat ~/.ssh/id_rsa\` to print the private ssh key. 
 \`\`\`sh 
$ cat ~/.ssh/id_rsa 
# Print the content of id_rsa 
\`\`\` 
5. Copy the content of \`id_rsa\` and paste it to Private Key field.`;
