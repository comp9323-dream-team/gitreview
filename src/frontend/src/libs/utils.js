import Axios from 'axios';
import BACKEND_URI from '../config';

export const handleAnchorNavigate = (history) => (e) => {
  // the power of closure!
  e.preventDefault();
  history.push(e.target.getAttribute('href'));
};

export const getErrorMessage = (err) => {
  if (err.response) {
    const respData = err.response.data;
    if ('message' in respData) {
      return respData.message;
    }
    return 'Unknown error.';
  }
  if (err.message) {
    return err.message;
  }
  return 'Network error.';
};

export const getAuthToken = () => localStorage.getItem('gitreview-token');

export const getEndpointUri = (endpoint) => `${BACKEND_URI}/${endpoint}`;

export const newRequest = (endpoint, method = 'get', body = null, bodyContentType = null, blobResponse = false) => {
  const headers = {};
  const authToken = getAuthToken();
  if (authToken) {
    headers.Authorization = `Token ${authToken}`;
  }
  if (bodyContentType) {
    headers['Content-Type'] = bodyContentType;
  }
  const reqObj = {
    method,
    headers,
    url: getEndpointUri(endpoint),
    data: body,
  };
  if (blobResponse) {
    reqObj.responseType = 'blob';
  }
  return Axios(reqObj);
};

export const readFileBase64 = (obj) => new Promise((resolve, reject) => {
  const reader = new FileReader();
  reader.readAsDataURL(obj);
  reader.onload = () => {
    // remove MIME URL declaration
    // it is in form of "data:*/*;base64,"
    // 7 is "base64,"
    const b64 = reader.result.substr(reader.result.indexOf('base64,') + 7);
    resolve(b64);
  };
  reader.onerror = (error) => reject(error);
});
