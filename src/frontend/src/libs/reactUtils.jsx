/* eslint-disable */
export const generateParagraphs = (data) => {
  const lines = data.split('\n');
  return lines.map((line) => (
    <p>{line}</p>
  ));
};
