# Updates / What have i done + Setup instruction + Technologies

## Technology

For the frontend, I decided to use:
- React JS (JS Frameworks)
- Material UI (React Component Library)
- React ESlint - Airbnb (JS and JSX Linter)
- React Router Dom (Dynamic Routing)
- Axios (HTTP Request to Backend)

Files Stracture:
- Every possible pages go under pages folder
- Every components go under components folder
- svg, img goes into assets

What have I done:
- Create Login and reset pages
- Need to fix few linter before able to display the frontend

## Backend

For able to run the backend, need to follow the instruction on backend folder and require linux os (WSL possible), not sure about windows (consult with Boni)
