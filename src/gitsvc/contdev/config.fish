#!/usr/bin/fish

# non mandatory configs
set -gx APT_PROXY "http://aptcache.lxc:8000"

# mandatory configs here
set -gx BASEIMG_URL "https://cdimages.ubuntu.com/ubuntu-base/releases/21.04/release/ubuntu-base-21.04-base-amd64.tar.gz"
set -gx BUSYBOX_URL "https://www.cse.unsw.edu.au/~z5272973/busybox"
set -gx BUSYBOX_INSTALLER_URL "https://raw.githubusercontent.com/jamestut/busybox-installer/main/busybox-installer.sh"
set -gx BASEIMG_FILENAME "ubuntu-base-21.04.tar.gz"
set -gx OUTPUT_FILENAME "gitreview-gitsvc-rootfs.tar.xz"
set -gx REPO_ADDR "http://mirror.internode.on.net/pub/ubuntu/ubuntu/"
set -gx REPO_DISTRO hirsute hirsute-updates
set -gx REPO_COMPONENT "main multiverse restricted universe"
set -gx LXC_PATH lxc
set -gx LXC_BASE_CONFIG_PATH base-lxc-config.conf
set -gx BUSYBOX_INSTALLER_PATH bbinstall/busybox-installer
set -gx SUPPLEMENTAL_PATH supplemental/install.fish
set -gx POSTSETUP_HOOK_PATH /opt/rootfs-setup/postsetup.sh
set -gx APT_INSTALL_PACKAGES git python3-pip fish
set -gx PIP3_INSTALL_REQUIREMENTS /opt/grsvc/requirements.txt
set -gx READINESS_CHECK_PATH /run/dhcpready
set -gx TZ Australia/Sydney