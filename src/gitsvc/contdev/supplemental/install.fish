#!/usr/bin/fish
if test -z ROOTFS_PATH
  echo "specify ROOTFS_PATH"
  exit 1
end
set SUPL_PATH (realpath supplemental)

function createdir
  if test 2 -ne (count $argv)
    echo "Usage: createdir (name) (perm)"
    exit 1
  end
  mkdir $argv[1]
  chmod $argv[2] $argv[1]
end

function copy_supl
  if test 3 -ne (count $argv)
    echo "Usage: copy_supl (source) (dest) (perm)"
    echo "  source = file, relative from 'supplemental' directory"
    echo "  dest   = target directory, rleative to pwd"
    echo "  perm   = permission of target file"
    exit 1
  end
  set TARGET_FILE_NAME (basename $argv[1])
  cp $SUPL_PATH/$argv[1] $argv[2]/$TARGET_FILE_NAME
  chmod $argv[3] $argv[2]/$TARGET_FILE_NAME
end

# we assume we run as root
cd $ROOTFS_PATH
# post-setup
createdir opt 700
createdir opt/grsvc 700
createdir opt/rootfs-setup 700
copy_supl postsetup.sh opt/rootfs-setup 500
# service files
copy_supl requirements.txt opt/grsvc 500
