#!/usr/bin/fish

function bail
  echo $argv[1] >&2
  exit 1
end

function run_check_error
  if ! $argv
    echo "  Error! Aborting operation."
    exit 1
  end
end

# checks
if ! source $CONFIG_FILE
  bail "Please set 'CONFIG_FILE' to a fish config variable."
end
if test (id -u) -ne 0
  bail "Run this script as root!"
end

# ensure all variables are set
set REQ_VARS BASEIMG_URL BASEIMG_FILENAME OUTPUT_FILENAME REPO_ADDR REPO_DISTRO REPO_COMPONENT LXC_PATH LXC_BASE_CONFIG_PATH BUSYBOX_INSTALLER_PATH SUPPLEMENTAL_PATH POSTSETUP_HOOK_PATH APT_INSTALL_PACKAGES PIP3_INSTALL_REQUIREMENTS READINESS_CHECK_PATH TZ
for VAR in $REQ_VARS
  if test -z "$$VAR"
    bail "Please set '"$VAR"' variable!"
  end
end

# set all path to canonical
set PATH_VARS BASEIMG_FILENAME OUTPUT_FILENAME LXC_PATH LXC_BASE_CONFIG_PATH BUSYBOX_INSTALLER_PATH SUPPLEMENTAL_PATH
for VAR in $PATH_VARS
  set -gx $VAR (realpath $$VAR)
end

# ensure working rootfs directory is empty
if test -e $LXC_PATH
  bail "Ensure that the LXC directory is nonexistent!"
end

# some constants
set -gx LXC_UTS_NAME working

# directories
set -gx CWD (pwd)
set -gx LXC_WORK_PATH $LXC_PATH/$LXC_UTS_NAME
set -gx ROOTFS_PATH $LXC_WORK_PATH/rootfs

if ! mkdir -p $ROOTFS_PATH
  bail "Failed to create working directory. Operation aborted."
end

# downloads here!
echo "Downloading base image ..."
if test -e $BASEIMG_FILENAME
  echo "Base image exists. Skipping download."
else
  run_check_error curl -o $BASEIMG_FILENAME $BASEIMG_URL
end

echo "Downloading busybox ..."
if test -e $BUSYBOX_INSTALLER_PATH/busybox
  echo "Busybox binary exists. Skipping download."
else
  run_check_error curl -o $BUSYBOX_INSTALLER_PATH/busybox $BUSYBOX_URL
end

echo "Downloading busybox installer ..."
if test -e $BUSYBOX_INSTALLER_PATH/busybox-installer.sh
  echo "Busybox installer script exists. Skipping download."
else
  run_check_error curl -o $BUSYBOX_INSTALLER_PATH/busybox-installer.sh $BUSYBOX_INSTALLER_URL
  run_check_error chmod a+x $BUSYBOX_INSTALLER_PATH/busybox-installer.sh
end

# we should have everything by now. Let's get down to business!
echo "Extracting base image ..."
cd $ROOTFS_PATH
run_check_error tar -xf $BASEIMG_FILENAME

if ! test -z $APT_PROXY
  echo "Setting APT proxy to base image ..."
  echo 'Acquire::http::Proxy "'$APT_PROXY'";' > etc/apt/apt.conf.d/10proxy
end

echo "Setting Ubuntu repo ..."
set -gx APT_SRC_RELDIR etc/apt/sources.list
rm $APT_SRC_RELDIR
for DISTRO in $REPO_DISTRO
  echo deb $REPO_ADDR $DISTRO $REPO_COMPONENT >> $APT_SRC_RELDIR
end

echo "Installing busybox ..."
cd $BUSYBOX_INSTALLER_PATH
run_check_error ./busybox-installer.sh $ROOTFS_PATH

echo "Writing LXC configuration ..."
cd $CWD
set -gx LXC_CONFIG_PATH $LXC_WORK_PATH/config
cp $LXC_BASE_CONFIG_PATH $LXC_CONFIG_PATH
echo "lxc.uts.name =" $LXC_UTS_NAME >> $LXC_CONFIG_PATH
echo "lxc.rootfs.path =" $ROOTFS_PATH >> $LXC_CONFIG_PATH

echo "Starting container for configuration ..."
run_check_error lxc-start -P $LXC_PATH -n $LXC_UTS_NAME

# functions to run command inside container
function run_in_container
  lxc-attach -P $LXC_PATH -n $LXC_UTS_NAME -- $argv
end

echo "Waiting for container to ready ..."
while ! run_in_container cat $READINESS_CHECK_PATH
  sleep 1
end

echo "Updating APT ..."
run_check_error run_in_container apt-get update

echo "Installing APT packages ..."
set -x DEBIAN_FRONTEND noninteractive
run_check_error run_in_container apt-get -y install --no-install-recommends $APT_INSTALL_PACKAGES

echo "Running supplemental script ..."
cd $CWD
run_check_error $SUPPLEMENTAL_PATH

echo "Installing pip3 dependencies ..."
run_check_error run_in_container pip3 install -r $PIP3_INSTALL_REQUIREMENTS

echo "Running postsetup hook ..."
run_check_error run_in_container $POSTSETUP_HOOK_PATH

echo "Cleaning APT ..."
run_check_error run_in_container apt clean
run_check_error rm -rf $ROOTFS_PATH/var/lib/apt/lists/*

echo "Stopping container ..."
lxc-stop -P $LXC_PATH -n $LXC_UTS_NAME

echo "Creating rootfs archive ..."
cd $ROOTFS_PATH
tar -c * | pv | xz -T 0 > $OUTPUT_FILENAME

echo "Done! The rootfs is ready to be imported into Docker!"
echo $OUTPUT_FILENAME
