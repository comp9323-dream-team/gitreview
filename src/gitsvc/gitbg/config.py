import os
from util import truthy_string

TASK_ID_SIZE=32

# need to load these configs from another source (env. variable)
# all of these have to be defined in the env, prefixed with "GR_"
# tuple (name, type/converter, (optional)default value)
_vars=[
  ("QUEUE_STORE_PATH", str),
  ("BASE_STORE_DIR", str),
  ("QUEUE_REQ_SOCKET", str),
  ("WORKER_COUNT", int, 1),
  ("REQ_BACKLOG", int, 2),
  ("UNLINK_SOCK_ON_START", truthy_string, True)
]

for var in _vars:
  varname = var[0]
  vartype = var[1]
  default_val = None if len(var) < 3 else var[2]
  varval = os.environ.get(f"GRBG_{varname}", default_val)
  if varval is None:
    raise KeyError(f"{varname} not set in environment variable!")
  varval = vartype(varval)
  globals()[varname] = varval
  