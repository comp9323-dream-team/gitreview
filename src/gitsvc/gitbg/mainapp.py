import signal
import os
import socket
import select
import sys
import errno
from workermgr import Workers

import config
import util

class MainApp:
  def __init__(self):
    # declare variables
    self._workers = None
    self._req_exit = False
    self._fd_wait_list = None
    self._signal_fd = None
    self._mainsock = None
    self._sighandlers = {
      signal.SIGTTIN: self._handle_worker_mgmt,
      signal.SIGTTOU: self._handle_worker_mgmt,
      signal.SIGQUIT: self._handle_exit,
      signal.SIGTERM: self._handle_exit,
      signal.SIGABRT: self._handle_exit,
      signal.SIGINT : self._handle_exit,
      signal.SIGCHLD: self._handle_sigchld
    }

  @staticmethod
  def _check_config():
    for check_positive in (config.WORKER_COUNT, config.REQ_BACKLOG):
      if check_positive < 1:
        raise ValueError("Must be positive integer.")

    if config.TASK_ID_SIZE % 2 != 0:
      raise ValueError("Task ID size must be even")

    # check persistence path
    persistence_path = os.path.join(config.QUEUE_STORE_PATH, "test")
    try:
      with open(persistence_path, "wb") as f:
        pass
      os.unlink(persistence_path)
    except:
      raise RuntimeError("Error accessing QUEUE_STORE_PATH.")

  def _handle_signal_fd(self, fd):
    while not self._req_exit:
      try:
        sig = os.read(fd, 1)
        if len(sig) == 0:
          break
        sig = sig[0]
        if sig in self._sighandlers:
          self._sighandlers[sig](sig)
      except BlockingIOError:
        return

  def _handle_exit(self, sig):
    self._req_exit = True

  def _handle_worker_mgmt(self, sig):
    # let the loop regenerate this list
    self._fd_wait_list = None
    if sig == signal.SIGTTIN:
      self._workers.add_worker()
    elif sig == signal.SIGTTOU:
      self._workers.reduce_worker()

  def _handle_sigchld(self, sig):
    # reap all child
    while True:
      try:
        pid, _ = os.waitpid(-1, os.WNOHANG)
        if pid == 0:
          break
        self._workers.request_waitpid(pid, True)
      except:
        break
    # respawn new child
    if len(self._workers) < config.WORKER_COUNT:
      self._workers.add_worker()

  def _handle_mainsock_incoming(self, fd):
    # NOTE: input are not sanitised here! Sending different from protocol is a DoS
    # as the server might be blocked forever!
    # do blocking read!
    connsock, _ = self._mainsock.accept()
    # 2 byte data size machine endian
    readsize = util.sock_recv_all(connsock, 2)
    if readsize is None:
      print("request corrupted!")
      return
    readsize = int.from_bytes(readsize, byteorder=sys.byteorder, signed=False)
    # data
    reqdata = util.sock_recv_all(connsock, readsize)
    if reqdata is None:
      print("request corrupted!")
      return
    self._workers.enqueue_job(reqdata)
    connsock.close()
  
  def run(self):
    MainApp._check_config()
    self._workers = Workers()

    # ignore these signals: we will use the wakeup_fd to handle the signals
    # it is because it is difficult to design a reentrant function
    # WARNING! if to many signals are coming, pipe buffers might be full,
    # thus the remaining signals will be lost. Buffer size should be able to 
    # handle 4096 in-flight signals tho.
    for sig in self._sighandlers.keys():
      signal.signal(sig, lambda sig,frame: True)

    # wake select on signal
    self._signal_fd = os.pipe2(os.O_NONBLOCK)
    signal.set_wakeup_fd(self._signal_fd[1])

    # initiate UDS for receiving requests
    self._mainsock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM | socket.SOCK_NONBLOCK)
    try:
      self._mainsock.bind(config.QUEUE_REQ_SOCKET)
    except OSError as ex:
      if ex.errno == errno.EADDRINUSE:
        if config.UNLINK_SOCK_ON_START:
          print("UNLINK_SOCK_ON_START is configured. Removing the socket file and try rebinding.")
          os.unlink(config.QUEUE_REQ_SOCKET)
          self._mainsock.bind(config.QUEUE_REQ_SOCKET)
        else:
          raise ex
    self._mainsock.listen(config.REQ_BACKLOG)

    # create initial workers
    for i in range(config.WORKER_COUNT):
      self._workers.add_worker()

    # handlers for sockets
    fdhandlers = None

    # load from persistent store (e.g. after server restart)
    self._workers.enqueue_from_persistence()

    # event loop!
    while True:
      if self._req_exit:
        os.unlink(config.QUEUE_REQ_SOCKET)
        self._workers.request_exit()
        exit()

      self._workers.process_jobs()

      if self._workers.worker_count_changed():
        fd_wait_list = None

      if fd_wait_list is None:
        fd_wait_list = [self._signal_fd[0], self._mainsock.fileno()]
        fd_wait_list.extend(self._workers.get_wait_list())
        # refresh handler dispatcher lookup table
        fdhandlers = {
          self._signal_fd[0]: self._handle_signal_fd,
          self._mainsock.fileno(): self._handle_mainsock_incoming
        }
        for fd in self._workers.get_wait_list():
          fdhandlers[fd] = self._workers.process_worker_msg

      ready_list, _, _ = select.select(fd_wait_list, [], [])
      # process ready FDs
      for fd in ready_list:
        fdhandlers[fd](fd)
    
if __name__ == "__main__":
  app = MainApp()
  app.run()
