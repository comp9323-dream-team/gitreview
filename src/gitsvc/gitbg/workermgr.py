from posix import O_NONBLOCK
import signal
import os
import sys
import secrets
from collections import OrderedDict

from worker import Worker
import config
import util

def _try_kill(pid, signal):
  if pid is None:
    return
  try:
    os.kill(pid, signal)
  except:
    pass

def _new_id():
  while True:
    ret = secrets.token_hex(config.TASK_ID_SIZE // 2)
    try:
      # xb = exclusive creation mode (fail if exists)
      f = open(os.path.join(config.QUEUE_STORE_PATH, ret), "xb")
    except:
      continue
    return (ret, f)

class WorkerInfo:
  pid = None
  wakepipes = None
  cmdpipes = None
  # file name to the job!
  activity = None

class Workers:
  def __init__(self):
    self.fd_map = {}
    self.pid_map = {}
    self.rridx = 0

    self.workers = []
    self.wait_list = []

    self.job_queue = OrderedDict()
    self.job_in_flight = OrderedDict()

    self._req_exit = False
    self._workers_change = False

  def enqueue_from_persistence(self):
    """Only to be called on first startup! Otherwise, jobs will be duplicated"""
    entries = os.scandir(config.QUEUE_STORE_PATH)
    items = [(e.name, e.stat().st_mtime) for e in entries if e.is_file() and (len(e.name) == config.TASK_ID_SIZE)]
    items.sort(key=lambda v: v[1])
    for key, _ in items:
      with open(os.path.join(config.QUEUE_STORE_PATH, key), "rb") as f:
        # all jobs are limited to 65k in size
        self.job_queue[key] = f.read(2**16 - 1)
    
  def add_worker(self):
    worker_info = WorkerInfo()
    # this is used for worker to signal us if they've finished
    worker_info.wakepipes = os.pipe2(0)
    # this is used for us to give worker a job
    worker_info.cmdpipes = os.pipe2(0)
    
    self.fd_map[worker_info.wakepipes[0]] = worker_info
    self._refresh_worker_lists()
    
    # let's create a child!
    pid = os.fork()
    if pid != 0:
      # parent. bookkeep then business as usual
      worker_info.pid = pid
      self.pid_map[pid] = worker_info
      self.process_jobs()
    else:
      # child. start the worker's event loop
      worker = Worker()
      worker.run(worker_info)
      exit()

  def reduce_worker(self):
    to_delete = self._select_worker_round_robin(lambda worker: worker.activity is None)
    if to_delete is not None:
      self._remove_worker(to_delete)

  def enqueue_job(self, data):
    """data is a byte array"""
    # persist job to disk
    id, fh = _new_id()
    with fh:
      fh.write(data)
    # enqueue
    self.job_queue[id] = data

  def process_worker_msg(self, fd):
    # at the moment, worker sends either 0/1 indicating fail/success respectively.
    # we don't really care for the result. we just want to delete data from persistent storage.
    if len(os.read(fd, 1)) == 0:
      # broken pipe
      self.process_error_fd(fd)
    else:
      if fd in self.fd_map:
        worker = self.fd_map[fd]
        if worker.activity is not None:
          # assume activity is finished (regardless of success/fail. let caller figure it out.)
          os.unlink(os.path.join(config.QUEUE_STORE_PATH, worker.activity))
          del self.job_in_flight[worker.activity]
          worker.activity = None

  def process_error_fd(self, fd):
    # assume broken pipe: terminate the worker and create a new one
    if fd in self.fd_map:
      self._remove_worker(self.fd_map[fd])
    if len(self) < config.WORKER_COUNT:
      self.add_worker()

  def process_jobs(self):
    for id, _ in self.job_queue.items():
      job_data = self.job_queue[id]
      while True:
        worker = self._select_worker_round_robin(lambda worker: worker.activity is None)
        if worker is None:
          return
        # signal worker!
        try:
          util.fd_write_all(worker.cmdpipes[1], int.to_bytes(len(job_data), 2, sys.byteorder, signed=False))
          util.fd_write_all(worker.cmdpipes[1], job_data)
          # job submitted successfully
          worker.activity = id
          break
        except:
          # error writing. very likely broken pipe = worker stops responding
          # remove this worker and try again
          self._remove_worker(worker)
      # indicate that this job is now in flight
      self.job_in_flight[id] = job_data
      del self.job_queue[id]

  def get_wait_list(self):
    return self.wait_list

  def worker_count_changed(self):
    """Returns True if workers are mutated since last call."""
    if self._workers_change:
      self._workers_change = False
      return True
    return False

  def request_exit(self):
    if not self._req_exit:
      self._req_exit = True
      print("Waiting for all workers to finish ...")
      for worker in self.workers:
        _try_kill(worker.pid, signal.SIGTERM)
        os.waitpid(worker.pid, 0)

  def request_waitpid(self, pid, reaped):
    """
    Assume this function is only called by SIGCHLD handler,
    which means that a child is stopped. Otherwise, this will block!
    """
    # avoid race condition
    if self._req_exit:
      return
    if pid in self.pid_map:
      worker = self.pid_map[pid]
      # this is to avoid killing a process that happens to
      # have the same PID as old worker
      worker.pid = None
      del self.pid_map[pid]
      self._remove_worker(worker)
      # blocking waitpid
      if not reaped:
        os.waitpid(pid, 0)

  def _select_worker_round_robin(self, check_cb):
    wkcount = len(self.workers)
    for _ in range(wkcount):
      if self.rridx >= wkcount:
        self.rridx = 0
      else:
        self.rridx = (self.rridx + 1) % wkcount
      if check_cb(self.workers[self.rridx]):
        return self.workers[self.rridx]
    return None

  def _remove_worker(self, worker):
    if worker.wakepipes[0] in self.fd_map:
      if worker.activity is not None:
        # the job that the worker has is no longer in flight, but still queued
        # we shall re-enqueue this job to the last! (that's your fault for crashing our workers!)
        self.job_queue[worker.activity] = self.job_in_flight[worker.activity]
        del self.job_in_flight[worker.activity]
        worker.activity = None
      _try_kill(worker.pid, signal.SIGTERM)
      # we are not deleting the PID map because SIGCHLD handler will take care of it
      del self.fd_map[worker.wakepipes[0]]
      self._refresh_worker_lists()

  def _refresh_worker_lists(self):
    self._workers_change = True
    self.workers = list(self.fd_map.values())
    self.wait_list = list(self.fd_map.keys())

  def __len__(self):
    return len(self.fd_map)