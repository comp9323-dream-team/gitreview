import os
from dulwich.repo import Repo
from dulwich.client import SSHGitClient
from dulwich.errors import NotGitRepository
import json

from filelock import *
import pathutils

def handle_fetch_job(repoid):
  repoid = repoid.decode('ascii')
  print(f"New fetch job for {repoid}")
  exobj = None
  try:
    _handle_fetch_job(repoid)
  except Exception as ex:
    exobj = ex
  finally:
    # set the status file back to idle
    with open_file_with_lock(pathutils.get_fetch_status_path(repoid), True) as f:
      f.truncate()
      f.write(b"IDLE")
      if exobj is not None:
        f.write(str(exobj).encode('ascii'))
        raise exobj

def _handle_fetch_job(repoid):
  if not os.path.isdir(pathutils.get_repo_path(repoid)):
    return
  git_path = pathutils.get_repo_git_path(repoid)
  with open_file_with_lock(pathutils.get_git_lock_file_path(repoid), False) as f:
    # init repo?
    try:
      repo = Repo(git_path)
    except NotGitRepository:
      repo = Repo.init(git_path)

    # remote config
    with open_file_with_lock(pathutils.get_remote_setting_path(repoid), False, False) as f:
      rc = json.load(f)
    remote_path = rc["path"]
    remote_username, remote_host = rc["host"].split("@", 1)

    # SSH client!
    client = SSHGitClient(remote_host, username=remote_username, 
      key_filename=pathutils.get_remote_private_key_path(repoid))

    # fetch all for now (determine_wants=None)
    fetch_result = client.fetch(remote_path, repo)
    # update heads of each references
    for key,value in fetch_result.refs.items():
      repo.refs[key] = value