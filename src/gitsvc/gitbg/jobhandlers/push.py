import json
import os
from dulwich.errors import SendPackError
from dulwich.client import SSHGitClient
from dulwich.porcelain import check_diverged, open_repo_closing, parse_reftuples
from dulwich.protocol import ZERO_SHA
from dulwich.repo import Repo

from filelock import *
import pathutils

def handle_push_job(repoid):
  repoid = repoid.decode('ascii')
  print(f"New push job for {repoid}")
  exmsg = None
  exobj = None
  try:
    _handle_push_job(repoid)
  except Exception as ex:
    exmsg = str(ex)
    exobj = ex
  finally:
    # set the status file back to idle
    with open_file_with_lock(pathutils.get_push_status_path(repoid), True, True) as f:
      _rewrite_queue(f, kind=b"IDLE", status=exmsg)
    if exobj is not None:
      raise exobj

def _handle_push_job(repoid):
  if not os.path.isdir(pathutils.get_repo_path(repoid)):
    return

  # this lock is to prevent another worker for push/fetch
  with open_file_with_lock(pathutils.get_git_lock_file_path(repoid), False) as masterlock:
    while True:
      with open_file_with_lock(pathutils.get_push_status_path(repoid), True, True) as statf:
        data = statf.read(4)
        if data in (b'PEND', b'BUSY'):
          if data == b'PEND':
            # indicates that we are doing processing
            _rewrite_queue(statf, kind=b'BUSY')
        elif data == b'IDLE':
          break
        else:
          raise ValueError("Push job queue data corrupted!")

        # read the first queue
        statf.seek(5, 0)
        queuespec = statf.readline()
        if len(queuespec) <= 1:
          # considered last (e.g. we don't support empty line)
          break

        refspec = queuespec[:-1] if queuespec == b'\n' else queuespec

      # TODO: exit the queue lock, and do the job
      # remote config
      with open(pathutils.get_remote_setting_path(repoid), "r") as f:
        rc = json.load(f)
      remote_path = rc["path"]
      remote_username, remote_host = rc["host"].split("@", 1)

      # SSH client
      client = SSHGitClient(remote_host, username=remote_username, 
        key_filename=pathutils.get_remote_private_key_path(repoid))

      repo = Repo(pathutils.get_repo_git_path(repoid))
      try:
        _push(client, remote_path, repo, refspec)
      except Exception as ex:
        # fail
        with open_file_with_lock(pathutils.get_push_status_path(repoid), True, True) as statf:
          _rewrite_queue(statf, kind=b"IDLE", status=str(ex))
        raise ex

      # OK. dequeue.
      with open_file_with_lock(pathutils.get_push_status_path(repoid), True, True) as statf:
        _rewrite_queue(statf, dequeue=True)

def _rewrite_queue(f, dequeue=False, kind=None, status=None):
  if kind != b'IDLE' and status is not None:
    raise ValueError("Only able to set status if type is idle.")

  # these operations can be optimized!
  f.seek(0, 0)
  lines = f.read().split(b'\n')

  origkind = kind
  kind = lines[0] if kind is None else kind

  if lines[0] not in (b'IDLE', b'PEND', b'BUSY'):
    if origkind != b'IDLE':
      raise ValueError("Queue data corrupted.")
    lines = [b'IDLE', status if status is not None else b'']
  elif lines[0] == b'IDLE':
    # no status msg for other kind besides IDLE
    if kind != b'IDLE':
      del lines[1]
    else:
      lines[1] = status if status is not None else b''
  elif lines[0] != b'IDLE' and kind == b'IDLE':
    # conversely, IDLE always have a status line
    lines.insert(1, status if status is not None else b'')
  
  lines[0] = kind

  if dequeue:
    # 2: kind and status
    minels = 2 if kind == b'IDLE' else 1
    if len(lines) > minels:
      del lines[-1]

  f.seek(0, 0)
  f.truncate()
  f.write(b'\n'.join(lines))

def _push(client, remote_path, repo, refspec):
  # Open the repo
  with open_repo_closing(repo) as r:
    refspecs = [refspec]

    selected_refs = []
    remote_changed_refs = {}

    def update_refs(refs):
      selected_refs.extend(parse_reftuples(r.refs, refs, refspecs))
      new_refs = {}
      # TODO: Handle selected_refs == {None: None}
      for (lh, rh, force_ref) in selected_refs:
        if lh is None:
          new_refs[rh] = ZERO_SHA
          remote_changed_refs[rh] = None
        else:
          try:
            localsha = r.refs[lh]
          except KeyError:
            raise RuntimeError("No valid ref %s in local repository" % lh)
          if not force_ref and rh in refs:
            check_diverged(r, refs[rh], localsha)
          new_refs[rh] = localsha
          remote_changed_refs[rh] = localsha
      return new_refs

    remote_location = client.get_url(remote_path)
    try:
      result = client.send_pack(
        remote_path,
        update_refs,
        generate_pack_data=r.generate_pack_data
      )
    except SendPackError as e:
      raise RuntimeError("Push to " + remote_location + " failed -> " + e.args[0].decode())

    for ref, error in (result.ref_status or {}).items():
      if error is not None:
        raise RuntimeError((b"Push of ref %s failed: %s\n" % (ref, error.encode('utf8'))).decode('utf8'))
