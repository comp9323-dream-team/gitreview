import jobhandlers.fetch as handlefetch
import jobhandlers.push as handlepush

def do_job_dispatch(data):
  cmd, param = data.split(b" ", 1)
  if cmd == b"FETCH":
    handlefetch.handle_fetch_job(param)
  elif cmd == b"PUSH":
    handlepush.handle_push_job(param)
  else:
    print("Unknown job type!")
