import signal
import select
import os
import sys
from contextlib import contextmanager

import util
import job
import traceback

class Worker:
  def run(self, info):
    self._worker_info = info
    self._signal_pipe = os.pipe2(os.O_NONBLOCK)
    signal.set_wakeup_fd(self._signal_pipe[1])
    self._install_signal_handlers()
    self._req_stop = False
    self._wait_handlers = {
      self._signal_pipe[0]: self._handle_signal_wakeup,
      self._worker_info.cmdpipes[0]: self._handle_incoming_job
    }
    self._wait_fds = [fd for fd in self._wait_handlers.keys()]
    self._run_loop()

  def _handle_stop(self, sig, frame):
    self._req_stop = True

  def _install_signal_handlers(self):
    # we will use regular preemptive signal handlers here as the "handle_stop" function
    # is completely reentrant safe!
    signal.signal(signal.SIGTERM, self._handle_stop)
    signal.signal(signal.SIGINT, self._handle_stop)
    signal.signal(signal.SIGQUIT, self._handle_stop)
    signal.signal(signal.SIGABRT, self._handle_stop)

  def _handle_signal_wakeup(self):
    os.read(self._signal_pipe[0], 1)

  def _get_job_data(self):
    readsize = util.fd_read_all(self._worker_info.cmdpipes[0], 2)
    if readsize is None:
      print("request corrupted!")
      return None
    readsize = int.from_bytes(readsize, byteorder=sys.byteorder, signed=False)
    reqdata = util.fd_read_all(self._worker_info.cmdpipes[0], readsize)
    if reqdata is None:
      print("request corrupted!")
      return None
    return reqdata

  def _handle_incoming_job(self):
    data = self._get_job_data()
    if data is None:
      return
    with self._send_status():
      try:
        print(f"New BG job (size = {len(data)})")
        job.do_job_dispatch(data)
        print("Job success!")
      except:
        print(f"Error performing job!")
        traceback.print_exc()

  @contextmanager
  def _send_status(self):
    v = b'\x00'
    try:
      yield
      # OK
      v = b'\x01'
    except:
      # error = 0
      pass
    finally:
      pass
    os.write(self._worker_info.wakepipes[1], v)

  def _run_loop(self):
    while not self._req_stop:
      ready_list, _, _ = select.select(self._wait_fds, [], [])
      for fd in ready_list:
        if fd in self._wait_handlers:
          self._wait_handlers[fd]()
    