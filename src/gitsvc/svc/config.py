import os

# size in bytes of the ID
ID_SIZE=8

# need to load these configs from another source (env. variable)
# all of these have to be defined in the env, prefixed with "GR_"
# tuple (name, type/converter, (optional)default value)
_vars=[
  ("BG_REQ_SOCKET", str),
  ("BASE_STORE_DIR", str),
  ("MAX_ASYNC_REQ", int, 16),
  ("FETCH_REQ_DELAY", int, 60),
  # fetching heads is lightweight, so ideally set a lower limit here
  ("FETCH_REQ_HEADS", int, 5),
  ("COMMITER_EMAIL", str, "approval@gitreview")
]

for var in _vars:
  varname = var[0]
  vartype = var[1]
  default_val = None if len(var) < 3 else var[2]
  varval = os.environ.get(f"GRSVC_{varname}", default_val)
  if varval is None:
    raise KeyError(f"{varname} not set in environment variable!")
  varval = vartype(varval)
  globals()[varname] = varval
  