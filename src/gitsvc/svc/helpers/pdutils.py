"""Patch and diff utils"""
from os import remove
from unidiff.patch import PatchSet
import unidiff.constants as unidiffconst
from dulwich.objects import Blob
from flask import g
import re

from . import repoutils

# regular file w/ no execute permission
_DEFAULT_MODE=0o100644

def generate_change_tuples(store, tree, diff_file):
  """Use the returned result with 'commit_tree_changes' method."""
  ret = []
  # should be an utf8 string here
  ps = PatchSet(diff_file)
  for p in ps:
    ppath = p.path.encode('utf8')
    if p.is_added_file:
      # add object to store
      newblob = create_blob(extract_new_file_contents(p))
      store.add_object(newblob)
      newmode = parse_mode(p.patch_info, _DEFAULT_MODE)
      ret.append((ppath, newmode, newblob.sha().hexdigest().encode('ascii')))
    elif p.is_removed_file:
      ret.append((ppath, None, None))
    elif p.is_modified_file:
      te = repoutils.get_tree_entry(store, tree, ppath)
      if te is None:
        raise ValueError("Wrong path")
      oldblob = store[te.sha]
      newblob = create_blob(get_modified_file(oldblob, p))
      store.add_object(newblob)
      # use old file's mode
      ret.append((ppath, te.mode, newblob.sha().hexdigest().encode('ascii')))
  return ret

def parse_mode(patchinfo, default=None):
  if not hasattr(g, 're_new_file_mode'):
    g.re_new_file_mode = re.compile(r'new file mode [0-7]{6}')
  for l in patchinfo:
    mtch = g.re_new_file_mode.match(l)
    if mtch is not None:
      return int(mtch.group(0)[-6:], 8)
  return default

def extract_new_file_contents(patchedfile):
  if not patchedfile.is_added_file:
    raise ValueError("This function is for extracting the contents of added file only!")
  # this is not efficient at all!
  ret = []
  for h in patchedfile:
    for l in h:
      if l.line_type == unidiffconst.LINE_TYPE_ADDED:
        ret.append(l.value.encode('utf8'))
  return b''.join(ret)

def get_modified_file(blob, patchedfile):
  # huge memory usage and not efficient!
  lines = blob.splitlines()

  removed_lines = []
  def handle_removed_lines():
    for i, ln in enumerate(reversed(removed_lines)):
      if lines[ln] is not None:
        if i > 0:
          raise ValueError("Unbalanced line modification.")
        else:
          break
      del lines[ln]
    removed_lines.clear()

  for h in patchedfile:
    for l in h:
      # in a valid diff file, substract always appear before added if it is a changed line
      if l.line_type == unidiffconst.LINE_TYPE_ADDED:
        target_line = l.target_line_no - 1
        if lines[target_line] is None:
          # must be a modified line!
          lines[target_line] = l.value.encode('utf8')
        else:
          lines.insert(target_line, l.value.encode('utf8'))
      elif l.line_type == unidiffconst.LINE_TYPE_REMOVED:
        source_line = l.source_line_no - 1
        lines[source_line] = None
        removed_lines.append(source_line)
      elif l.line_type == unidiffconst.LINE_TYPE_CONTEXT:
        handle_removed_lines()
        if lines[l.target_line_no - 1] != l.value.encode('utf8'):
          raise ValueError("Context mismatch")
    handle_removed_lines()
  return b''.join((l for l in lines if l is not None))

def create_blob(content):
  ret = Blob()
  ret.data = content
  return ret