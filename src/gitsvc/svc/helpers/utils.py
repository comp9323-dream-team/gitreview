import secrets
import config
import os
import sys
import socket
import common.util as commonutil

class ValidationErrorListBuilder:
  def __init__(self, data=None):
    self._data = []
    self._ret = {"fields": self._data}
    if data is not None:
      self.extend(data)

  def get(self):
    return self._ret

  def add(self, field_name, reason):
    self._data.append({
      'name':field_name,
      'reason':reason
    })

  def extend(self, data):
    for item in data:
      self.add(item[0], item[1])

  def __len__(self):
    return len(self._data)

def default_str(v, default=""):
  return v if v is not None else default

def parse_int(v):
  try:
    return int(v)
  except:
    return None

def send_bg_job(data):
  """Except on error."""
  if len(data) > ((2**16)-1):
    raise ValueError("Requested data for background job is too big.")
  sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
  try:
    sock.connect(config.BG_REQ_SOCKET)
    size = len(data).to_bytes(2, sys.byteorder, signed=False)
    sock.sendall(size, 0)
    sock.sendall(data)
  finally:
    # never raises exception even when not open
    sock.close()
