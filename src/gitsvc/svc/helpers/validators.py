import email.utils as emailutil
import phonenumbers
import phonenumbers.phonenumberutil as phonenumberutil
import urllib.parse as urlparse
from flask_restx import abort
import sys
from flask import g
import re

import config
import helpers.utils as utils

def _add_veb(veb, field, reason):
  if veb is None:
    abort(403, f"Validation error on '{field}': {reason}")
  else:
    veb.add(field, reason)

def validate_id(v, field=None, veb=None):
  def errorise(msg):
    if field is None:
      abort(400, msg)
    veb.add(field, msg)

  r = utils.parse_int(v) if type(v) is not int else v
  if r is None:
    errorise("ID must be integer.")
  try:
    return r.to_bytes(config.ID_SIZE, sys.byteorder, signed=False).hex()
  except:
    errorise("Invalid ID size.")

def id_str_to_integer(v):
  """'v' must be string!"""
  if not hasattr(g, 're_id_valid'):
    g.re_id_valid = re.compile('^[a-f0-9]{16}$')
  if g.re_id_valid.match(v) is None:
    return None
  return int.from_bytes(bytes.fromhex(v), byteorder=sys.byteorder, signed=False)

def default_value(field, value, default, veb=None):
  if value is None:
    if default is not None:
      value = default
    else:
      _add_veb(veb, field, "This field is required.")
      return None
  return value

def validate_enum(field, value, enumtype, default=None, veb=None):
  value = default_value(field, value, default, veb)
  if value is None:
    return None
  
  if type(value) is not enumtype:
    try:
      value = enumtype(value)
    except:
      _add_veb(veb, field, "Unknown value.")
  
  return value