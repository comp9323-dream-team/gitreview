# credits: Hayden Smith

from flask_restx import abort
from flask import request

import helpers.utils as utils

def get_request_body():
  """Get request body as an object.
  Automatically generates appropriate error if Flask failed to interpret request as JSON
  """
  j = request.json
  if not j:
    abort(400, "Expected a JSON object. Make sure you've set the 'Content-Type' header to 'application/json'.")
  return j

def get_request_arg(arg, type=str, required=False, default=None):
  """Get the value of arg from request.args, converted it using `type`.
  
  - If arg is provided but could not be parsed by `type`, then a 400 error is thrown.
  - If requires is True and the arg is not provided, then a 400 error is thrown.
  - If required is False and the arg is not provided, then default is returned.
  """
  if arg not in request.args:
    if required:
      abort(400, "Expected '{}' query parameter".format(arg))
    else:
      return default
  else:
    try:
      return type(request.args[arg])
    except:
      abort(400, "Query parameter '{}' malformed".format(arg))

def dict_unpack(thedict, *args, **kargs):
  """Copy values from the given key inside a dict into a tuple. Also doing mass validation."""
  # arguments
  required = kargs.get("required", False)
  thetype = kargs.get("type", None)
  strdefault = kargs.get("strdefault", None)
  strlenmin = kargs.get("strlenmin", 0)
  strlenmax = kargs.get("strlenmax", float("inf"))
  strtrim = kargs.get("strtrim", None)

  # constants for this invocation
  typestrerr = None if thetype is None else f"Expected type '{thetype.__name__}'"

  veb = utils.ValidationErrorListBuilder()
  for arg in args:
    v = thedict.get(arg, None)
    # required fields?
    if required:
      if v is None:
        veb.add(arg, "Required field not provided.")
        continue
    # type match?
    if thetype is not None:
      if type(v) is not thetype:
        veb.add(arg, typestrerr)
        continue
    # convert null keys or other unknown types to string?
    if strdefault is not None:
      if type(v) is not str:
        thedict[arg] = strdefault
    # strlen? (if caller wanted to check strlen, we MUST assume that the values are all string)
    if (strlenmin > 0) or (strlenmax < float('inf')) or (strtrim is not None):
      if v is None:
        continue
      if type(v) is not str:
        veb.add(arg, "Not a string.")
        continue
      if strtrim:
        thedict[arg] = v.strip()
      if len(v) < strlenmin:
        veb.add(arg, f"At least {strlenmin} characters.")
        continue
      elif len(v) > strlenmax:
        veb.add(arg, f"At most {strlenmax} characters")
        continue

  if len(veb):
    abort(400, **veb.get())

  return tuple(thedict.get(arg, None) for arg in args)

def single_unpack(v):
  """Extract the first element of a tuple/array (with None check)."""
  if v is None:
    return None
  (ret,) = v
  return ret
  
def row_extract(rows, vcol, *keys):
  """Convert row into tuple. The first (index 0) column is assumed to be the key."""
  ret = {key:None for key in keys}
  for row in rows:
    if row[0] in ret:
      ret[row[0]] = row[1:] if vcol is None else row[vcol]
  return tuple((ret[key] for key in keys))