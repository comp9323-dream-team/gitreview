import common.pathutils as pathutils
import os
import queue
from flask_restx import abort
from dulwich.repo import Commit, Repo, Tree
from dulwich.errors import NotGitRepository

import models

def check_repo_exists(id):
  if not os.path.isdir(pathutils.get_repo_path(id)):
    abort(404, "Repository not found.")

def get_repo_object(id):
  try:
    return Repo(pathutils.get_repo_git_path(id))
  except NotGitRepository:
    abort(428, "Fetch the repository first!")

def get_tree_entry(store, tree, path):
  while True:
    splt = path.split(b'/', 1)
    if len(splt) > 1:
      found = False
      for item in tree.iteritems():
        if item.path == splt[0]:
          tree = store[item.sha]
          path = splt[1]
          found = True
          break
      if not found:
        break
    else:
      for item in tree.iteritems():
        if item.path == splt[0]:
          return item
      break
  return None

def get_tree_object_from_hash(store, hash):
  """Hash can be either a commit or tree"""
  if type(hash) is not bytes:
    hash = hash.encode('ascii')
  try:
    root = store[hash]
    if type(root) == Commit:
      hash = root.tree
      root = store[hash]
    if type(root) != Tree:
      abort(404, "Given hash is not a valid tree or commit.")
  except:
    abort(404, "Requested object not found in repository.")

  return root, hash

def commit_parents(repo, commithash, isparent=None):
  # not including itself
  ret = []
  
  # for BFS
  q = queue.Queue()

  # initial check
  if commithash not in repo:
    return None
  obj = repo[commithash]
  if type(obj) is not Commit:
    return None
  q.put(obj)

  # loop
  while not q.empty():
    obj = q.get()
    for parent in obj.parents:
      if parent in repo:
        pobj = repo[parent]
        if type(pobj) is Commit:
          # enqueue
          ret.append(parent)
          q.put(pobj)
          if parent == isparent:
            return True

  return ret if isparent is None else False

def get_mode_type(mode):
  mode = (mode >> 12) & 0xF
  if mode == 8:
    return models.TreeItemType.FILE
  elif mode == 10:
    return models.TreeItemType.LINK
  elif mode == 4:
    return models.TreeItemType.TREE
  else:
    return models.TreeItemType.UNKNOWN
