import os
from flask_restx import abort

import helpers.validators as validators
import helpers.repoutils as repoutils
import common.pathutils as pathutils

def basic_rfr_check(repoid, rfrid):
  repoid = validators.validate_id(repoid)
  repoutils.check_repo_exists(repoid)
  rfrid = validators.validate_id(rfrid)
  rfrmeta = pathutils.get_rfr_meta(repoid, rfrid)
  try:
    os.stat(rfrmeta)
  except FileNotFoundError:
    abort(404, "RFR not found.")
  return repoid, rfrid, rfrmeta
