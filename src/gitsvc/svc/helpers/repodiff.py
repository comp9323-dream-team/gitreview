from dulwich.repo import Repo, Commit, Tree, Blob
import dulwich.diff_tree as dt
from subprocess import Popen, PIPE
import difflib

import models

def find_tree_diff(store, id1, id2, mark_unchanged=False):
  ids = [id1, id2]
  for idx, sha in enumerate(ids):
    if sha is None:
      continue
    obj = store[sha]
    if type(obj) is Commit:
      ids[idx] = obj.tree
    elif type(obj) is not Tree:
      raise ValueError("Not a tree or commit.")
  id1, id2 = ids

  CHANGE_MAP = {
    dt.CHANGE_ADD: models.ChangeType.ADD.value,
    dt.CHANGE_DELETE: models.ChangeType.REMOVE.value,
    dt.CHANGE_UNCHANGED: models.ChangeType.NONE.value,
    dt.CHANGE_MODIFY: models.ChangeType.MODIFY.value
  }

  # for signle ids (e.g. either of them is None), if enabled = mark as no change
  # instead of all delete or all addition
  if mark_unchanged:
    if id1 is not None and id2 is not None:
      raise ValueError("either id must be None for mark_unchanged.")
    CHANGE_MAP[dt.CHANGE_DELETE] = models.ChangeType.NONE.value
    CHANGE_MAP[dt.CHANGE_ADD] = models.ChangeType.NONE.value
  
  ret = []
  # parent stack
  ps = []
  # based on current model:
  # - modify = only contents are changed, name doesn't
  # - changes of file names, even if contents and hashes are identical, will result in delete + add
  # - this is done in DFS
  for ch in dt.tree_changes(store, id1, id2, want_unchanged=True, include_trees=True):
    # file name
    pth = ch.new.path if ch.type == dt.CHANGE_ADD else ch.old.path
    sha = ch.new.sha if ch.type == dt.CHANGE_ADD else ch.old.sha
    if pth == b'':
      continue
    pthcmp = pth.split(b'/')

    # follow tree_diff_item
    obj = {
      "name": pthcmp[-1].decode('utf8'),
      "change": CHANGE_MAP[ch.type],
    }

    # determine object type
    objref = store[sha]
    if type(objref) is Tree:
      obj['type'] = models.TreeItemType.TREE.value
      obj['items'] = []
    elif type(objref) is Blob:
      # no special indicator if the file is a link
      obj['type'] = models.TreeItemType.FILE.value
    else:
      ValueError("Only tree and file are supported.")

    if len(pthcmp) <= len(ps):
      # descending to parent
      while len(pthcmp) <= len(ps):
        ret = ps.pop()
    elif len(pthcmp) > (len(ps) + 1):
      # if we're here, we're viewing children of previous item
      # unlike descending which can happen several levels at once,
      # ascending is always one by one (unless we have bug in the traverser!)
      lastparent = ret[-1]['items']
      ps.append(ret)
      ret = lastparent

    ret.append(obj)
  
  # pop the stacks
  while len(ps) > 0:
    ret = ps.pop()
  
  return ret

def download_tree_patch(repopath, tree1, tree2):
  """Uses external Git app for this one!"""
  proc = Popen(["git", "diff-tree", "-p", tree1.decode('ascii'), tree2.decode('ascii')], cwd=repopath, stdout=PIPE)
  stdout, stderr = proc.communicate()
  if proc.wait() != 0:
    raise RuntimeError("Git cannot generate diff.")
  return stdout

def get_blob_diff(store, hash1, hash2):
  if hash1 is None and hash2 is None:
    raise ValueError("Nothing specified")
  elif hash1 is None or hash2 is None:
    ret = {"left": None, "right": None}
    if hash1 is not None:
      ret["left"] = get_blob_lines(store, hash1)
    elif hash2 is not None:
      ret["right"] = get_blob_lines(store, hash2)
    return ret

  obj1 = store[hash1].splitlines()
  obj2 = store[hash2].splitlines()

  ret1 = []
  ret2 = []
  
  sm = difflib.SequenceMatcher(a=obj1, b=obj2, autojunk=False)
  # operation, begin1, end1, begin2, end2
  for op, b1, e1, b2, e2 in sm.get_opcodes():
    if op == "insert":
      for i in range(b2, e2):
        ret1.append(None)
        ret2.append((models.ChangeType.ADD.value, obj2[i].decode('utf8')))
    elif op == "delete":
      for i in range(b1, e1):
        ret1.append((models.ChangeType.REMOVE.value, obj1[i].decode('utf8')))
        ret2.append(None)
    elif op == "equal":
      for i in range(e1 - b1):
        ret1.append((models.ChangeType.NONE.value, obj1[i + b1].decode('utf8')))
        ret2.append((models.ChangeType.NONE.value, obj2[i + b2].decode('utf8')))
    else:
      # modification. no. of lines might be different
      modes = [models.ChangeType.REMOVE.value, models.ChangeType.ADD.value]
      d1 = e1 - b1
      d2 = e2 - b2
      for i in range(max(d1, d2)):
        if (i < d1) and (i < d2):
          # both presents
          ret1.append((modes[0], obj1[i + b1].decode('utf8')))
          ret2.append((modes[1], obj2[i + b2].decode('utf8')))
        elif i < d1:
          # ret2 is null
          ret1.append((modes[0], obj1[i + b1].decode('utf8')))
          ret2.append(None)
        else:
          # ret1 is null
          ret1.append(None)
          ret2.append((modes[1], obj2[i + b2].decode('utf8')))
  
  return {"left": ret1, "right": ret2}

def get_blob_lines(store, hash):
  obj = store[hash].splitlines()
  return [(models.ChangeType.NONE.value, l.decode('utf8')) for l in obj]
