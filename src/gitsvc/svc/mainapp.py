from flask import Flask,request,jsonify,make_response,abort
from flask_restx import Api
from flask_cors import CORS
from werkzeug.exceptions import HTTPException
import config
import os
import stat

app = Flask(__name__)
# app configs
app.config['ERROR_404_HELP'] = False
# allow different domain to access this web service
CORS(app)
api = Api(app)

@app.errorhandler(HTTPException)
def handle_exception(e):
    """Return JSON instead of HTML for HTTP errors."""
    return make_response(jsonify({
        "message": e.description,
    }), e.code)

# check if base directory is there
if not os.path.isdir(config.BASE_STORE_DIR):
    raise RuntimeError("Base store directory does not exist!")
if not os.path.isdir(os.path.join(config.BASE_STORE_DIR, "repos")):
    raise RuntimeError("Base repo directory does not exist!")
if not stat.S_ISSOCK(os.stat(config.BG_REQ_SOCKET).st_mode):
    raise RuntimeError("BG path is not a socket!")

# import handlers (the one that has namespaces + route assignments) here
import handlers.repo
import handlers.repobrowse
import handlers.rfr
import handlers.rfrclose
import handlers.review
