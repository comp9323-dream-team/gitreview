"""
This module is for declaring data models used throughout the
backend application.
"""

from mainapp import api
from flask_restx import fields
from enum import Enum

repo_request = api.model('repo_request', {
  'id': fields.Integer(required=True)
})

repo_io_status = api.model('repo_io_status', {
  'busy': fields.Boolean(required=True),
  'allow_retry': fields.Integer(description='Allowed time in seconds to perform retry.'),
  'last_status': fields.String(description='If null, there is no status!'),
  'queue': fields.List(fields.String())
})

class RepoFetchRequestType(Enum):
  ALL_HEADS = "ALL_HEADS"
  HEADS = "HEADS"
  TREES = "TREES"
  COMMITS = "COMMITS"

repo_fetch_request = api.model('repo_fetch_request', {
  'action': fields.String(enum=[i.value for i in RepoFetchRequestType], required=True),
  'hashes': fields.List(fields.String())
})

class KeyType(Enum):
  OPENSSH_PRIVATE_KEY_V1 = "OPENSSH_PRIVATE_KEY_V1"
  PUBLIC_KEY_FP_SHA256 = "PUBLIC_KEY_FP_SHA256"

repo_remote_setting = api.model('repo_remote_setting', {
  'host': fields.String(required=True),
  'path': fields.String(required=True),
  'key': fields.String(required=True),
  'key_type': fields.String(required=True, enum=[i.value for i in KeyType])
})

repo_heads = api.model('repo_heads', {
  'heads': fields.Wildcard(fields.String(), description="Key/value pair of refspec:hash.")
})

class TreeItemType(Enum):
  UNKNOWN = "UNKNOWN"
  TREE = "TREE"
  FILE = "FILE"
  LINK = "LINK"

tree_content_item = api.model('tree_content_item', {
  'name': fields.String(required=True),
  'type': fields.String(required=True, enum=[i.value for i in TreeItemType])
})

browse_result = api.model('browse_result', {
  'type': fields.String(required=True, enum=[i.value for i in TreeItemType]),
  'tree': fields.List(fields.Nested(tree_content_item),
    description="Only available if return type is TREE"),
  'file': fields.String(
    description="Contents of the file/link (base64 encoded for either). Only available if return type is FILE.")
})

class RFRType(Enum):
  BASE_ONLY = "bo"
  TARGET_COMMIT = "tc"
  PATCHSET = "ps"

basic_rfr_info = api.model('basic_rfr_info', {
  'id': fields.Integer(required=True),
  'basecommit': fields.String(required=True, description="Hash of the base commit for this RFR.")
})

rfr_revision_req = api.model('rfr_revision_req', {
  'type': fields.String(required=True, enum=['tc','ps']),
  'data': fields.String(description="base64 data of git diff if 'ps'. Target commit if 'tc'.")
})

rfr_meta = api.model('rfr_meta', {
  'basecommit': fields.String(required=True, description="SHA base commit of the RFR."),
  'type': fields.String(required=True, enum=[i.value for i in RFRType]),
  'revisions': fields.Integer(required=True, description="Number of revisions of this RFR. 0 for zero revisions."),
  'closed': fields.Boolean(required=True),
  'refspec': fields.String(description="Refspec for closed patchset-RFRs.")
})

rfr_rev_meta = api.model('rfr_revision_req_result', {
  'revision': fields.Integer(required=True)
})

class ChangeType(Enum):
  NONE = "N" # no change
  ADD = "+"
  REMOVE = "-"
  MODIFY = "M"

tree_diff_item = api.model('tree_diff_item', {
  'name': fields.String(),
  'change': fields.String(required=True, enum=[i.value for i in ChangeType]),
  'type': fields.String(required=True, enum=[i.value for i in TreeItemType]),
  'items': fields.List(fields.Arbitrary(), description="List of tree_diff_item. Only available if type is TREE.")
})

push_request = api.model('push_request', {
  'branch_name': fields.String(description="Required only if merge is True, and RFR is of 'ps' type."),
  'merge': fields.Boolean()
})

line_model = api.model('line_model', {
  "line" : fields.Integer(),
  "content" : fields.String(description="Content of each line in the file.")
})

inline_review_file = api.model('inline_review_file', {
  'filename': fields.String(description="Name on the file"),
  'old': fields.Nested(line_model),
  'new' : fields.Nested(line_model)
})

inline_review_data = api.model('inline_review_data', {
  'inline_reviews' : fields.List(fields.Nested(inline_review_file))
})
