from helpers.validators import id_str_to_integer, validate_id
from handlers.repo import repoapi
from flask_restx import Resource, abort
from dulwich.refs import check_ref_format
from dulwich.objects import Commit
import json
from io import BytesIO
from time import time

import models
import config
from helpers.request_handlers import *
from helpers.rfrutils import basic_rfr_check
import common.pathutils as pathutils
import helpers.repoutils as repoutils
from common.filelock import lock_file, open_file_with_lock

@repoapi.route('/<repoid>/rfr/<rfrid>/close')
class RepoRFRClose(Resource):
  @repoapi.doc(description="Close an RFR")
  @repoapi.response(400, 'Invalid branch name.')
  @repoapi.response(403, 'RFR already closed.')
  @repoapi.response(404, 'Invalid branch name.')
  @repoapi.response(409, 'Given branch name already exists.')
  @repoapi.response(200, "Repository pushing status ('success' or 'enqueued').")
  @repoapi.expect(models.push_request)
  def post(self, repoid, rfrid):
    body = get_request_body()

    # first, check branch_name (only if specified)
    branch_name = body.get("branch_name", None)
    if branch_name is not None:
      branch_name = branch_name.strip()
      if len(branch_name) == 0:
        branch_name = None
    if branch_name is not None:
      if '/' in branch_name:
        abort(400, "'/' in branch name is not supported.")
      refspec = f'refs/heads/{branch_name}'.encode('ascii')
      if not check_ref_format(refspec):
        abort(400, "Invalid branch name")

    merge_required = bool(body.get("merge", False))

    repoid, rfrid, rfrmeta = basic_rfr_check(repoid, rfrid)

    # open push status with lock first before proceeding with locking RFR meta
    # this is to avoid deadlock
    with open_file_with_lock(pathutils.get_push_status_path(repoid), True, True) as statf:
      # we open the meta first to see if this is eligible
      with open(rfrmeta, "r+") as metaf:
        with lock_file(metaf.fileno(), True):
          metaobj = json.load(metaf)
          if metaobj.get('closed', False):
            abort(403, "This RFR is already closed.")

          metaobj['closed'] = True

          if metaobj['type'] == models.RFRType.PATCHSET.value and merge_required:
            if branch_name is None:
              abort(400, "branch_name is required for merging patchset RFR.")
            # check if branchname is in local
            repo = repoutils.get_repo_object(repoid)
            if refspec in set(repo.refs):
              abort(409, "Given branch name already exists.")
            try:
              # patchset RFR: read target tree
              with open(pathutils.get_rfr_revision_meta(repoid, rfrid, metaobj["revisions"]), "rb") as f:
                targettree = f.read()

              # create the commit
              c = Commit()
              c.tree = targettree
              c.parents = [metaobj['basecommit'].encode('ascii')]
              c.author = c.committer = f"GitReview <{config.COMMITER_EMAIL}>".encode('ascii')
              c.commit_time = c.author_time = int(time())
              c.commit_timezone = c.author_timezone = 0
              c.encoding = b"UTF-8"
              c.message = f"GitReview patchset approval for RFR {rfrid}".encode("ascii")

              # add commit and refspec
              repo.object_store.add_object(c)
              repo.refs[refspec] = c.sha().hexdigest().encode('ascii')

            except Exception as ex:
              abort(500, str(ex))

            metaobj['refspec'] = refspec.decode('ascii')
          else:
            merge_required = False

          # update meta
          metaf.seek(0, 0)
          metaf.truncate(0)
          json.dump(metaobj, metaf)

          # close immediately if base only / target commit type / cancelled patchset
          if not merge_required:
            return "success"

      rfrid = rfrid.encode('ascii')
      try:
        # at this point, we will be submitting patchset RFR. Check if this RFR is enqueued already
        pushstat = statf.read().split(b'\n')
        if pushstat[0] in (b'BUSY', b'PEND'):
          for queued_rfr in pushstat[1:]:
            if queued_rfr == refspec:
              return "enqueued"

        # let's enqueue ...
        if pushstat[0] == b'IDLE':
          # entry 1 always status message (can be empty)
          del pushstat[1]
          pushstat[0] = b'PEND'
        pushstat.append(refspec)
        statf.seek(0, 0)
        statf.truncate(0)
        statf.write(b'\n'.join(pushstat))
      finally:
        # e.g. job might be submitted when the bgsvc was down
        # bgsvc is responsible for setting this from PEND to BUSY
        if pushstat[0] == b'PEND':
          _submit_push_job(repoid)
      return "enqueued"

@repoapi.doc(description="Get the push status and queue of the repo.")
@repoapi.route('/<repoid>/pushstatus')
class RepoRFRCloseStatus(Resource):
  @repoapi.response(200, 'Repository pushing status.', models.repo_io_status)
  def get(self, repoid):
    repoid = validate_id(repoid)
    repoutils.check_repo_exists(repoid)

    ret = {
      'busy': False
    }

    with open(pathutils.get_push_status_path(repoid), "rb") as f:
      kind = f.read(4)
      # read first \n
      f.read(1)

      if kind == b'IDLE':
        rem = f.readline()
        if len(rem) > 1:
          # error message from background worker
          if rem[-1] == b'\n':
            rem = rem[:-1]
          ret['last_status'] = rem.decode('utf8')
        return ret

      if kind in (b'BUSY', b'PEND'):
        data = f.read().split(b'\n')
        # parse output
        q = [i.decode('ascii') for i in data]
        ret['queue'] = q

        # try re-request to bg
        if kind == b'PEND':
          if _submit_push_job(repoid):
            ret['busy'] = True
          else:
            ret['last_status'] = 'Service failed to enqueue push jobs.'
        else:
          ret['busy'] = True

        return ret
      else:
        abort(500, 'Queue data error.')

def _submit_push_job(repoid):
  data = BytesIO()
  data.write(b"PUSH ")
  data.write(repoid.encode('ascii'))
  try:
    utils.send_bg_job(data.getvalue())
    return True
  except Exception as ex:
    print(f"Error submitting push job: {str(ex)}")
    return False
