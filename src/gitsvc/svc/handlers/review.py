from flask.helpers import send_file
from handlers.repo import repoapi
from flask_restx import Resource, abort
from flask import send_file
import os
import json
from dulwich.objects import Blob

from common.filelock import open_file_with_lock
import common.pathutils as pathutils
import helpers.rfrutils as rfrutils
import helpers.repoutils as repoutils
import models
from helpers.request_handlers import *
import helpers.validators as validators

@repoapi.route('/<repoid>/rfr/<rfrid>/<rfrrev>/review')
class ReviewList(Resource):
  @repoapi.doc(description="List of reviews for a particular RFR revision.")
  @repoapi.response(200, "List of review IDs.")
  def get(self, repoid, rfrid, rfrrev):
    repoid, rfrid, _ = rfrutils.basic_rfr_check(repoid, rfrid)
    reviewdir = pathutils.get_review_dir(repoid, rfrid, rfrrev)
    if not os.path.isdir(reviewdir):
      abort(404, "RFR revision not found.")

    ret = []
    for i in os.listdir(reviewdir):
      idint = validators.id_str_to_integer(i)
      if idint is not None:
        ret.append(idint)
    return ret

@repoapi.route('/<repoid>/rfr/<rfrid>/<rfrrev>/review/<reviewid>')
class ReviewData(Resource):
  @repoapi.doc(description="Augment an existing review.")
  @repoapi.response(200, "Review content successfully augmented.")
  @repoapi.response(400, "Invalid request, review data not applicable for this RFR, or attempting to modify existing review comment.")
  @repoapi.response(403, "RFR revision cannot accept further augmentations.")
  @repoapi.expect(models.inline_review_data)
  def put(self, repoid, rfrid, rfrrev, reviewid):
    repoid, rfrid, _ = rfrutils.basic_rfr_check(repoid, rfrid)
    _check_review_allowed(repoid, rfrid, rfrrev)
    reviewid = validators.validate_id(reviewid)

    repo = repoutils.get_repo_object(repoid)
    body = get_request_body()
    old_tree, new_tree = _get_trees(repo, repoid, rfrid, rfrrev)

    try:
      body = _validate_sanitize_request_body(body, repo.object_store, old_tree, new_tree)
    except Exception as ex:
      abort(400, self._generic_errstr(ex))

    try:
      with open_file_with_lock(pathutils.get_review_data_path(repoid, rfrid, rfrrev, reviewid), True, False) as f:
        olddata = json.load(f)
        try:
          ret = _process_append_review(olddata, body)
          # OK
          f.seek(0, 0)
          f.truncate()
          json.dump(ret, f)
          return "success"
        except Exception as ex:
          abort(400, f"Review merge failed: {str(ex)}")
    except FileNotFoundError:
      abort(404, "RFR revision or review ID not found.")

  @repoapi.doc(description="Creates a new review.")
  @repoapi.expect(models.inline_review_data)
  @repoapi.response(200, "Review content successfully created.")
  @repoapi.response(400, "Invalid request or review data not applicable for this RFR.")
  @repoapi.response(403, "RFR revision cannot accept a review.")
  def post(self, repoid, rfrid, rfrrev, reviewid):
    repoid, rfrid, _ = rfrutils.basic_rfr_check(repoid, rfrid)
    _check_review_allowed(repoid, rfrid, rfrrev)
    reviewid = validators.validate_id(reviewid)

    repo = repoutils.get_repo_object(repoid)

    # for 400 errors
    errstr = None

    # check if the desired review ID does NOT exist
    try:
      with open(pathutils.get_review_data_path(repoid, rfrid, rfrrev, reviewid), "x") as f:
        body = get_request_body()
        old_tree, new_tree = _get_trees(repo, repoid, rfrid, rfrrev)
        try:
          body = _validate_sanitize_request_body(body, repo.object_store, old_tree, new_tree)
          # OK! just save
          json.dump(body, f)
          return "success"
        except Exception as ex:
          errstr = self._generic_errstr(ex)
    except FileNotFoundError:
      abort(404, "RFR revision not found.")
    except FileExistsError:
      abort(409, "Given review ID already exists for the RFR revision.")
    
    # at this point, must be 400 error.
    # unlink the created file and move on
    os.unlink(pathutils.get_review_data_path(repoid, rfrid, rfrrev, reviewid))
    abort(400, errstr)

  @repoapi.doc(description="Get the contents of the review.")
  @repoapi.response(200, "Review content for the given RFR revision.", models.inline_review_data)
  @repoapi.response(404, "Requested review data not found.")
  def get(self, repoid, rfrid, rfrrev, reviewid):
    repoid, rfrid, _ = rfrutils.basic_rfr_check(repoid, rfrid)
    reviewid = validators.validate_id(reviewid)

    path = pathutils.get_review_data_path(repoid, rfrid, rfrrev, reviewid)
    return send_file(path, "application/json")

  def _generic_errstr(self, ex):
    if type(ex) is KeyError:
      errstr = f"field not found: {str(ex)}"
    else:
      errstr = str(ex)
    return f"Invalid inline review data: {errstr}"

def _check_review_allowed(repoid, rfrid, rfrrev):
  """Check if RFR is closed os requested RFR rev is not the latest version"""
  with open(pathutils.get_rfr_meta(repoid, rfrid), "r") as f:
    rfrmeta = json.load(f)
    if rfrmeta.get("closed", False):
      abort(403, "RFR cannot accept further reviews as it is closed.")
    if int(rfrrev) != rfrmeta.get("revisions"):
      abort(403, f"Can only target latest version of RFR (currently {rfrmeta['revisions']})")

def _get_trees(repo, repoid, rfrid, rfrrev):
  rfrrev = int(rfrrev)

  left_tree = None
  right_tree = None
  with open(pathutils.get_rfr_meta(repoid, rfrid), "r") as f:
    rfrmeta = json.load(f)
    left_tree, _ = repoutils.get_tree_object_from_hash(repo.object_store, rfrmeta["basecommit"].encode('ascii'))
    # revision 0 implies base only
    if rfrrev > 0:
      with open(pathutils.get_rfr_revision_meta(repoid, rfrid, rfrrev), "rb") as f:
        right_tree, _ = repoutils.get_tree_object_from_hash(repo.object_store, f.read())
  
  return (left_tree, right_tree)

def _validate_sanitize_request_body(body, store, old_tree, new_tree):
  def get_max_line_count(tree, filename):
    if tree is None:
      return 0
    te = repoutils.get_tree_entry(store, tree, filename)
    if te is None:
      return 0
    obj = store[te.sha]
    if type(obj) is not Blob:
      return 0
    
    return len(obj.data.split(b'\n'))

  ret = []
  for fileobj in body['inline_reviews']:
    fileobj = {field:fileobj[field] for field in ["filename", "old", "new"]}
    max_line_old = get_max_line_count(old_tree, fileobj["filename"].encode('utf8'))
    max_line_new = get_max_line_count(new_tree, fileobj["filename"].encode('utf8'))
    if max_line_old == 0 and max_line_new == 0:
      raise ValueError(f"File '{fileobj['filename']}' cannot be resolved.")
    for k, ml in (("old", max_line_old), ("new", max_line_new)):
      linesdata = {}
      for idx in range(len(fileobj[k])):
        lineobj = fileobj[k][idx]
        lineno = int(lineobj["line"])
        if lineno < 0 or lineno >= ml:
          raise ValueError("Comment exceeded allowable line number.")
        if lineno in linesdata:
          raise ValueError("Duplicate comments detected.")
        linesdata[lineno] = lineobj["content"]
      fileobj[k] = [{
        "line": l,
        "content": linesdata[l]
      } for l in sorted(linesdata)]
    ret.append(fileobj)
  return {'inline_reviews': ret}

def _process_append_review(olddata, newdata):
  # oldir is the return
  oldirs = olddata['inline_reviews']
  newirs = newdata['inline_reviews']
  # build old filename dictionary
  oldfnmap = {i['filename']: i for i in oldirs}
  for newir in newirs:
    newfn = newir['filename']
    if newfn not in oldfnmap:
      oldirs.append(newir)
    else:
      oldir = oldfnmap[newfn]
      # check line by line
      for k in ("old", "new"):
        # build line number map
        oldlnmap = {i["line"]:i for i in oldir[k]}
        for lc in newir[k]:
          if lc["line"] in oldlnmap:
            raise ValueError(f"Changing comment is not allowed (on {k} '{newfn}' line {lc['line']}).")
          oldlnmap[lc["line"]] = lc
        # overwrite!
        oldir[k] = [oldlnmap[i] for i in sorted(oldlnmap)]

  return olddata
