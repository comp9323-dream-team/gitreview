from handlers.repo import repoapi
from flask_restx import Resource, abort
from dulwich.repo import Repo, Commit, Tree, Blob
from dulwich.errors import NotGitRepository
from flask import make_response

import models
from helpers.request_handlers import *
import common.pathutils as pathutils
import helpers.repoutils as repoutils
import helpers.validators as validators
import helpers.repodiff as repodiff
import base64

@repoapi.route('/<repoid>/heads')
class RepoHeads(Resource):
  @repoapi.doc(description="Get list of heads (refspecs, branches, tags) and their referenced commit hashes.")
  @repoapi.response(200, "List of references or heads and their hash values.", models.repo_heads)
  @repoapi.response(404, "Repository not found.")
  @repoapi.response(428, "Fetch the repository first!")
  def get(self, repoid):
    id = repoid
    id = validators.validate_id(id)
    repoutils.check_repo_exists(id)

    repo = repoutils.get_repo_object(id)

    try:
      ret = {k.decode('utf8'):repo.refs[k].decode('ascii') for k in repo.refs}
    except KeyError:
      abort(428, "Previous fetch failed. Please check your private key settings.")
    return {"heads": ret}

@repoapi.route('/<repoid>/parents/<hash>')
class CommitParents(Resource):
  @repoapi.doc(description="Get list of parents for the given commit hash.")
  @repoapi.response(200, "List of parents, ordered by closest parent.")
  @repoapi.response(404, "Repository or commit hash not found.")
  def get(self, repoid, hash):
    repoid = validators.validate_id(repoid)
    repoutils.check_repo_exists(repoid)
    repo = repoutils.get_repo_object(repoid)
    ret = repoutils.commit_parents(repo, hash.encode('ascii'))
    if ret is None:
      abort(404, "Commit not found in this repository.")
    return [i.decode('ascii') for i in ret]

@repoapi.route('/<repoid>/browse/<hash>')
class RepoBrowse(Resource):
  @repoapi.doc(description="Get tree (directory) structure of the given tree or commit hash.")
  @repoapi.response(200, "Tree structure of the given tree or commit hash.", models.tree_diff_item)
  @repoapi.response(404, "Repository, hash, or path is not found.")
  @repoapi.response(428, "Fetch the repository first!")
  def get(self, repoid, hash, path=""):
    id = repoid
    id = validators.validate_id(id)
    repoutils.check_repo_exists(id)
    repo = repoutils.get_repo_object(id)
    _, hash = repoutils.get_tree_object_from_hash(repo.object_store, hash)
    return repodiff.find_tree_diff(repo.object_store, hash, None, mark_unchanged=True)

@repoapi.route('/<repoid>/download/<hash>/<path:path>')
class RepoDownload(Resource):
  @repoapi.doc(description="Download the raw file from the given commit or tree hash and path.")
  @repoapi.response(200, "Contents of the file in the given path and hash.")
  @repoapi.response(404, "Given repo, hash, or path not found.")
  @repoapi.produces(['application/octet-stream'])
  def get(self, repoid, hash, path):
    id = repoid
    id = validators.validate_id(id)
    repoutils.check_repo_exists(id)
    repo = repoutils.get_repo_object(id)
    root, _ = repoutils.get_tree_object_from_hash(repo.object_store, hash)
    
    obj, _ = _get_blob_common(repo.object_store, root, path)
    resp = make_response(obj.data)
    resp.headers.add('Content-Type', 'application/octet-stream')
    return resp

@repoapi.route('/<repoid>/lines/<hash>/<path:path>')
class RepoDownloadLines(Resource):
  @repoapi.doc(description="Get the file contents in the tree or commit hash and path in line-by-line format.")
  @repoapi.response(200, "Contents of the file in the given path and hash, in list of diff line format.")
  @repoapi.response(404, "Given repo, hash, or path not found.")
  @repoapi.response(415, "Attempting to retreive binary file, which is not supported.")
  def get(self, repoid, hash, path):
    id = repoid
    id = validators.validate_id(id)
    repoutils.check_repo_exists(id)
    repo = repoutils.get_repo_object(id)
    root, _ = repoutils.get_tree_object_from_hash(repo.object_store, hash)

    _, objhash = _get_blob_common(repo.object_store, root, path)
    try:
      return repodiff.get_blob_lines(repo.object_store, objhash)
    except UnicodeDecodeError:
      abort(415, "Binary files not supported.")

def _get_blob_common(store, tree, path):
  try:
    te = repoutils.get_tree_entry(store, tree, path.encode('utf8'))
    if te is None:
      raise KeyError()
    obj = store[te.sha]
  except KeyError:
    abort(404, "Given path not found.")
  
  if type(obj) is not Blob:
    abort(400, "Target path is not a file.")
  
  return obj, te.sha
