from mainapp import api
from flask_restx import Resource, abort, reqparse, fields
from flask import request
import os
from io import BytesIO
from datetime import datetime, timedelta
import json
import openssh_key.private_key_list as pkl
import hashlib
import base64

import customworker
import models
import helpers.validators as validators
from helpers.request_handlers import *
import common.pathutils as pathutils
import helpers.repoutils as repoutils
from common.filelock import *
import config

repoapi = api.namespace('repo', description='Manage Git repositories on the system.')

@repoapi.route('')
class Repo(Resource):
  @repoapi.expect(models.repo_request)
  @repoapi.doc(description="Create a new empty repo with the given ID.")
  @repoapi.response(200, "Repository successfully created.")
  @repoapi.response(400, 'Invalid request data, format, or length.')
  @repoapi.response(409, 'Requested repo ID already exists.')
  def post(self):
    (id,) = dict_unpack(get_request_body(), "id", required=True)
    id = validators.validate_id(id)

    # directories
    repodirs = pathutils.get_repo_dirs(id)
    # first entry always main dir
    maindir = repodirs[0]
    if os.path.isdir(maindir):
      abort(409, "Repository with the ID already exists.")
    
    # create the required directories
    for dir in repodirs:
      os.mkdir(dir)
    
    # and required files
    fetch_status_file = pathutils.get_fetch_status_path(id)
    with open(fetch_status_file, "xb") as f:
      # default value is IDLE
      f.write(b"IDLE")
    # update time so that we won't complain later about throttling
    ts = (datetime.now() - timedelta(minutes=1)).timestamp()
    os.utime(fetch_status_file, times=(ts, ts))

    push_status_file = pathutils.get_push_status_path(id)
    with open(push_status_file, "xb") as f:
      # same as before, default is IDLE
      # but this time, there is no throttling!
      f.write(b"IDLE\n")

    # empty files
    for filename in [
      pathutils.get_remote_setting_path(id),
      pathutils.get_remote_private_key_path(id),
      pathutils.get_git_lock_file_path(id)
    ]:
      with open(filename, "xb"): pass
    
    return "success"

@repoapi.route('/<repoid>/remote')
class RepoRemote(Resource):
  @repoapi.expect(models.repo_remote_setting)
  @repoapi.doc(description="Set remote data of the repository. This includes SSH host, private key, and repository path.")
  @repoapi.response(200, "Remote successfully set.")
  @repoapi.response(404, 'Repository not found.')
  def post(self, repoid):
    id = repoid
    id = validators.validate_id(id)
    repoutils.check_repo_exists(id)
    
    # check host
    body = get_request_body()
    host, path, key, key_type = dict_unpack(body, "host", "path", "key", "key_type", required=True)
    if len(host) < 2:
      abort(400, "Invalid hostname.")
    host = host.encode('idna').decode('ascii')
    if '@' not in host:
      abort(400, 'Please add username on host.')
    if key_type != models.KeyType.OPENSSH_PRIVATE_KEY_V1.value:
      abort(400, "Unsupported key_type.")
    
    # sanitise key material
    if key[-1] != '\n':
      key = key + '\n'
    else:
      # how many newlines?
      for i in range(1, len(key)):
        if key[-i] != '\n': break
        if i >= 2:
          abort(400, "Extra newline in key material.")
    
    # check if key material is OK
    try:
      keypairs = pkl.PrivateKeyList.from_string(key, passphrase="")
    except:
      abort(400, "Invalid private key data. Private key must be in OpenSSH v1 format and not passphrase-protected.")
    if len(keypairs) < 1:
      abort(400, "Empty private key.")
    
    # store!
    with open_file_with_lock(pathutils.get_remote_setting_path(id), True, False) as f:
      f.truncate()
      json.dump({
        "host": host,
        "path": path
      }, f)
    with open_file_with_lock(pathutils.get_remote_private_key_path(id), True, False) as f:
      f.truncate()
      f.write(key)
      os.fchmod(f.fileno(), 0o600)
    
    return "success"

  @repoapi.doc(description="Get stored remote settings for this repository.")
  @repoapi.response(200, "Remote settings.", models.repo_remote_setting)
  @repoapi.response(404, 'Repository or remote settings not found.')
  def get(self, repoid):
    id = repoid
    id = validators.validate_id(id)
    repoutils.check_repo_exists(id)
    try:
      # no need for locking here
      with open(pathutils.get_remote_setting_path(id), "rb") as f:
        remoteinfo = json.load(f)
      with open(pathutils.get_remote_private_key_path(id), "r") as f:
        keypairs = pkl.PrivateKeyList.from_string(f.read())
      if len(keypairs) < 1:
        raise RuntimeError("No private key")
      ret = {
        "host": remoteinfo["host"],
        "path": remoteinfo["path"],
        "key": base64.b64encode(hashlib.sha256(keypairs[0].public.pack_public_bytes()).digest()).decode('ascii'),
        "key_type": models.KeyType.PUBLIC_KEY_FP_SHA256.value
      }
      return ret
    except:
      abort(404, "Remote key settings not found or corrupted.")

# TODO: merge and push operations must not be performed when fetching (and vice versa)
@repoapi.route('/<repoid>/fetch')
class RepoFetch(Resource):
  @repoapi.doc(description="Get the remote repository fetching status.")
  @repoapi.response(200, 'Repository fetching status.', models.repo_io_status)
  @repoapi.response(404, 'Repository not found.')
  def get(self, repoid):
    id = repoid
    id = self._check_repo(id)
    with open_file_with_lock(pathutils.get_fetch_status_path(id), False) as f:
      return self.check_status(f)

  @repoapi.doc(description="Submit a fetch job.")
  #@repoapi.expect(models.repo_fetch_request)
  @repoapi.response(200, 'Fetch task successfully submitted.')
  @repoapi.response(404, 'Repository or remote settings not found.')
  @repoapi.response(429, 'Fetch is busy or rate limit exceeded. Try again later.', models.repo_io_status)
  def post(self, repoid):
    # at the moment, we will do fetch all heads! these will be disabled for now.
    # body = get_request_body()
    # fetch_action = dict_unpack(body, "action", required=True)
    # fetch_hashes = dict_unpack(body, "hashes", required=False)
    # fetch_action = models.RepoFetchRequestType(fetch_action)

    # check remote
    id = repoid
    RepoRemote().get(id)

    id = self._check_repo(id)
    
    with open_file_with_lock(pathutils.get_fetch_status_path(id), True) as f:
      status = self.check_status(f)
      if status.get("busy", False) or status.get("allow_retry", None):
        abort(429, **status)
      self.submit_fetch_job(id, f)

    return "success"

  def _check_repo(self, id):
    id = validators.validate_id(id)
    repoutils.check_repo_exists(id)
    return id

  def check_status(self, f):
    """Please flock the file before calling this"""
    ret = {}
    ret["busy"] = self._is_fetch_busy(f)
    if not ret["busy"]:
      ret["allow_retry"] = self._check_throttle_allowance(f)
      ret["last_status"] = None
      # check for status if any.
      # the file pointer should be already at the correct position
      # after we did the check busy thing.
      status = f.read()
      # ignore single char status! (they might be stray \n or ' ')
      if len(status) > 1:
        ret["last_status"] = status.decode('ascii')
    return ret

  def submit_fetch_job(self, id, status_file):
    try:
      data = BytesIO()
      data.write(b"FETCH ")
      data.write(id.encode('ascii'))
      utils.send_bg_job(data.getvalue())
      # OK
      status_file.seek(0, 0)
      status_file.truncate()
      status_file.write(b"BUSY")
    except Exception as ex:
      # revert
      status_file.seek(0, 0)
      status_file.truncate()
      status_file.write(b"IDLE")
      status_file.write(str(ex).encode('utf8'))

  def _is_fetch_busy(self, f):
    f.seek(0, 0)
    return f.read(4) != b"IDLE"

  def _check_throttle_allowance(self, f):
    """Returns None if user can request fetch immediately. Otherwise remaining time in seconds."""
    r = datetime.now().timestamp() - os.fstat(f.fileno()).st_mtime
    r = int(config.FETCH_REQ_DELAY - r)
    return r if r > 0 else None
