from dulwich.object_store import commit_tree_changes
from handlers.repo import repoapi
from flask_restx import Resource, abort
from flask import make_response
from dulwich.repo import Repo, Commit
import os
import json
import fcntl
import base64

import models
import helpers.validators as validators
import helpers.repoutils as repoutils
import common.pathutils as pathutils
from common.filelock import lock_file, open_file_with_lock
from helpers.request_handlers import *
from helpers.utils import ValidationErrorListBuilder
from helpers.rfrutils import basic_rfr_check
import helpers.pdutils as pdutils
import helpers.repodiff as repodiff

@repoapi.route('/<repoid>/rfr')
class RepoRFR(Resource):
  @repoapi.doc(description="Creates a new RFR. Newly created RFRs are always 'base commit only' in type.")
  @repoapi.expect(models.basic_rfr_info)
  @repoapi.response(200, "RFR successfully created.")
  @repoapi.response(404, "Repo ID or basecommit not found.")
  def post(self, repoid):
    repoid = validators.validate_id(repoid)
    repoutils.check_repo_exists(repoid)

    # validate supplied fields
    rfrid, basecommit = dict_unpack(get_request_body(), 
      "id", "basecommit", required=True)
    rfrid = validators.validate_id(rfrid, "rfrid")
    
    # open repo
    repo = repoutils.get_repo_object(repoid)
    # check if basecommit is valid
    basecommitb = basecommit.encode('ascii')
    if not (basecommitb in repo and type(repo[basecommitb]) is Commit):
      abort(404, "basecommit not found in the specified repo.")
    # normalise head names
    basecommit = repo[basecommitb].sha().hexdigest()
    basecommitb = basecommit.encode('ascii')

    # create the RFR!
    rfrpath = pathutils.get_rfr_dir(repoid, rfrid)
    try:
      os.mkdir(rfrpath)
    except FileExistsError:
      abort(409, "RFR with the ID already exists.")
    metaobj = {
      "basecommit": basecommit,
      "type": models.RFRType.BASE_ONLY.value,
      "revisions": 0,
      "closed": False
    }
    with open(pathutils.get_rfr_meta(repoid, rfrid), "w") as f:
      with lock_file(f.fileno(), True):
        json.dump(metaobj, f)

    # revision 0 review folder
    os.mkdir(pathutils.get_rfr_revision_path(repoid, rfrid, 0))
    os.mkdir(pathutils.get_review_dir(repoid, rfrid, 0))
    
    return "success"

  @repoapi.doc(description="List of RFR IDs in this repo.")
  @repoapi.response(200, "List of RFRs")
  @repoapi.response(404, "Repo ID not found.")
  def get(self, repoid):
    repoid = validators.validate_id(repoid)
    repoutils.check_repo_exists(repoid)
    
    ret = []
    rfrpath = pathutils.get_repo_rfr_path(repoid)
    for de in os.scandir(rfrpath):
      if de.is_dir():
        idint = validators.id_str_to_integer(de.name)
        if idint is not None:
          ret.append(idint)
    
    return ret

@repoapi.route('/<repoid>/rfr/<rfrid>')
class RFR(Resource):
  @repoapi.doc(description="Submit an RFR revision.")
  @repoapi.expect(models.rfr_revision_req)
  @repoapi.response(200, "RFR revision successfully submitted.", models.rfr_rev_meta)
  @repoapi.response(400, "Bad request or invalid git diff file.")
  @repoapi.response(403, "RFR already closed.")
  @repoapi.response(404, "Repo or RFR not found.")
  @repoapi.response(409, "Resubmitting the same patch for 'tc'.")
  @repoapi.response(422, "(tc only) Target commit is not a child of the base commit or latest revision.")
  def post(self, repoid, rfrid):
    repoid, rfrid, rfrmeta = basic_rfr_check(repoid, rfrid)
    
    # open repo
    repo = repoutils.get_repo_object(repoid)

    # validate request
    rfrtype, rfrdata = dict_unpack(get_request_body(), "type", "data", required=True)
    rfrtype = validators.validate_enum("rfrtype", rfrtype, models.RFRType)

    # check/edit
    with open(rfrmeta, "r+") as f:
      with lock_file(f.fileno(), True):
        # load and truncate file
        metaobj = json.load(f)

        if metaobj.get('closed', False):
          abort(403, "This RFR is already closed.")

        # check type
        if metaobj['type'] == models.RFRType.BASE_ONLY.value:
          metaobj['type'] = rfrtype.value
        elif metaobj['type'] != rfrtype.value:
          abort(400, f"Invalid rfrtype. Expected '{metaobj['type']}'.")

        # check data
        rfrdata = rfrdata.encode('ascii')
        if rfrtype == models.RFRType.TARGET_COMMIT:
          # what is the target commit of the previous revision?
          if metaobj['revisions'] == 0:
            # no previous revision. ensure basecommit is the parent.
            prev_commit = metaobj['basecommit'].encode('ascii')
          else:
            # ensure current target commit is a child of the latest revision
            with open(pathutils.get_rfr_revision_meta(repoid, rfrid, metaobj['revisions']), "rb") as rmf:
              prev_commit = rmf.read()
          if not repoutils.commit_parents(repo, rfrdata, prev_commit):
            abort(422, "New target commit is not a child of the last revision.")
          newhash = rfrdata
        elif rfrtype == models.RFRType.PATCHSET:
          try: diff_file = base64.b64decode(rfrdata).decode('utf8')
          except: abort(400, "Invalid base64 encoding.")
          tree = repo[repo[metaobj['basecommit'].encode('ascii')].tree]
          try: changes = pdutils.generate_change_tuples(repo.object_store, tree, diff_file)
          except Exception as ex: abort(400, f"Error applying patchset: {str(ex)}")
          if len(changes) == 0:
            abort(400, "Invalid or empty patchset.")
          newtree = commit_tree_changes(repo.object_store, tree, changes)
          newhash = newtree.sha().hexdigest().encode('ascii')
          # ensure user not resubmitting the same patch :)
          for i in reversed(range(1, metaobj['revisions'] + 1)):
            with open(pathutils.get_rfr_revision_meta(repoid, rfrid, i), "rb") as rmfchk:
              prev_tree = rmfchk.read()
            if prev_tree == newhash:
              abort(409, "The given patch already submitted for this RFR.")

        # success!
        # rewrite RFR meta
        metaobj['revisions'] += 1
        newrev = metaobj['revisions']
        f.seek(0, 0)
        f.truncate(0)
        json.dump(metaobj, f)

    # make RFR folder
    os.mkdir(pathutils.get_rfr_revision_path(repoid, rfrid, newrev))
    # for review data
    os.mkdir(pathutils.get_review_dir(repoid, rfrid, newrev))

    # update metadata indicating there is a new revision
    with open(pathutils.get_rfr_revision_meta(repoid, rfrid, newrev), "wb") as f:
      f.write(newhash)

    return {"revision": newrev}

  @repoapi.doc(description="Get basic information of an RFR.")
  @repoapi.response(200, "RFR data", models.rfr_meta)
  def get(self, repoid, rfrid):
    _, _, rfrmeta = basic_rfr_check(repoid, rfrid)
    with open(rfrmeta, "r") as f:
      return json.load(f)

@repoapi.route('/<repoid>/rfr/<rfrid>/<rfrrev>/browse')
class RFRBrowse(Resource):
  @repoapi.doc(description="Get the differencing tree (directory) structure of a particular RFR revision.")
  @repoapi.response(200, "Difference tree structure of the given RFR revision.", models.tree_diff_item)
  def get(self, repoid, rfrid, rfrrev):
    # note: for 'bo' RFRs, use standard repo browser instead
    repoid, rfrid, rfrmeta = basic_rfr_check(repoid, rfrid)
    rfrrevmeta = pathutils.get_rfr_revision_meta(repoid, rfrid, rfrrev)
    metaobj, targethash = _rfrrev_commons(rfrmeta, rfrrevmeta)

    repo = Repo(pathutils.get_repo_git_path(repoid))
    objstore = repo.object_store
    
    if metaobj["type"] == models.RFRType.TARGET_COMMIT.value:
      # convert target commit hash to target tree hash
      targethash = repo[targethash].tree
    
    # convert base hash to tree
    basetree = repo[metaobj["basecommit"].encode('ascii')].tree

    ret = repodiff.find_tree_diff(objstore, basetree, targethash)
    return ret

@repoapi.route('/<repoid>/rfr/<rfrid>/<rfrrev>/download')
class RFRDownload(Resource):
  @repoapi.doc(description="Download the patch file of the RFR revision. This patch file is applicable to the RFR's base commit.")
  @repoapi.response(200, "The patch file of this RFR revision that can be applied to the base commit.")
  @repoapi.produces(["text/x-patch"])
  def get(self, repoid, rfrid, rfrrev):
    repoid, rfrid, rfrmeta = basic_rfr_check(repoid, rfrid)
    rfrrevmeta = pathutils.get_rfr_revision_meta(repoid, rfrid, rfrrev)
    metaobj, targethash = _rfrrev_commons(rfrmeta, rfrrevmeta)

    repo = Repo(pathutils.get_repo_git_path(repoid))
    tree_src = repo[metaobj['basecommit'].encode('ascii')].tree
    tree_dst = targethash if metaobj['type'] == models.RFRType.PATCHSET.value else repo[targethash].tree

    ret = repodiff.download_tree_patch(pathutils.get_repo_git_path(repoid), tree_src, tree_dst)
    resp = make_response(ret)
    resp.headers.set('Content-Type', 'text/x-patch')
    return resp
 
@repoapi.route('/<repoid>/rfr/<rfrid>/<rfrrev>/item/<path:path>')
class RFRGetRevItem(Resource):
  @repoapi.doc(description="Get the difference of the file from the given path between the RFR's base commit and this particular RFR revision.")
  @repoapi.response(200, "List of diff lines (left and right).")
  @repoapi.response(400, "Invalid request or path specification.")
  @repoapi.response(415, "Attempting to diff binary file.")
  def get(self, repoid, rfrid, rfrrev, path):
    repoid, rfrid, rfrmeta = basic_rfr_check(repoid, rfrid)
    rfrrevmeta = pathutils.get_rfr_revision_meta(repoid, rfrid, rfrrev)
    metaobj, targethash = _rfrrev_commons(rfrmeta, rfrrevmeta)
    path = path.encode('utf8')

    repo = Repo(pathutils.get_repo_git_path(repoid))
    objstor = repo.object_store

    if metaobj['type'] == models.RFRType.TARGET_COMMIT.value:
      targethash = repo[targethash].tree
    srchash = repo[metaobj['basecommit'].encode('ascii')].tree

    items = [srchash, targethash]
    for i,_ in enumerate(items):
      items[i] = repoutils.get_tree_entry(objstor, repo[items[i]], path)
      if items[i] is not None:
        if repoutils.get_mode_type(items[i].mode) != models.TreeItemType.FILE:
          abort(400, "Invalid path specification.")
        items[i] = items[i].sha
    srcitem, dstitem = items
    if srcitem is None and dstitem is None:
      abort(404, "Path not found")

    try:
      ret = repodiff.get_blob_diff(objstor, srcitem, dstitem)
    except UnicodeDecodeError:
      abort(415, "Binary files not supported.")
    return ret

def _rfrrev_commons(rfrmeta, rfrrevmeta):
  with open(rfrmeta, "r") as f:
    metaobj = json.load(f)
  try:
    with open(rfrrevmeta, "rb") as f:
      targethash = f.read()
  except FileNotFoundError:
    abort(404, "RFR revision not found.")
  return metaobj, targethash
