import signal
import gunicorn.workers.sync
import threading
import sys
import os
import select
import time
from contextlib import contextmanager

class WorkerDataDummy: pass
_worker = WorkerDataDummy()

def _notifier_thread():
  wait_fds = [_worker.pipe[0]]
  while _worker.instance.alive:
    _worker.instance.tmp.notify()
    select.select(wait_fds, [], [], _worker.timeout)

    # check request for kill
    if _worker.rk_target is not None:
      if time.monotonic() > _worker.rk_target:
        signal.pthread_kill(_worker.main_thread, signal.SIGUSR2)
        _worker.rk_target = None

@contextmanager
def request_kill(second):
  """If the requester didn't exit the context manager within the given time, we will raise an exception."""
  _worker.rk_target = int(time.monotonic()) + second
  try:
    yield
  finally:
    _worker.rk_target = None

def increase_rk(second):
  """Update request kill timer to the given second from now."""
  if _worker.rk_target is None:
    raise ValueError("Request kill is not presently set.")
  _worker.rk_target = int(time.monotonic()) + second

def handle_usr2(sig, frame):
  raise InterruptedError

class MyWorker(gunicorn.workers.sync.SyncWorker):
  def __init__(self, age, ppid, sockets, app, timeout, cfg, log):
    global _worker
    
    _worker.instance = self
    _worker.timeout = max(0.5, timeout)
    # rk = request kill
    # if not None, absolute monotonic time when USR2 will be fired
    _worker.rk_target = None
    _worker.main_thread = threading.get_ident()
    
    # modify timeout because we don't need to periodically notify main again
    timeout = 2**30
    
    super().__init__(age, ppid, sockets, app, timeout, cfg, log)

  def notify(self):
    """Blank notify function: the notifier thread will take care of this."""
    pass

  def init_signals(self):
    super().init_signals()

    signal.signal(signal.SIGUSR2, handle_usr2)
    signal.siginterrupt(signal.SIGUSR2, False)

    # initialise and start notifier thread
    _worker.pipe = self.PIPE
    notifier_thread = threading.Thread(target=_notifier_thread, name="Notifier Thread")
    notifier_thread.start()
    _worker.notifier_thread = notifier_thread.ident
