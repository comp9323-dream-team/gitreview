import socket
import io
import os

def sock_recv_all(sock, reqlen):
  return _read_all_stub(lambda rl: sock.recv(rl, socket.MSG_WAITALL), reqlen)

def fd_read_all(fd, reqlen):
  return _read_all_stub(lambda rl: os.read(fd, rl), reqlen)

def _read_all_stub(readfn, reqlen):
  recv = io.BytesIO()
  while reqlen > 0:
    data = readfn(reqlen)
    if len(data) == 0:
      # socket closed prematurely. assume data is gone.
      return None
    reqlen -= recv.write(data)
  return recv.getvalue()

# note: writing can be exceptional!
def fd_write_all(fd, data):
  """Note: this can raise exception! (e.g. broken pipe)"""
  _write_all_stub(data, lambda data: os.write(fd, data))

def _write_all_stub(data, writefn):
  written = 0
  while written < len(data):
    written += writefn(data if written == 0 else data[written:])

def truthy_string(val):
  val = str(val).lower().strip()
  if val == "true":
    return True
  # integer other than 0 == true. 0 is false
  try:
    intval = int(val)
    if intval != 0:
      return True
  except:
    pass
  # anything else (e.g. "correct", "alright", "false") = return false
  return False
