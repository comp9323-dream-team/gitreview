import os
import config

# folders
def get_repo_path(id):
  return os.path.join(config.BASE_STORE_DIR, "repos", id)

def get_repo_git_path(id):
  """This path is used to store git objects."""
  return os.path.join(get_repo_path(id), "git")

def get_repo_status_path(id):
  """This path is used to store locks, fetch/push statuses, etc."""
  return os.path.join(get_repo_path(id), "status")

def get_repo_meta_path(id):
  """This path is used to store metadata (e.g. remotes)"""
  return os.path.join(get_repo_path(id), "meta")

def get_repo_rfr_path(id):
  """This path is used to store RFRs"""
  return os.path.join(get_repo_path(id), "rfr")

# important files
def get_fetch_status_path(id):
  return os.path.join(get_repo_status_path(id), "fetch")

def get_push_status_path(id):
  return os.path.join(get_repo_status_path(id), "push")

def get_remote_setting_path(id):
  return os.path.join(get_repo_meta_path(id), "remote")

def get_remote_private_key_path(id):
  return os.path.join(get_repo_meta_path(id), "keyfile")

def get_git_lock_file_path(id):
  """When git fetch/push happens, lock this file exclusively!"""
  return os.path.join(get_repo_status_path(id), "git_lock")

# all things about repo

def get_repo_dirs(id):
  return [get_repo_path(id), get_repo_git_path(id), get_repo_status_path(id), get_repo_meta_path(id),
    get_repo_rfr_path(id)]

# rfr
def get_rfr_dir(repoid, rfrid):
  return os.path.join(get_repo_rfr_path(repoid), rfrid)

def get_rfr_meta(repoid, rfrid):
  return os.path.join(get_rfr_dir(repoid, rfrid), "meta.json")

def get_rfr_revision_path(repoid, rfrid, rfrrev):
  return os.path.join(get_rfr_dir(repoid, rfrid), str(rfrrev))

def get_rfr_revision_meta(repoid, rfrid, rfrrev):
  return os.path.join(get_rfr_revision_path(repoid, rfrid, rfrrev), "meta.data")

# reviews
def get_review_dir(repoid, rfrid, rfrrev):
  return os.path.join(get_rfr_revision_path(repoid, rfrid, rfrrev), "reviews")

def get_review_data_path(repoid, rfrid, rfrrev, reviewid):
  return os.path.join(get_review_dir(repoid, rfrid, rfrrev), reviewid)
