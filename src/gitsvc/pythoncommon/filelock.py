import os
import fcntl
from contextlib import contextmanager

@contextmanager
def lock_file(fd, exclusive):
  fcntl.flock(fd, fcntl.LOCK_EX if exclusive else fcntl.LOCK_SH)
  try:
    yield
  finally:
    try:
      fcntl.flock(fd, fcntl.LOCK_UN)
    except:
      pass

@contextmanager
def open_file_with_lock(path, write, binary=True):
  mode = f"r{'+' if write else ''}{'b' if binary else ''}"
  try:
    with open(path, mode) as f:
      with lock_file(f.fileno(), write):
        yield f
  finally:
    pass
