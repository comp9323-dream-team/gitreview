import os
import pickle
from multiprocessing import Process, process
import time

import config
from helpers.file_helper import scan_file, remove_queue
from helpers.email_sender import send_template_email_request_reviewer

def mail_processor(file, data, worker_name):
  
  (receiver, dest, sender, rfr_link) = data
  
  print(f"Processing {file} to send")
  print(f"{worker_name} is sending email from {receiver} to {sender}")
  
  #sending the email
  try:
    send_template_email_request_reviewer(receiver, dest, sender, rfr_link)
  except Exception:
    print("failed to send the email")
  
  # removing file from queue folder
  remove_queue(file)
  
  print(f"Finished processing queue {file}")
  
def mailer_service():  
  in_queues, filenames = scan_file(config.MAIL_QUEUE_DIR)
  
  queue_number = len(in_queues)
  
  # check if there is job in queue or not
  if queue_number > 0:
    print(f"Got {queue_number} mail to send.")
        
    # sort jobs
    filenames.sort()
    
    # do the jobs in queue
    while len(filenames) > 0:
      # separate jobs to be give to worker
      if len(filenames) > config.WORKER:
        worker_number = config.WORKER
      else:
        worker_number = len(filenames)
        
      processes = []
      # apply job to worker
      for i in range(worker_number):
        worker_name = f"Worker {i}"
        file = filenames.pop(0)
        data = in_queues[file]
        
        processor = Process(target=mail_processor, args=(file, data, worker_name))
        processes.append(processor)
        processor.start()  
        
      finished_jobs = 0
      while finished_jobs < worker_number:
        for i in range(worker_number):
          if not processes[i].is_alive():
            finished_jobs += 1
          
        if finished_jobs < worker_number:
          finished_jobs = 0
          time.sleep(1)
            
        # print(finished_jobs, worker_number)      
  else:
    #print("No mail to send, wait for 1 second")
    time.sleep(config.SCAN_INTERVAL)

if __name__ == '__main__':
  while True:
    mailer_service()
    
  