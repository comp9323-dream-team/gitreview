from dotenv import load_dotenv
from pathlib import Path
import os

import dotenv

dotenv_path = Path('container/.env')
load_dotenv(dotenv_path=dotenv_path)

# configs that doesn't depend on a particular system configuration
# MAX_EMAIL=5
WORKER = 3
SCAN_INTERVAL = 2

# need to load these configs from another source (env. variable)
# all of these have to be defined in the env, prefixed with "GITREVIEW_"
# either a name, or a tuple of (name, type)
_vars=[
  "SMTP_SERVER",
  ("SMTP_PORT",int),
  "SMTP_USERNAME",
  "SMTP_SENDER",
  "SMTP_PASSWORD",
  "QUEUE_DIR",
  "MAIL_QUEUE_DIR"
]

# array of optinal values. tuples: (name, type, default value)
for optional, vars in [(False, _vars)]:
  for var in vars:
    if type(var) is str:
      varname = var
      vartype = str
    elif type(var) is tuple:
      varname = var[0]
      vartype = var[1]
    else:
      raise RuntimeError("Unknown variable definition format.")
    try:
      varval = os.environ[varname]
    except KeyError as ex:
      if not optional:
        raise ex
      else:
        varval = var[2]
    varval = vartype(varval)
    globals()[varname] = varval
  