from email import message
import smtplib, ssl
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import config
from urllib.parse import urljoin
import os
import sys

def _send_email_raw(host, port, uname, pword, sender, receiver, subject, msg):
  """
  Send email using SMTP
    params:
    host = host address
    port = port number
    pword = password
    sender = email address of the sender
    receiver = email address of the receiver
    subject = text subject of the email
    msg = plain text body of the email
  """
  context = ssl.create_default_context()
  try:
    with smtplib.SMTP(host=host, port=port) as mailer:
      msg = f"Subject: {subject}\n\n{msg}"
      mailer.starttls(context=context)
      mailer.login(uname, pword)
      mailer.sendmail(sender, receiver, msg)
      return True
      
  except Exception as e:
    print(f"Send mail error! {e}")
    return False

def send_email(dest, subject, msg):
  # use the remaining params from config
  return _send_email_raw(config.SMTP_SERVER, config.SMTP_PORT,
    config.SMTP_USERNAME, config.SMTP_PASSWORD, config.SMTP_SENDER,
    dest, subject, msg)
    
def send_template_email_reset_password(iforgot, dest, token):
  """Send an email for reset password/email verification.
     params:
      iforgot = True for forget/reset password mode, False for email verification mode
  """

  if config.RESET_PASSWORD_SKIP_EMAIL:
    print(f"Reset password token {token}", file=sys.stderr)
    return

  link = urljoin(config.WEBURL, os.path.join("resetpassword/verify", token))

  if iforgot:
    subject = "GitReview - Forgot Password"
    lines = [
      "You have requested to reset your password on GitReview.",
      "Click on the link below to reset your password:",
      "",
      link,
      ""
    ]
  else:
    subject = "GitReview - Email Verification"
    lines = [
      "Welcome to GitReview!",
      "Click on the link below to verify your email and set a new password!",
      "",
      link,
      ""
    ]

  lines.extend([
    "Please copy and paste the above link to your browser if the link cannot be clicked."
    "",
    "If you don't request this operation, please ignore this email.",
    f"This email will expire in {config.SESSION_EXPIRY_DAYS} days.",
    "You can request this email again by using the 'forgot password' option on GitReview's website."
  ])
  
  return send_email(dest, subject, '\n'.join(lines))
  
def send_template_email_request_reviewer(receiver, dest, sender, rfr_link):
  """Send an email for request reviewer.
     params:
      rfr_link = link to the rfr which requested to review
  """
  subject = f"GitReview - Request for review by {sender}"
  body = [
    f"Hi there, {receiver}",
    f"An RFR is ready to review, {sender} send a request for his RFR to be reviewed,",
    "You can find the RFR in the link below:",
    "",
    rfr_link,
    "",
    "Please copy and paste the above link to your browser if the link cannot be clicked."
    "",
    "It would be so great if You willing to review that RFR. Happy reviewing!!!",
    "",
    "Best regards",
    "GitReview team"
  ]
  
  return send_email(dest, subject, '\n'.join(body))