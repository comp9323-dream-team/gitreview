import pickle
import os.path as path

import config
    
def dump_queue(data):
  filename = str(gen_id())
  filename = path.join(config.QUEUE_DIR, f"qm_{filename}")
  with open(filename, "wb") as f:
    pickle.dump(data, f, fix_imports=False)