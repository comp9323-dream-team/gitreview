import os
import pickle

import config

def scan_file(file_path):
  # scan folder
  files = os.scandir(file_path)
  
  in_queues = {}
  filenames = []
  
  # read data in the files
  for file in files:
    if str(file.name)[:3] == "qm_":
      if file.is_file():
        with open(f"{file_path}{file.name}", "rb") as f:
          try:
            filenames.append(str(file.name))
            in_queues[file.name] = pickle.load(f)
          except EOFError:
            print("failed to open queue file.")

  return (in_queues, filenames)
  
def remove_queue(file):
  # generate path of the file
  file_path = f"{config.MAIL_QUEUE_DIR}{file}"
  
  # delete file if exist
  if os.path.exists(file_path):
    os.remove(file_path)
  else:
    print(f"The queue {file} does not exist")
