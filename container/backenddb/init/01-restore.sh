#!/bin/bash

file="/docker-entrypoint-initdb.d/ddl.psql"
basedatafile="/docker-entrypoint-initdb.d/basedata.psql"
dbname="gitreviewdb"

# psql -U postgres -tc "CREATE DATABASE $dbname"

echo "Restoring DB definition using $file"
psql -U postgres $dbname < "$file" || exit 1
echo "Restoring DB basedata using $file"
psql -U postgres $dbname < "$basedatafile" || exit 1

echo "Init script done!"
